<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrimaryCustomerMarketColumnInMetaVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->integer('primary_customer_market')->after('year_established')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->dropColumn('primary_customer_market');
        });
    }
}
