<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYearEstablishInMetaVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->integer('year_established')->after('website')->nullable();
            $table->string('business_category')->after('website')->nullable();
            $table->string('billing_alternate_phone')->after('billing_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->dropColumn('year_established');
            $table->dropColumn('business_category');
            $table->dropColumn('billing_alternate_phone');
        });
    }
}
