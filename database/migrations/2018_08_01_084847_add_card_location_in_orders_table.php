<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardLocationInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('card_location')->after('card_cvc')->nullable();
            $table->string('card_address')->after('card_cvc')->nullable();
            $table->string('card_city')->after('card_cvc')->nullable();
            $table->integer('card_state_id')->after('card_cvc')->nullable();
            $table->string('card_state')->after('card_cvc')->nullable();
            $table->string('card_zip')->after('card_cvc')->nullable();
            $table->integer('card_country_id')->after('card_cvc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('card_location');
            $table->dropColumn('card_address');
            $table->dropColumn('card_city');
            $table->dropColumn('card_state_id');
            $table->dropColumn('card_state');
            $table->dropColumn('card_zip');
            $table->dropColumn('card_country_id');
        });
    }
}
