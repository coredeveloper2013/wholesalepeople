<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameStateToStateTextInBuyerShippingAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buyer_shipping_addresses', function (Blueprint $table) {
            $table->renameColumn('state', 'state_text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buyer_shipping_addresses', function (Blueprint $table) {
            $table->renameColumn('state_text', 'state');
        });
    }
}
