<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowInMobileMainSliderColumnInMetaVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->boolean('show_in_mobile_main_slider')->after('show_in_main_slider')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->dropColumn('show_in_mobile_main_slider');
        });
    }
}
