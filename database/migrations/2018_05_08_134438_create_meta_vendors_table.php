<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetaVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('company_name');
            $table->string('business_name');
            $table->string('website')->nullable();
            $table->string('billing_address');
            $table->string('billing_unit')->nullable();
            $table->string('billing_city');
            $table->integer('billing_state_id')->nullable();
            $table->string('billing_state')->nullable();
            $table->string('billing_zip');
            $table->integer('billing_country_id');
            $table->string('billing_phone');
            $table->string('billing_fax')->nullable();
            $table->boolean('billing_commercial');
            $table->string('factory_address');
            $table->string('factory_unit')->nullable();
            $table->string('factory_city');
            $table->integer('factory_state_id')->nullable();
            $table->string('factory_state')->nullable();
            $table->string('factory_zip');
            $table->integer('factory_country_id');
            $table->string('factory_phone');
            $table->string('factory_alternate_phone')->nullable();
            $table->string('factory_evening_phone')->nullable();
            $table->string('factory_fax')->nullable();
            $table->boolean('factory_commercial')->nullable();
            $table->string('company_info', 1000)->nullable();
            $table->string('hear_about_us');
            $table->string('hear_about_us_other')->nullable();
            $table->boolean('receive_offers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_vendors');
    }
}
