<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSettingsColumnsInMetaVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->boolean('setting_not_logged')->after('receive_offers')->default(0);
            $table->boolean('setting_unverified_user')->after('receive_offers')->default(0);
            $table->boolean('setting_unverified_checkout')->after('receive_offers')->default(0);
            $table->boolean('setting_sort_activation_date')->after('receive_offers')->default(0);
            $table->boolean('setting_consolidation')->after('receive_offers')->default(0);
            $table->boolean('setting_estimated_shipping_charge')->after('receive_offers')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->dropColumn('setting_not_logged');
            $table->dropColumn('setting_unverified_user');
            $table->dropColumn('setting_unverified_checkout');
            $table->dropColumn('setting_sort_activation_date');
            $table->dropColumn('setting_consolidation');
            $table->dropColumn('setting_estimated_shipping_charge');
        });
    }
}
