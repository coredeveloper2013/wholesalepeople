<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreNoColumnInMetaBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_buyers', function (Blueprint $table) {
            $table->string('store_no')->after('website')->nullable();
            $table->string('attention')->after('website')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_buyers', function (Blueprint $table) {
            $table->dropColumn('store_no');
            $table->dropColumn('attention');
        });
    }
}
