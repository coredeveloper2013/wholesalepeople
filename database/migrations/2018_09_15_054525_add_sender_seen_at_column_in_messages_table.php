<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSenderSeenAtColumnInMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->timestamp('sender_seen_at')->after('topic')->nullable();
            $table->timestamp('receiver_seen_at')->after('topic')->nullable();
            $table->dropColumn('seen_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->dropColumn('sender_seen_at');
            $table->dropColumn('receiver_seen_at');
            $table->timestamp('seen_at')->after('topic')->nullable();
        });
    }
}
