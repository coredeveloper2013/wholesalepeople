<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMainSliderColumnInItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->boolean('main_slider')->after('vendor_style_no')->default(0);
            $table->boolean('category_top_slider')->after('vendor_style_no')->default(0);
            $table->boolean('category_second_slider')->after('vendor_style_no')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('main_slider');
            $table->dropColumn('category_top_slider');
            $table->dropColumn('category_second_slider');
        });
    }
}
