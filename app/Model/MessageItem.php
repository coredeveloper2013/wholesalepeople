<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MessageItem extends Model
{
    protected $fillable = [
        'message_id', 'sender', 'message', 'seen_at'
    ];

    public function files() {
        return $this->hasMany('App\Model\MessageFile', 'message_id');
    }
}
