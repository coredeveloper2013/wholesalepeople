<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryBanner extends Model
{
    protected $fillable = [
        'type', 'vendor_id', 'category_id'
    ];

    public function vendor() {
        return $this->belongsTo('App\Model\MetaVendor', 'vendor_id');
    }
}
