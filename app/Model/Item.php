<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'vendor_meta_id', 'status', 'style_no', 'category_id', 'price', 'orig_price', 'pack_id', 'sorting', 'description', 'available_on',
        'availability', 'name', 'default_parent_category', 'default_second_category', 'default_third_category', 'exclusive',
        'min_qty', 'even_color', 'fabric_id', 'made_in_id', 'labeled', 'keywords', 'body_size_id', 'pattern_id', 'length_id',
        'style_id', 'master_fabric_id', 'memo', 'vendor_style_no', 'activated_at', 'main_slider', 'category_top_slider', 'category_second_slider',
        'new_top_slider', 'new_second_slider', 'fabric'
    ];

    public function getPriceAttribute($value) {
        $price = $value;

        $discount = Category::whereIn('id', $this->categories()->pluck('id')->toArray())->max('discount');

        if ($discount != null) {
            $price = $value - (($discount / 100) * $value);
        }

        return $price;
    }

    public function getOrigPriceAttribute($value) {
        $price = $value;

        $discount = Category::whereIn('id', $this->categories()->pluck('id')->toArray())->max('discount');

        if ($discount != null) {
            $price = $this->getOriginal('price');
        }

        return $price;
    }

    public function colors() {
        return $this->belongsToMany('App\Model\Color');
    }

    public function category() {
        return $this->belongsTo('App\Model\Category');
    }

    public function images() {
        return $this->hasMany('App\Model\ItemImages')->orderBy('sort');
    }

    public function vendor() {
        return $this->belongsTo('App\Model\MetaVendor', 'vendor_meta_id', 'id');
    }

    public function pack() {
        return $this->belongsTo('App\Model\Pack');
    }

    public function madeInCountry() {
        return $this->belongsTo('App\Model\MadeInCountry', 'made_in_id');
    }

    public function bodySize() {
        return $this->belongsTo('App\Model\BodySize', 'body_size_id');
    }

    public function pattern() {
        return $this->belongsTo('App\Model\Pattern', 'pattern_id');
    }

    public function length() {
        return $this->belongsTo('App\Model\Length', 'length_id');
    }

    public function style() {
        return $this->belongsTo('App\Model\Style', 'style_id');
    }

    public function categories() {
        return $this->belongsToMany('App\Model\Category');
    }
}
