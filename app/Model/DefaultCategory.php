<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DefaultCategory extends Model
{
    use SoftDeletes;

    protected $table = 'default_categories';
    protected $fillable = [
        'name', 'parent', 'sort'
    ];

    public function parentCategory() {
        return $this->belongsTo('App\Model\DefaultCategory', 'parent');
    }

    public function subCategories() {
        return $this->hasMany('App\Model\DefaultCategory', 'parent', 'id')->orderBy('sort')->orderBy('name');
    }

    public function bodySizes() {
        return $this->hasMany('App\Model\BodySize', 'parent_category_id', 'id');
    }

    public function patterns() {
        return $this->hasMany('App\Model\Pattern', 'parent_category_id', 'id');
    }

    public function lengths() {
        return $this->hasMany('App\Model\Length', 'sub_category_id', 'id');
    }

    public function styles() {
        return $this->hasMany('App\Model\Style', 'parent_category_id', 'id');
    }
}
