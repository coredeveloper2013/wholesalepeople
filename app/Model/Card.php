<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = [
        'user_id', 'default', 'card_number', 'mask', 'card_name', 'card_expiry', 'card_cvc', 'card_type', 'billing_address',
        'billing_city', 'billing_state_id', 'billing_state', 'billing_zip', 'billing_country_id', 'billing_location'
    ];

    public function billingState() {
        return $this->belongsTo('App\Model\State', 'billing_state_id');
    }

    public function billingCountry() {
        return $this->belongsTo('App\Model\Country', 'billing_country_id');
    }
}
