<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            'vendor.layouts.dashboard', 'App\Http\ViewComposers\VendorLayout'
        );

        View::composer(
            ['layouts.app'], 'App\Http\ViewComposers\MainLayout'
        );

        View::composer(
            ['buyer.layouts.profile'], 'App\Http\ViewComposers\BuyerProfileLayout'
        );

        View::composer(
            'admin.layouts.admin', 'App\Http\ViewComposers\AdminLayout'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}