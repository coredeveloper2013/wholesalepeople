<?php

namespace App\Http\Controllers\Api;

use App\Enumeration\Availability;
use App\Enumeration\Role;
use App\Model\BodySize;
use App\Model\Category;
use App\Model\Color;
use App\Model\DefaultCategory;
use App\Model\Item;
use App\Model\ItemImages;
use App\Model\Length;
use App\Model\MadeInCountry;
use App\Model\MasterFabric;
use App\Model\Pack;
use App\Model\Pattern;
use App\Model\Style;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Exception\NotReadableException;
use Uuid;
use Image;
use File;
use Carbon\Carbon;

class ItemController extends Controller
{
    public function getItems(Request $request) {
        if (!isset($request->username) || !isset($request->password))
            return response()->json(['success' => false, 'message' => 'username & password parameter required.']);

        $user = User::where('user_id', $request->username)
            ->whereIn('role', [Role::$VENDOR, Role::$VENDOR_EMPLOYEE])
            ->with('vendor')->first();

        if (!$user)
            return response()->json(['success' => false, 'message' => 'Username not found.']);

        if ($user->vendor->active == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is inactivate.']);

        if ($user->vendor->verified == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is not verified.']);

        if (Hash::check($request->password, $user->password)) {
            $tmp = Item::where('vendor_meta_id', $user->vendor_meta_id)
                ->with('madeInCountry', 'bodySize', 'pattern', 'length', 'style', 'colors', 'images')
                ->get();
            $items = [];

            foreach ($tmp as $item) {
                // Pack
                $sizes = explode("-", $item->pack->name);
                $pack = '';

                for ($i = 1; $i <= sizeof($sizes); $i++) {
                    $var = 'pack' . $i;

                    if ($item->pack->$var != null) {
                        $pack .= $item->pack->$var . '-';
                    } else {
                        $pack .= '0-';
                    }
                }
                $pack = rtrim($pack, "-");

                // Default categories
                $pc1 = DefaultCategory::where('id', $item->default_parent_category)->first();
                $pc2 = DefaultCategory::where('id', $item->default_second_category)->first();
                $pc3 = DefaultCategory::where('id', $item->default_third_category)->first();

                $defaultCategory = $pc1->name;

                if ($pc2)
                    $defaultCategory .= ','.$pc2->name;

                if ($pc3)
                    $defaultCategory .= ','.$pc3->name;

                $colors = [];
                $images = [];

                foreach ($item->colors as $color)
                    $colors[] = $color->name;

                foreach ($item->images as $image)
                    $images[] = [
                        'original' => ($image->image_path != null ? url($image->image_path) : null),
                        'list' => ($image->list_image_path != null ? url($image->list_image_path) : null),
                        'thumbs' => ($image->thumbs_image_path != null ? url($image->thumbs_image_path) : null),
                    ];

                $items [] = [
                    'id' => $item->id,
                    'status' => $item->status,
                    'style_no' => $item->style_no,
                    'vendor_category' => $item->category->name,
                    'price' => $item->price,
                    'original_price' => $item->orig_price,
                    'size' => $item->pack->name,
                    'pack' => $pack,
                    'colors' => $colors,
                    'images' => $images,
                    'sorting' => $item->sorting,
                    'description' => $item->description,
                    'available_on' => $item->available_on,
                    'availability' => ($item->availability == Availability::$ARRIVES_SOON ? 'Arrives Soon / Back Order' : 'In Stock'),
                    'name' => $item->name,
                    'default_category' => $defaultCategory,
                    'exclusive' => $item->exclusive,
                    'min_qty' => $item->min_qty,
                    'even_color' => $item->even_color,
                    'fabric' => $item->fabric,
                    'made_in' => ($item->madeInCountry != null ? $item->madeInCountry->name : null),
                    'labeled' => $item->labeled,
                    'keywords' => $item->keywords,
                    'body_size' => ($item->bodySize != null ? $item->bodySize->name : null),
                    'pattern' => ($item->pattern != null ? $item->pattern->name : null),
                    'length' => ($item->length != null ? $item->length->name : null),
                    'style' => ($item->style != null ? $item->style->name : null),
                    'memo' => $item->memo,
                    'vendor_style_no' => $item->vendor_style_no,
                    'created_at' => ($item->created_at != null ? $item->created_at->format('Y-m-d H:i:s') : null),
                    'activated_at' => $item->activated_at,
                    'updated_at' => ($item->updated_at != null ? $item->updated_at->format('Y-m-d H:i:s') : null),
                ];
            }

            return response()->json(['success' => true, 'items' => $items]);
        }

        return response()->json(['success' => false, 'message' => 'Invalid Password.']);
    }

    public function createItem(Request $request) {
        if (!isset($request->username) || !isset($request->password))
            return response()->json(['success' => false, 'message' => 'username & password parameter required.']);

        $user = User::where('user_id', $request->username)
            ->whereIn('role', [Role::$VENDOR, Role::$VENDOR_EMPLOYEE])
            ->with('vendor')->first();

        if (!$user)
            return response()->json(['success' => false, 'message' => 'Username not found.']);

        if ($user->vendor->active == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is inactivate.']);

        if ($user->vendor->verified == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is not verified.']);

        if (Hash::check($request->password, $user->password)) {
            $requiredParameters = ['styleno', 'vendorcategory', 'defaultcategory', 'size', 'pack', 'unitprice', 'color'];

            foreach ($requiredParameters as $parameter) {
                if (!isset($request->$parameter) || $request->$parameter == '')
                    return response()->json(['success' => false, 'message' => 'These parameters required: '.implode(',', $requiredParameters)]);
            }

            $found = false;
            $status = 0;
            $item = Item::where('vendor_meta_id', $user->vendor_meta_id)
                ->where('style_no', $request->styleno)
                ->first();

            if ($item) {
                $status = $item->status;
                $found = true;
            }

            if ($request->status && $request->status == '1')
                $status = 1;

            // Vendor Category
            $vCategories = [];
            if ($request->vendorcategory != null && $request->vendorcategory == '')
                return response()->json(['success' => false, 'message' => 'Vendor category required.']);

            $vendorCategories = explode(',', $request->vendorcategory);

            $vendorCategory = Category::where('vendor_meta_id', $user->vendor_meta_id)
                ->where('status', 1)
                ->where('name', $vendorCategories[0])
                ->first();

            if (!$vendorCategory)
                return response()->json(['success' => false, 'message' => 'Vendor category not found.']);

            $vCategories[] = $vendorCategory->id;

            if (sizeof($vendorCategories) > 1) {
                for($i=1; $i < sizeof($vendorCategories); $i++) {
                    $vendorCategory = Category::where('vendor_meta_id', $user->vendor_meta_id)
                        ->where('status', 1)
                        ->where('name', $vendorCategories[1])
                        ->first();

                    if (!$vendorCategory)
                        return response()->json(['success' => false, 'message' => 'Vendor sub category not found.']);

                    $vCategories[] = $vendorCategory->id;
                }
            }

            // Default Category Check
            $dc = explode(',', $request->defaultcategory);
            $defaultCategory = DefaultCategory::where('name', $dc[0])
                ->where('parent', 0)
                ->first();

            if (!$defaultCategory)
                return response()->json(['success' => false, 'message' => 'Default category not found.']);

            // Second default category check
            $defaultCategorySecondId = null;
            if (sizeof($dc) > 1) {
                $defaultCategorySecond = DefaultCategory::where('name', $dc[1])
                    ->where('parent', $defaultCategory->id)
                    ->first();

                if (!$defaultCategorySecond)
                    return response()->json(['success' => false, 'message' => 'Sub category not found.']);
                else
                    $defaultCategorySecondId = $defaultCategorySecond->id;
            }

            // Third default category check
            $defaultCategoryThirdId = null;
            if (sizeof($dc) > 2) {
                $defaultCategoryThird = DefaultCategory::where('name', $dc[2])
                    ->where('parent', $defaultCategorySecond->id)
                    ->first();

                if (!$defaultCategoryThird)
                    return response()->json(['success' => false, 'message' => 'Sub category not found.']);
                else
                    $defaultCategoryThirdId = $defaultCategoryThird->id;
            }


            // Size Check
            $packQuery = Pack::query();
            $packQuery->where('vendor_meta_id', $user->vendor_meta_id)
                ->where('status', 1)
                ->where('name', $request->size);

            $packSizes = explode('-', $request->pack);

            for($i=1; $i <= sizeof($packSizes); $i++) {
                $var = 'pack'.$i;
                $packQuery->where('pack'.$i, (int) $packSizes[$i-1]);

                /*if ((int) $packSizes[$i-1] != $pack->$var) {
                    $pack = null;
                    return $packSizes;
                    break;
                }*/
            }

            $pack = $packQuery->first();


            if (!$pack) {
                $pack = new Pack;
                $pack->name = $request->size;
                $pack->status = 1;
                $pack->default = 0;
                $pack->vendor_meta_id = $user->vendor_meta_id;

                $packSizes = explode('-', $request->pack);
                for($i=1; $i <= sizeof($packSizes); $i++) {
                    $var = 'pack'.$i;

                    $pack->$var = (int) $packSizes[$i-1];
                }

                $pack->save();
            }

            // Made In Country
            $madeInId = null;

            if ($request->madein != null && $request->madein != '') {
                $madeIn = MadeInCountry::where('vendor_meta_id', $user->vendor_meta_id)
                    ->where('status', 1)
                    ->where('name', $request->madein)
                    ->first();

                if ($madeIn)
                    $madeInId = $madeIn->id;
            }

            // Body Size
            $bodySizeId = null;

            if ($request->bodysize != null && $request->bodysize != '') {
                $bodySize = BodySize::where('name', $request->bodysize)
                    ->where('parent_category_id', $defaultCategory->id)->first();

                if ($bodySize)
                    $bodySizeId = $bodySize->id;
            }

            // Pattern
            $patternId = null;

            if ($request->pattern != null && $request->pattern != '') {
                $pattern = Pattern::where('name', $request->pattern)
                    ->where('parent_category_id', $defaultCategory->id)->first();

                if ($pattern)
                    $patternId = $pattern->id;
            }

            // Length
            $lengthId = null;

            if ($request->length != null && $request->length != '' && $defaultCategorySecondId != null) {
                $length = Length::where('name', $request->length)
                    ->where('sub_category_id', $defaultCategorySecondId)->first();

                if ($length)
                    $lengthId = $length->id;
            }

            // Style
            $styleId = null;

            if ($request->style != null && $request->style != '') {
                $style = Style::where('name', $request->style)
                    ->where('sub_category_id', $defaultCategorySecondId)->first();

                if ($style)
                    $styleId = $style->id;
            }

            // Master Fabric
            $masterFabricId = null;

            if ($request->masterfabric != null && $request->masterfabric != '') {
                $f = MasterFabric::where('name', $request->masterfabric)->first();

                if ($f)
                    $masterFabricId = $f->id;
            }

            // Available On
            $date = '';

            if ($request->availableon != null || $request->availableon != '') {
                if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->availableon))

                $date = $request->availableon;
            }

            // Availability
            $availability = Availability::$IN_STOCK;

            if ($date != '') {
                if(time() < strtotime($date)) {
                    $availability = Availability::$ARRIVES_SOON;
                }
            }

            // Colors check
            if ($request->color != null && $request->color == '')
                return response()->json(['success' => false, 'message' => 'Color is required.']);

            $colorIds = [];
            $colors = explode(',', $request->color);

            foreach ($colors as $color) {
                $c = Color::where('status', 1)
                    ->where('vendor_meta_id', $user->vendor_meta_id)
                    ->where('name', $color)
                    ->first();

                if (!$c) {
                    $c = Color::create([
                        'name' => $color,
                        'status' => 1,
                        'vendor_meta_id' => $user->vendor_meta_id
                    ]);
                }

                $colorIds[] = $c->id;
            }

            // Create Item
            if ($found) {
                $item->status = $status;
                $item->price = $request->unitprice;
                $item->orig_price = $request->originalprice;
                $item->pack_id = $pack->id;
                $item->description = $request->productdescription;
                $item->available_on = $request->availableon;
                $item->availability = $availability;
                $item->name = $request->itemname;
                $item->default_parent_category = $defaultCategory->id;
                $item->default_second_category = $defaultCategorySecondId;
                $item->default_third_category = $defaultCategoryThirdId;
                $item->min_qty = $request->packqty;
                $item->fabric = $request->fabric;
                $item->labeled = $request->labeled;
                $item->made_in_id = $madeInId;
                $item->keywords = $request->searchkeyword;
                $item->body_size_id = $bodySizeId;
                $item->pattern_id = $patternId;
                $item->length_id = $lengthId;
                $item->style_id = $styleId;
                $item->master_fabric_id = $masterFabricId;
                $item->memo = $request->inhousememo;
                $item->vendor_style_no = $request->vendorstyleno;
                $item->activated_at = ($status == 1 ? Carbon::now()->toDateTimeString() : null);

                $item->save();
                $item->touch();

                $item->colors()->detach();
                $item->categories()->detach();
                $item->images()->delete();
            } else {
                $item = Item::create([
                    'vendor_meta_id' => $user->vendor_meta_id,
                    'status' => $status,
                    'style_no' => $request->styleno,
                    'price' => $request->unitprice,
                    'orig_price' => $request->originalprice,
                    'pack_id' => $pack->id,
                    'description' => $request->productdescription,
                    'available_on' => $request->availableon,
                    'availability' => $availability,
                    'name' => $request->itemname,
                    'default_parent_category' => $defaultCategory->id,
                    'default_second_category' => $defaultCategorySecondId,
                    'default_third_category' => $defaultCategoryThirdId,
                    'min_qty' => $request->packqty,
                    'fabric' => $request->fabric,
                    'labeled' => $request->labeled,
                    'made_in_id' => $madeInId,
                    'keywords' => $request->searchkeyword,
                    'body_size_id' => $bodySizeId,
                    'pattern_id' => $patternId,
                    'length_id' => $lengthId,
                    'style_id' => $styleId,
                    'master_fabric_id' => $masterFabricId,
                    'memo' => $request->inhousememo,
                    'vendor_style_no' => $request->vendorstyleno,
                    'activated_at' => ($status == 1 ? Carbon::now()->toDateTimeString() : null),
                ]);
            }

            $item->colors()->attach($colorIds);
            $item->categories()->attach($vCategories);

            // Images
            if ($request->images != '') {
                $urls = explode(',', $request->images);
                $images_color = [];
                $colors = explode(',', $request->color);

                if ($request->images_color != '')
                    $images_color = explode(',', $request->images_color);

                $sort = 1;
                foreach ($urls as $url) {
                    $filename = Uuid::generate()->string;
                    $ext = pathinfo($url, PATHINFO_EXTENSION);


                    $listSavePath = 'images/vendors/'.$user->vendor_meta_id.'/list/' . $filename . '.' . $ext;
                    $originalSavePath = 'images/vendors/'.$user->vendor_meta_id.'/original/' . $filename . '.' . $ext;
                    $thumbsSavePath = 'images/vendors/'.$user->vendor_meta_id.'/thumbs/' . $filename . '.' . $ext;



                    // List Image
                    try
                    {
                        $img = Image::make($url)->resize(200, 300);
                        $img->save(public_path($listSavePath), 100);
                    }
                    catch(NotReadableException $e)
                    {
                        continue;
                    }



                    // Thumbs Image
                    $thumb = Image::make($url)->resize(150, 150);
                    $thumb->save(public_path($thumbsSavePath), 100);

                    File::copy($url, public_path($originalSavePath));
                    //File::copy($url, public_path('images/item/' . $filename . '.' . $ext));


                    // Color
                    $colorId = null;

                    if (sizeof($images_color) >= $sort) {
                        if (in_array($images_color[$sort-1], $colors)) {
                            if ($images_color[$sort-1] != null && $images_color[$sort-1] != '') {
                                $color = Color::where('vendor_meta_id', $user->vendor_meta_id)
                                    ->where('status', 1)
                                    ->where('name', $images_color[$sort-1])
                                    ->first();

                                if ($color)
                                    $colorId = $color->id;
                            }
                        }
                    }

                    ItemImages::create([
                        'item_id' => $item->id,
                        'sort' => $sort,
                        'color_id' => $colorId,
                        'image_path' => $originalSavePath,
                        'list_image_path' => $listSavePath,
                        'thumbs_image_path' => $thumbsSavePath,
                    ]);

                    $sort++;
                }
            }

            return response()->json(['success' => true, 'message' => 'Success']);
        }

        return response()->json(['success' => false, 'message' => 'Invalid Password.']);
    }

    public function changeStatus(Request $request) {
        if (!isset($request->username) || !isset($request->password))
            return response()->json(['success' => false, 'message' => 'username & password parameter required.']);

        $user = User::where('user_id', $request->username)
            ->whereIn('role', [Role::$VENDOR, Role::$VENDOR_EMPLOYEE])
            ->with('vendor')->first();

        if (!$user)
            return response()->json(['success' => false, 'message' => 'Username not found.']);

        if ($user->vendor->active == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is inactivate.']);

        if ($user->vendor->verified == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is not verified.']);

        if (Hash::check($request->password, $user->password)) {
            $requiredParameters = ['styleno', 'status'];

            foreach ($requiredParameters as $parameter) {
                if (!isset($request->$parameter) || $request->$parameter == '')
                    return response()->json(['success' => false, 'message' => 'These parameters required: '.implode(',', $requiredParameters)]);
            }

            $item = Item::where('style_no', $request->styleno)->where('vendor_meta_id', $user->vendor_meta_id)->first();


            if ($request->status != '0' && $request->status != '1')
                return response()->json(['success' => false, 'message' => 'Status must be 0 or 1.']);

            if (!$item)
                return response()->json(['success' => false, 'message' => 'Style No. not found.']);

            $item->status = (int) $request->status;

            if ($request->status == '1')
                $item->activated_at = Carbon::now()->toDateTimeString();

            $item->save();

            return response()->json(['success' => true, 'message' => 'Status updated.']);
        }
        return response()->json(['success' => false, 'message' => 'Invalid Password.']);
    }
}
