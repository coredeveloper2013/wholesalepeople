<?php

namespace App\Http\Controllers\Api;

use App\Enumeration\Role;
use App\Model\Color;
use App\Model\MasterColor;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ColorController extends Controller
{
    public function addColor(Request $request) {
        if (!isset($request->username) || !isset($request->password))
            return response()->json(['success' => false, 'message' => 'username & password parameter required.']);

        $user = User::where('user_id', $request->username)
            ->whereIn('role', [Role::$VENDOR, Role::$VENDOR_EMPLOYEE])
            ->with('vendor')->first();

        if (!$user)
            return response()->json(['success' => false, 'message' => 'Username not found.']);

        if ($user->vendor->active == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is inactivate.']);

        if ($user->vendor->verified == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is not verified.']);

        if (Hash::check($request->password, $user->password)) {
            $requiredParameters = ['name', 'master_color'];

            foreach ($requiredParameters as $parameter) {
                if (!isset($request->$parameter) || $request->$parameter == '')
                    return response()->json(['success' => false, 'message' => 'These parameters required: '.implode(',', $requiredParameters)]);
            }

            $color = Color::where('name', $request->name)
                ->where('vendor_meta_id', $user->vendor_meta_id)
                ->first();

            if ($color)
                return response()->json(['success' => false, 'message' => 'Color already exist.']);

            $masterColor = MasterColor::where('name', $request->master_color)->first();

            if (!$masterColor)
                return response()->json(['success' => false, 'message' => 'Master Color not found.']);

            Color::create([
                'name' => $request->name,
                'status' => 1,
                'master_color_id' => $masterColor->id,
                'vendor_meta_id' => $user->vendor_meta_id
            ]);

            return response()->json(['success' => true, 'message' => 'Color added.']);
        }

        return response()->json(['success' => false, 'message' => 'Invalid Password.']);
    }
}
