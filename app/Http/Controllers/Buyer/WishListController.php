<?php

namespace App\Http\Controllers\Buyer;

use App\Model\CartItem;
use App\Model\Item;
use App\Model\WishListItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WishListController extends Controller
{
    public function index() {
        $obj = new WishListItem();
        $items = Item::where('status', 1)
            ->whereIn('id', $obj->getItemIds())
            ->with('images', 'vendor')
            ->get();

        return view('buyer.profile.wishlist', compact('items'));
    }

    public function addToWishList(Request $request) {
        $item = WishListItem::where('user_id', Auth::user()->id)
            ->where('item_id', $request->id)->first();

        if (!$item) {
            WishListItem::create([
                'user_id' => Auth::user()->id,
                'item_id' => $request->id,
            ]);
        }
    }

    public function removeWishListItem(Request $request) {
        WishListItem::where('item_id', $request->id)
            ->where('user_id', Auth::user()->id)
            ->delete();
    }

    public function itemDetails(Request $request) {
        $item = Item::where('status', 1)
            ->where('id', $request->id)
            ->with('colors')
            ->first();

        return response()->json($item->toArray());
    }

    public function addToCart(Request $request) {
        $item = Item::where('id', $request->item_id)
            ->where('status', 1)
            ->with('pack')
            ->first();

        foreach ($request->colors as $color_id => $count) {
            if ($count != null || $count != '') {
                $count = (int) $count;

                $previous = CartItem::where('item_id', $item->id)
                    ->where('color_id', $color_id)
                    ->first();

                if ($previous) {
                    $previous->quantity = $previous->quantity + $count;
                    $previous->save();
                } else {
                    CartItem::create([
                        'user_id' => Auth::user()->id,
                        'vendor_meta_id' => $item->vendor_meta_id,
                        'item_id' => $item->id,
                        'color_id' => $color_id,
                        'quantity' => $count,
                    ]);
                }
            }
        }
    }
}
