<?php

namespace App\Http\Controllers\Buyer;

use App\Enumeration\VendorImageType;
use App\Model\Card;
use App\Model\Notification;
use App\Model\Order;
use App\Model\VendorImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use CreditCard;

class OtherController extends Controller
{
    public function showOrderDetails(Order $order) {
        if ($order->user_id != Auth::user()->id)
            abort(404);

        $allItems = [];
        $order->load( 'items', 'vendor');

        // Logo
        $vendor_logo_path = '';

        $image = VendorImage::where('vendor_meta_id', $order->vendor_meta_id)
            ->where('type', VendorImageType::$LOGO)
            ->where('status', 1)->first();

        if ($image)
            $vendor_logo_path = asset($image->image_path);

        foreach($order->items as $item)
            $allItems[$item->item_id][] = $item;

        return view('buyer.order_details', compact('order', 'vendor_logo_path', 'allItems'))->with('page_title', 'Order Details: '.$order->order_number);
    }

    public function viewNotification(Request $request) {
        Notification::where('id', $request->id)->update(['view' => 1]);

        return redirect()->to($request->link);
    }

    public function allNotification() {
        $notifications = Notification::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('buyer.notifications', compact('notifications'))->with('page_title', 'Notifications');
    }

    public function addCreditCard(Request $request) {
        $rules = [
            'card_number' => 'required|max:191|min:6',
            'card_name' => 'required|max:191',
            'expiration_date' => 'required|date_format:"m/y"',
            'cvc' => 'required',
            'factoryAddress' => 'required|string|max:255',
            'factoryCity' => 'required|string|max:255',
            'factoryZipCode' => 'required|string|max:255',
            'factoryCountry' => 'required',
        ];


        if ($request->factoryLocation == "INT")
            $rules['factoryState'] = 'required|string|max:255';
        else
            $rules['factoryStateSelect'] = 'required';

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()->first()]);
        }


        $validator->after(function ($validator) use($request) {
            // Card Number Check
            $card = CreditCard::validCreditCard($request->card_number);

            if (!$card['valid'])
                $validator->errors()->add('number', 'Invalid Card Number');

            // CVC Check
            $validCvc = CreditCard::validCvc($request->cvc, $card['type']);
            if (!$validCvc)
                $validator->errors()->add('cvc', 'Invalid CVC');

            // Expiry Check
            $tmp  = explode('/', $request->expiration_date);
            $validDate = CreditCard::validDate('20'.$tmp[1], $tmp[0]);
            if (!$validDate)
                $validator->errors()->add('expiry', 'Invalid Expiry');
        });


        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()->first()]);
        }

        $card = CreditCard::validCreditCard($request->card_number);
        $cardType = $card['type'];

        $factory_state_id = null;
        $factory_state = null;

        if ($request->factoryLocation == "INT")
            $factory_state = $request->factoryState;
        else
            $factory_state_id = $request->factoryStateSelect;

        if ($request->default) {
            Card::where('user_id', Auth::user()->id)->update([
                'default' => 0
            ]);
        }

        $card = Card::create([
            'user_id' => Auth::user()->id,
            'default' => ($request->default ? 1 : 0),
            'card_number' => encrypt($request->card_number),
            'mask' => str_repeat("*", (strlen($request->card_number) - 4)).substr($request->card_number,-4,4),
            'card_name' => encrypt($request->card_name),
            'card_expiry' => $request->expiration_date,
            'card_cvc' => encrypt($request->cvc),
            'card_type' => $cardType,
            'billing_location' => $request->factoryLocation,
            'billing_address' => $request->factoryAddress,
            'billing_city' => $request->factoryCity,
            'billing_state_id' => $factory_state_id,
            'billing_state' => $factory_state,
            'billing_zip' => $request->factoryZipCode,
            'billing_country_id' => $request->factoryCountry
        ]);

        return response()->json(['success' => true, 'message' => $card->toArray()]);
    }
}
