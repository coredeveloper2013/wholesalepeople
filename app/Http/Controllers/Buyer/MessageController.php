<?php

namespace App\Http\Controllers\Buyer;

use App\Enumeration\MessageRole;
use App\Model\Message;
use App\Model\MessageFile;
use App\Model\MessageItem;
use App\Model\MetaVendor;
use App\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Uuid;
use Storage;
use Mail;

class MessageController extends Controller
{
    public function index() {
        $vendors = MetaVendor::where('verified', 1)->where('active', 1)->where('live', 1)->orderBy('company_name')->get();

        // Inbox
        $inboxes = Message::where('receiver_type', MessageRole::$BUYER)
            ->where('receiver_id', Auth::user()->id)
            ->orderBy('updated_at', 'desc')
            ->with('items')->get();

        $inboxes2 =Message::where('sender_type', MessageRole::$BUYER)
            ->where('sender_id', Auth::user()->id)
            ->orderBy('updated_at', 'desc')
            ->with(['items' => function($query) {
                $query->where('sender', 0);
            }])->get();

        foreach ($inboxes2 as $inbox) {
            if (sizeof($inbox->items) > 0) {
                $inboxes->add($inbox);
            }
        }

        $inboxes = $inboxes->sortByDesc('updated_at');

        // Outbox
        $outboxes = [];
        $tmp = Message::where('sender_type', MessageRole::$BUYER)
            ->where('sender_id', Auth::user()->id)
            ->orderBy('updated_at', 'desc')
            ->with(['items' => function($query) {
                $query->where('sender', 0);
            }])->get();

        foreach ($tmp as $t) {
            if (sizeof($t->items) == 0) {
                $outboxes[] = $t;
            }
        }

        return view('buyer.profile.message', compact('vendors', 'outboxes', 'inboxes'));
    }

    public function add(Request $request) {
        $orderID = null;

        if ($request->orderID && $request->orderID != '') {
            $order = Order::where('id', $request->orderID)
                ->where('user_id', Auth::user()->id)
                ->where('vendor_meta_id', $request->vendor)
                ->first();

            if (!$order)
                abort(404);

            $orderID = $order->id;
        }

        $message = Message::create([
            'sender_id' => Auth::user()->id,
            'sender_type' => MessageRole::$BUYER,
            'receiver_id' => ($request->type == '1' ? $request->vendor : 0),
            'receiver_type' => ($request->type == '1' ? MessageRole::$VENDOR : MessageRole::$ADMIN),
            'title' => $request->title,
            'topic' => $request->topic,
            'order_id' => $orderID,
            'sender_seen_at' => Carbon::now()->toDateTimeString()
        ]);

        $item = MessageItem::create([
            'message_id' => $message->id,
            'message' => $request->message
        ]);

        // Files
        if ($request->attachments) {
            foreach ($request->attachments as $f) {
                $extension = $f->getClientOriginalExtension();
                $filename = Uuid::generate()->string . '.' . $extension;
                $mime = $f->getMimeType();

                Storage::disk('message_files')->put($filename, file_get_contents($f));

                MessageFile::create([
                    'message_id' => $item->id,
                    'filename' => $filename,
                    'sender_seen_at' => Carbon::now()->toDateTimeString(),
                    'mime' => $mime
                ]);
            }
        }

        // Send Mail
        if ($request->type == '1') {
            $vendor = MetaVendor::where('id', $request->vendor)->first();
            $email = $vendor->user->email;

            $data = [
                'company_name' => Auth::user()->buyer->company_name,
                'message_obj' => $message,
                'text' => $request->message
            ];

            //return view('emails.vendor.new_message', $data);

            try {
                Mail::send('emails.vendor.new_message', $data, function ($message) use ($email) {
                    $message->subject('New Message');
                    $message->to($email, Auth::user()->name);
                });
            } catch (\Exception $exception) {

            }
        }
    }

    public function chats($id) {
        $message = Message::where('id', $id)->with('items')->first();

        if (!$message)
            abort(404);

        if (($message->sender_id == Auth::user()->id && $message->sender_type == MessageRole::$BUYER) || ($message->receiver_id == Auth::user()->id && $message->receiver_type == MessageRole::$BUYER)) {
            $isSender = $message->isSender();

            $message->timestamps = false;

            if ($isSender) {
                MessageItem::where('message_id', $message->id)->where('sender', 0)->update([
                    'seen_at' => Carbon::now()->toDateTimeString()
                ]);

                $message->sender_seen_at = Carbon::now()->toDateTimeString();
                $personName = $message->receiverName();
            } else {
                MessageItem::where('message_id', $message->id)->where('sender', 1)->update([
                    'seen_at' => Carbon::now()->toDateTimeString()
                ]);

                $message->receiver_seen_at = Carbon::now()->toDateTimeString();
                $personName = $message->senderName();
            }

            $message->save();

            return view('buyer.profile.message_items', compact('message', 'isSender', 'personName'));
        } else {
            abort(404);
        }
    }

    public function addChat(Request $request) {
        $message = Message::where('id', $request->id)->first();

        if (($message->sender_id == Auth::user()->id && $message->sender_type == MessageRole::$BUYER) || ($message->receiver_id == Auth::user()->id && $message->receiver_type == MessageRole::$BUYER)) {
            $item = MessageItem::create([
                'message_id' => $message->id,
                'sender' => $message->isSender(),
                'message' => $request->msg,
            ]);

            // Files
            if ($request->attachments) {
                foreach ($request->attachments as $f) {
                    $extension = $f->getClientOriginalExtension();
                    $filename = Uuid::generate()->string . '.' . $extension;
                    $mime = $f->getMimeType();

                    Storage::disk('message_files')->put($filename, file_get_contents($f));

                    MessageFile::create([
                        'message_id' => $item->id,
                        'filename' => $filename,
                        'original_filename' => $f->getClientOriginalName(),
                        'mime' => $mime
                    ]);
                }
            }


            if ($message->isSender())
                $message->receiver_seen_at = null;
            else
                $message->sender_seen_at = null;

            $message->touch();
            $message->save();

            // Send Mail
            if ($message->receiver_type == MessageRole::$VENDOR) {
                $vendor = MetaVendor::where('id', $message->receiver_id)->first();
                $email = $vendor->user->email;

                $data = [
                    'company_name' => Auth::user()->buyer->company_name,
                    'message_obj' => $message,
                    'text' => $request->msg
                ];

                //return view('emails.vendor.new_message', $data);

                try {
                    Mail::send('emails.vendor.new_message', $data, function ($message) use ($email) {
                        $message->subject('New Message');
                        $message->to($email, Auth::user()->name);
                    });
                } catch (\Exception $exception) {

                }
            }

            return response()->json(['success' => true, 'message' => date('F j, Y H:i A', strtotime($item->created_at))]);
        }

        return response()->json(['success' => false, 'message' => 'Cannot add']);
    }
}
