<?php

namespace App\Http\Controllers;

use App\Enumeration\CategoryBannerType;
use App\Enumeration\Role;
use App\Enumeration\SliderType;
use App\Enumeration\VendorImageType;
use App\Model\BestItem;
use App\Model\BodySize;
use App\Model\CartItem;
use App\Model\Category;
use App\Model\CategoryBanner;
use App\Model\Color;
use App\Model\Coupon;
use App\Model\DefaultCategory;
use App\Model\Item;
use App\Model\ItemImages;
use App\Model\ItemView;
use App\Model\MasterColor;
use App\Model\MasterFabric;
use App\Model\MessageFile;
use App\Model\MetaVendor;
use App\Model\OpeningSoon;
use App\Model\Pattern;
use App\Model\Review;
use App\Model\Setting;
use App\Model\SliderItem;
use App\Model\Style;
use App\Model\VendorImage;
use App\Model\Visitor;
use App\Model\WishListItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Storage;
use Image;
use Uuid;
use Carbon\Carbon;


class HomeController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function comingSoon() {
        $openingDate = Setting::where('name', 'opening_date')->first();

        return view('pages.coming_soon', compact('openingDate'));
    }

    public function index()
    {
        // Opening Soon check
        $os = Setting::where('name', 'opening_soon')->first();

        if ($os && $os->value == '1')
            return $this->comingSoon();

        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();

        // New Vendors
        $newVendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->with(['images' => function($q) {
                $q->where('status', 1)->where('type', VendorImageType::$BIDDING_SMALL_BANNER);
            }])
            ->whereHas('images', function($query) {
                $query->where('status', 1)->where('type', VendorImageType::$BIDDING_SMALL_BANNER);
            })
            ->orderBy('activated_at', 'desc')
            ->limit(7)
            ->get();

        foreach ($newVendors as &$vendor) {
            $items = Item::where('status', 1)
                ->where('vendor_meta_id', $vendor->id)
                ->orderBy('activated_at', 'desc')
                ->with('images')
                ->limit(5)
                ->get();

            $vendor->items = $items;
        }

        // Best Selling Items
        $sql = "SELECT items.id, t.count 
                FROM items 
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id 
                JOIN meta_vendors ON items.vendor_meta_id = meta_vendors.id
                WHERE items.deleted_at IS NULL AND items.status = 1 AND meta_vendors.active=1 AND meta_vendors.verified=1 AND meta_vendors.live=1
                ORDER BY count DESC
                LIMIT 7";


        $bestItems = DB::select($sql);

        $bestItemIds = [];
        foreach ($bestItems as $item)
            $bestItemIds[] = $item->id;

        $bestItems = [];
        if (sizeof($bestItemIds) >= 7) {
            $bestItems = Item::whereIn('id', $bestItemIds)
                ->with('images', 'vendor', 'colors')
                ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $bestItemIds) . " )"))
                ->get();
        } else {
            $bestItems = Item::where('status', 1)
                ->with('images', 'vendor', 'colors')
                ->whereIn('vendor_meta_id', $activeVendors)
                ->orderBy('activated_at', 'desc')
                ->limit(7)
                ->get();
        }

        // New Arrivals
        $womenCategory = DefaultCategory::where('name', 'women')->where('parent', 0)->first();

        $newArrivals = [];
        if ($womenCategory) {
            $newArrivals = Item::where('status', 1)
                ->whereIn('vendor_meta_id', $activeVendors)
                ->where('default_parent_category', $womenCategory->id)
                ->orderBy('activated_at', 'desc')
                ->limit(14)
                ->with('images', 'vendor', 'colors')
                ->get();
        }

        // New Arrivals
        $accessoriesCategory = DefaultCategory::where('name', 'accessories')->where('parent', 0)->first();

        $newArrivalsAccessories = [];
        if ($accessoriesCategory) {
            $newArrivalsAccessories = Item::where('status', 1)
                ->whereIn('vendor_meta_id', $activeVendors)
                ->where('default_parent_category', $accessoriesCategory->id)
                ->orderBy('activated_at', 'desc')
                ->limit(7)
                ->with('images', 'vendor', 'colors')
                ->get();
        }

        // Main Slider
        $mainSliderVendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->where('show_in_main_slider', 1)
            ->with(['images' => function($q) {
                $q->where('status', 1)->where('type', VendorImageType::$BIDDING_BIG_BANNER);
            }])
            ->whereHas('images', function($query) {
                $query->where('status', 1)->where('type', VendorImageType::$BIDDING_BIG_BANNER);
            })
            ->orderBy('main_slider_at', 'desc')
            ->get();

        foreach ($mainSliderVendors as &$vendor) {
            $mainSliderItemIds = [];

            $tmp = SliderItem::where('vendor_meta_id', $vendor->id)
                ->where('type', SliderType::$MAIN_SLIDER)
                ->orderBy('sort')
                ->get();

            foreach ($tmp as $t)
                $mainSliderItemIds[] = $t->item_id;


            $vendor->items = [];

            if (sizeof($mainSliderItemIds) > 0) {
                $vendor->items = Item::where('status', 1)
                    ->whereIn('id', $mainSliderItemIds)
                    ->with('images')
                    ->limit(7)
                    ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $mainSliderItemIds) . " )"))
                    ->get();
            }

        }

        // Mobile Slider
        $mobileMainSliderVendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->where('show_in_mobile_main_slider', 1)
            ->with(['images' => function($q) {
                $q->where('status', 1)->where('type', VendorImageType::$BIDDING_SMALL_BANNER);
            }])
            ->whereHas('images', function($query) {
                $query->where('status', 1)->where('type', VendorImageType::$BIDDING_SMALL_BANNER);
            })
            ->orderBy('created_at', 'desc')
            ->get();


        // Welcome Notification
        $welcome_msg = '';

        if(!isset($_COOKIE['welcome_popup_stylepic'])) {
            $setting = Setting::where('name', 'welcome_notification')->first();

            if ($setting && $setting->value != null)
                $welcome_msg = $setting->value;

            setcookie("welcome_popup_stylepic", 'sessionexists', time()+3600*24);
        }

        // Opening Soons Item
        $osItems = OpeningSoon::orderBy('sort')->get();

        return view('pages.home', compact('newVendors', 'newArrivals', 'bestItems', 'mainSliderVendors',
            'mobileMainSliderVendors', 'welcome_msg', 'osItems', 'newArrivalsAccessories'));
    }

    public function categoryPage($category) {
        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();

        // New Vendors
        $sql = "SELECT vendor_meta_id FROM items WHERE default_parent_category = ".$category->id." GROUP BY vendor_meta_id";
        $tmp = DB::select($sql);
        $newVendorIds = [];

        foreach($tmp as $item)
            $newVendorIds[] = $item->vendor_meta_id;

        $newVendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->whereIn('id', $newVendorIds)
            ->with(['images' => function($q) {
                $q->where('status', 1)->where('type', VendorImageType::$BIDDING_SMALL_BANNER);
            }])
            ->whereHas('images', function($query) {
                $query->where('status', 1)->where('type', VendorImageType::$BIDDING_SMALL_BANNER);
            })
            ->orderBy('activated_at', 'desc')
            ->limit(6)
            ->get();

        foreach ($newVendors as &$vendor) {
            $items = Item::where('status', 1)
                ->where('vendor_meta_id', $vendor->id)
                ->orderBy('activated_at', 'desc')
                ->with('images')
                ->limit(4)
                ->get();

            $vendor->items = $items;
        }

        // New Arrivals
        $newArrivals = Item::where('status', 1)
            ->where('default_parent_category', $category->id)
            ->whereIn('vendor_meta_id', $activeVendors)
            ->orderBy('activated_at', 'desc')
            ->limit(12)
            ->with('images', 'vendor', 'colors')
            ->get();

        // Top Slider
        $topSliderVendorIds = [];

        $tmp = CategoryBanner::where('type', CategoryBannerType::$TOP_SLIDER)
            ->where('category_id', $category->id)
            ->get();

        foreach ($tmp as $item)
            $topSliderVendorIds[] = $item->vendor_id;

        $topSliderVendors = [];
        $tmp = MetaVendor::where('active', 1)
            ->where('verified', 1)
            ->where('live', 1)
            ->whereIn('id', $topSliderVendorIds)
            ->get();

        foreach ($tmp as &$vendor) {
            $ids = [];
            $tmp = SliderItem::where('vendor_meta_id', $vendor->id)
                ->where('type', SliderType::$CATEGORY_TOP_SLIDER)
                ->orderBy('sort')
                ->get();

            foreach ($tmp as $t)
                $ids[] = $t->item_id;

            $items = [];

            if (sizeof($ids) > 0) {
                $items = Item::where('status', 1)
                    ->whereIn('id', $ids)
                    ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $ids) . " )"))
                    ->with('images')
                    ->limit(6)
                    ->get();
            }

            if (sizeof($items) > 0) {
                $vendor->items = $items;
                $topSliderVendors[] = $vendor;
            }
        }

        // Small Banner
        $smallBannerVendorIds = [];

        $tmp = CategoryBanner::where('type', CategoryBannerType::$SMALL_BANNER)
            ->where('category_id', $category->id)
            ->get();

        foreach ($tmp as $item)
            $smallBannerVendorIds[] = $item->vendor_id;

        $smallBannerVendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->whereIn('id', $smallBannerVendorIds)
            //->whereIn('id', $newVendorIds)
            ->with(['images' => function($q) {
                $q->where('status', 1)->where('type', VendorImageType::$SMALL_AD_BANNER);
            }])
            ->whereHas('images', function($query) {
                $query->where('status', 1)->where('type', VendorImageType::$SMALL_AD_BANNER);
            })
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();

        // Second Slider
        $secondSliderVendorIds = [];

        $tmp = CategoryBanner::where('type', CategoryBannerType::$SECOND_SLIDER)
            ->where('category_id', $category->id)
            ->get();

        foreach ($tmp as $item)
            $secondSliderVendorIds[] = $item->vendor_id;

        $secondSliderVendors = [];
        $tmp = MetaVendor::where('active', 1)
            ->where('verified', 1)
            ->where('live', 1)
            ->whereIn('id', $secondSliderVendorIds)
            ->get();

        foreach ($tmp as &$vendor) {
            $ids = [];
            $tmp = SliderItem::where('vendor_meta_id', $vendor->id)
                ->where('type', SliderType::$CATEGORY_SECOND_SLIDER)
                ->orderBy('sort')
                ->get();

            foreach ($tmp as $t)
                $ids[] = $t->item_id;

            $items = [];

            if (sizeof($ids) > 0) {
                $items = Item::where('status', 1)
                    ->whereIn('id', $ids)
                    ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $ids) . " )"))
                    ->with('images')
                    ->limit(6)
                    ->get();
            }

            if (sizeof($items) > 0) {
                $vendor->items = $items;
                $secondSliderVendors[] = $vendor;
            }
        }


        $category->load('subCategories');
        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->orderBy('company_name')->get();

        // Top Banner
        $topBannerUrl = '';
        $topBannerVendor = '';
        $bannerVendor = CategoryBanner::where('type', CategoryBannerType::$TOP_BANNER)
            ->where('category_id', $category->id)
            ->first();

        if ($bannerVendor) {
            $banner = VendorImage::where('type', VendorImageType::$TOP_BANNER)
                ->where('vendor_meta_id', $bannerVendor->vendor_id)
                ->where('status', 1)
                ->with('vendor')
                ->first();

            if ($banner) {
                $topBannerVendor = $banner->vendor->company_name;
                $topBannerUrl = cdn($banner->image_path);
            }
        }

        return view('pages.parent_category', compact('category', 'vendors', 'newVendors', 'newArrivals',
            'topSliderVendors', 'smallBannerVendors', 'secondSliderVendors', 'topBannerVendor', 'topBannerUrl'));
    }

    public function newArrivalHomePage() {
        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();

        // New Vendors
        $newVendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->with(['images' => function($q) {
                $q->where('status', 1)->where('type', VendorImageType::$BIDDING_SMALL_BANNER);
            }])
            ->whereHas('images', function($query) {
                $query->where('status', 1)->where('type', VendorImageType::$BIDDING_SMALL_BANNER);
            })
            ->orderBy('activated_at', 'desc')
            ->limit(6)
            ->get();

        foreach ($newVendors as &$vendor) {
            $items = Item::where('status', 1)
                ->where('vendor_meta_id', $vendor->id)
                ->orderBy('activated_at', 'desc')
                ->with('images')
                ->limit(4)
                ->get();

            $vendor->items = $items;
        }

        // New Arrivals
        $newArrivals = Item::where('status', 1)
            ->whereIn('vendor_meta_id', $activeVendors)
            ->orderBy('activated_at', 'desc')
            ->limit(12)
            ->with('images', 'vendor', 'colors')
            ->get();

        // Top Slider
        $topSliderVendorIds = [];

        $tmp = CategoryBanner::where('type', CategoryBannerType::$TOP_SLIDER)
            ->where('category_id', 0)
            ->get();

        foreach ($tmp as $item)
            $topSliderVendorIds[] = $item->vendor_id;

        $topSliderVendors = [];
        $tmp = MetaVendor::where('active', 1)
            ->where('verified', 1)
            ->where('live', 1)
            ->whereIn('id', $topSliderVendorIds)
            ->get();

        foreach ($tmp as &$vendor) {
            $ids = [];
            $tmp = SliderItem::where('vendor_meta_id', $vendor->id)
                ->where('type', SliderType::$NEW_ARRIVAL_TOP_SLIDER)
                ->orderBy('sort')
                ->get();

            foreach ($tmp as $t)
                $ids[] = $t->item_id;

            $items = [];

            if (sizeof($ids) > 0) {
                $items = Item::where('status', 1)
                    ->whereIn('id', $ids)
                    ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $ids) . " )"))
                    ->with('images')
                    ->limit(6)
                    ->get();
            }

            if (sizeof($items) > 0) {
                $vendor->items = $items;
                $topSliderVendors[] = $vendor;
            }
        }

        // Small Banner
        $smallBannerVendorIds = [];

        $tmp = CategoryBanner::where('type', CategoryBannerType::$SMALL_BANNER)
            ->where('category_id', 0)
            ->get();

        foreach ($tmp as $item)
            $smallBannerVendorIds[] = $item->vendor_id;

        $smallBannerVendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->whereIn('id', $smallBannerVendorIds)
            //->whereIn('id', $newVendorIds)
            ->with(['images' => function($q) {
                $q->where('status', 1)->where('type', VendorImageType::$SMALL_AD_BANNER);
            }])
            ->whereHas('images', function($query) {
                $query->where('status', 1)->where('type', VendorImageType::$SMALL_AD_BANNER);
            })
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();

        // Second Slider
        $secondSliderVendorIds = [];

        $tmp = CategoryBanner::where('type', CategoryBannerType::$SECOND_SLIDER)
            ->where('category_id', 0)
            ->get();

        foreach ($tmp as $item)
            $secondSliderVendorIds[] = $item->vendor_id;

        $secondSliderVendors = [];
        $tmp = MetaVendor::where('active', 1)
            ->where('verified', 1)
            ->where('live', 1)
            ->whereIn('id', $secondSliderVendorIds)
            ->get();

        foreach ($tmp as &$vendor) {
            $tmp = SliderItem::where('vendor_meta_id', $vendor->id)
                ->where('type', SliderType::$NEW_ARRIVAL_SECOND_SLIDER)
                ->orderBy('sort')
                ->get();

            $ids = [];
            foreach ($tmp as $t)
                $ids[] = $t->item_id;

            $items = [];

            if (sizeof($ids) > 0) {
                $items = Item::where('status', 1)
                    ->whereIn('id', $ids)
                    ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $ids) . " )"))
                    ->with('images')
                    ->limit(6)
                    ->get();
            }

            if (sizeof($items) > 0) {
                $vendor->items = $items;
                $secondSliderVendors[] = $vendor;
            }
        }

        // Default Category
        $parentCategories = DefaultCategory::where('parent', 0)->orderBy('sort')->get();


        // Vendors
        /*$vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->orderBy('company_name')->get();*/

        // Arrival Dates
        $totalCount = Item::where('status', 1)->count();
        $byArrivalDate = [];
        $byArrivalDate[] = [
            'name' => 'All',
            'count' => number_format($totalCount),
            'day' => 'A'
        ];

        for($i=0; $i <= 6; $i++) {
            $count = Item::where('status', 1)
                ->whereDate('activated_at', date('Y-m-d', strtotime("-$i day")))
                ->count();

            $byArrivalDate[] = [
                'name' => date('F j', strtotime("-$i day")),
                'count' => number_format($count),
                'day' => $i
            ];
        }

        // Master Colors
        $masterColors = MasterColor::orderBy('name')->get();

        // Top Banner
        $topBannerUrl = '';
        $topBannerVendor = '';
        $bannerVendor = CategoryBanner::where('type', CategoryBannerType::$TOP_BANNER)
            ->where('category_id', 0)
            ->first();

        if ($bannerVendor) {
            $banner = VendorImage::where('type', VendorImageType::$TOP_BANNER)
                ->where('vendor_meta_id', $bannerVendor->vendor_id)
                ->where('status', 1)
                ->with('vendor')
                ->first();

            if ($banner) {
                $topBannerVendor = $banner->vendor->company_name;
                $topBannerUrl = cdn($banner->image_path);
            }
        }

        return view('pages.new_arrival_home', compact(  'newVendors', 'newArrivals', 'parentCategories',
            'topSliderVendors', 'smallBannerVendors', 'secondSliderVendors', 'byArrivalDate', 'masterColors', 'topBannerUrl', 'topBannerVendor'));
    }

    public function subCategoryPage($category) {
        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();

        $category->load('subCategories', 'parentCategory', 'lengths');
        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->orderBy('company_name')->get();

        $bodySizes = BodySize::where('parent_category_id', $category->parentCategory->id)
            ->orderBy('name')
            ->get();

        $patterns = Pattern::where('parent_category_id', $category->parentCategory->id)
            ->orderBy('name')
            ->get();

        $styles = Style::where('sub_category_id', $category->id)
            ->orderBy('name')
            ->get();

        $masterColors = MasterColor::orderBy('name')->get();
        $masterFabrics = MasterFabric::orderBy('name')->get();

        $items = [];
        /*$items = Item::where('status', 1)
            ->where('default_second_category', $category->id)
            ->orderBy('activated_at', 'desc')
            ->with('vendor', 'images', 'colors')
            ->paginate(54);*/

        foreach($category->subCategories as &$cat) {
            $count = Item::where('status', 1)->where('default_third_category', $cat->id)->whereIn('vendor_meta_id', $activeVendors)->count();
            $cat->totalItem = $count;
        }

        foreach($vendors as &$vendor) {
            $count = Item::where('status', 1)
                ->where('default_second_category', $category->id)
                ->where('vendor_meta_id', $vendor->id)->count();
            $vendor->totalItem = $count;
        }

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

        return view('pages.sub_category', compact('category', 'vendors', 'masterColors', 'masterFabrics',
            'items', 'wishListItems', 'bodySizes', 'patterns', 'styles'));
    }

    public function catalogPage($category) {
        $category->load('parentCategory');
        $parentCategory = $category->parentCategory;
        $parentCategory->load('subCategories', 'lengths', 'parentCategory');

        $bodySizes = BodySize::where('parent_category_id', $parentCategory->parentCategory->id)
            ->orderBy('name')
            ->get();

        $patterns = Pattern::where('parent_category_id', $parentCategory->parentCategory->id)
            ->orderBy('name')
            ->get();

        $styles = Style::where('sub_category_id', $parentCategory->id)
            ->orderBy('name')
            ->get();

        $vendors = MetaVendor::orderBy('company_name')->where('active', 1)->where('verified', 1)->where('live', 1)->get();
        $masterColors = MasterColor::orderBy('name')->get();
        $masterFabrics = MasterFabric::orderBy('name')->get();

        $items = [];
        /*$items = Item::where('status', 1)
            ->where('default_third_category', $category->id)
            ->orderBy('activated_at', 'desc')
            ->with('vendor', 'images', 'colors')
            ->paginate(30);*/

        foreach($vendors as &$vendor) {
            $count = Item::where('status', 1)
                ->where('default_third_category', $category->id)
                ->where('vendor_meta_id', $vendor->id)->count();
            $vendor->totalItem = $count;
        }

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

        return view('pages.catalog', compact('category', 'parentCategory', 'vendors', 'masterColors',
            'masterFabrics', 'items', 'wishListItems', 'bodySizes', 'patterns', 'styles'));
    }

    public function itemDetailsPage(Item $item, Request $request) {
        $item->load('images', 'vendor', 'pack', 'colors');

        if ($item->vendor->active == 0 || $item->vendor->verified == 0 || $item->vendor->live == 0)
            abort(404);

        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->pluck('id')->toArray();

        // Suggensted items
        $suggestItemsQuery = Item::query();

        if ($item->default_third_category != null || $item->default_third_category != '')
            $suggestItemsQuery->where('default_third_category', $item->default_third_category);
        else if ($item->default_second_category != null || $item->default_second_category != '')
            $suggestItemsQuery->where('default_second_category', $item->default_second_category);
        else
            $suggestItemsQuery->where('default_parent_category', $item->default_parent_category);

        $suggestItemsQuery->whereIn('vendor_meta_id', $activeVendors);
        $suggestItems = $suggestItemsQuery->where('status', 1)->limit(10)->inRandomOrder()->get();

        $sizes = explode("-", $item->pack->name);

        $itemInPack = 0;

        for($i=1; $i <= sizeof($sizes); $i++) {
            $var = 'pack'.$i;

            if ($item->pack->$var != null)
                $itemInPack += (int) $item->pack->$var;
        }

        // Vendor
        $vendor = $item->vendor;
        $vendor->load('parentCategories');

        // Product Count
        $totalItem = 0;
        /*$totalItem = Item::where('status', 1)->where('vendor_meta_id', $vendor->id)->count();
        foreach ($vendor->parentCategories as &$cat) {
            $withSub = [$cat->id];

            foreach ($cat->subCategories as $sub)
                $withSub[] = $sub->id;

            $count = Item::where('vendor_meta_id', $vendor->id)
                ->where('status', 1)
                ->whereHas('categories', function ($query) use ($withSub) {
                    $query->whereIn('id', $withSub);
                })
                ->count();

            $cat->count = $count;

            foreach ($cat->subCategories as &$sub) {
                $count = Item::where('status', 1)
                    ->whereHas('categories', function ($query) use ($sub) {
                        $query->where('id', $sub->id);
                    })
                    ->count();

                $sub->count = $count;
            }
        }*/

        // Small banner
        $smallBannerPath = '';
        $vendorSmallBanner = VendorImage::where('vendor_meta_id', $vendor->id)
            ->where('type', VendorImageType::$MOBILE_MAIN_BANNER)
            ->where('status', 1)->first();

        if ($vendorSmallBanner)
            $smallBannerPath = cdn($vendorSmallBanner->image_path);

        // Wishlist
        $wishlist = WishListItem::where('user_id', Auth::user()->id)
            ->where('item_id', $item->id)
            ->first();

        if ($wishlist)
            $isInWishlist = true;
        else
            $isInWishlist = false;

        // Blocked check
        $blockedVendorIds = Auth::user()->blockedVendorIds();
        if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
            // Banner
            $bannerPath = '';
            $vendorBanner = VendorImage::where('vendor_meta_id', $vendor->id)
                ->where('type', VendorImageType::$HOME_PAGE_BANNER)
                ->where('status', 1)->first();

            if ($vendorBanner)
                $bannerPath = cdn($vendorBanner->image_path);

            // Small banner
            $smallBannerPath = '';
            $vendorSmallBanner = VendorImage::where('vendor_meta_id', $vendor->id)
                ->where('type', VendorImageType::$MOBILE_MAIN_BANNER)
                ->where('status', 1)->first();

            if ($vendorSmallBanner)
                $smallBannerPath = cdn($vendorSmallBanner->image_path);

            $vendor = $item->vendor;

            return view('errors.403', compact('vendor', 'bannerPath', 'smallBannerPath'));
        }

        // Cart
        $cartCount = CartItem::where('user_id', Auth::user()->id)->count();

        // Recently View item
        ItemView::where('user_id', Auth::user()->id)->where('item_id', $item->id)->delete();

        ItemView::create([
            'user_id' => Auth::user()->id,
            'item_id' => $item->id,
        ]);

        $recentlyViewItems = ItemView::where('user_id', Auth::user()->id)
            ->where('created_at', '>', Carbon::parse('-24 hours'))
            ->where('item_id', '!=', $item->id)
            ->orderBy('created_at', 'desc')
            ->with('item')
            ->limit(6)
            ->get();


        // Next Previous Item
        $nextItem = null;
        $previousItem = null;

        // For next
        $temp = Item::where('status', 1)
            ->where('vendor_meta_id', $item->vendor_meta_id)
            ->orderBy('activated_at', 'desc')
            ->get();

        for($i=0; $i <sizeof($temp); $i++) {
            if ($temp[$i]->id == $item->id) {
                if (isset($temp[$i+1])) {
                    $nextItem = $temp[$i+1];
                    break;
                }
            }
        }

        // For previous
        $temp = Item::where('status', 1)
            ->where('vendor_meta_id', $item->vendor_meta_id)
            ->orderBy('activated_at', 'desc')
            ->get();

        for($i=sizeof($temp)-1; $i >= 0; $i--) {
            if ($temp[$i]->id == $item->id) {
                if (isset($temp[$i-1])) {
                    $previousItem = $temp[$i-1];
                    break;
                }
            }
        }

        /*$nextItem = Item::where('status', 1)
            ->where('vendor_meta_id', $item->vendor_meta_id)
            ->where('activated_at', '<=', $item->activated_at)
            ->where('id', '!=', $item->id)
            ->orderBy('activated_at', 'desc')
            ->orderBy('id')
            ->limit(1)
            ->first();*/

        /*$previousItem = Item::where('status', 1)
            ->where('vendor_meta_id', $item->vendor_meta_id)
            ->where('activated_at', '>=', $item->activated_at)
            ->where('id', '!=', $item->id)
            ->limit(1)
            ->orderBy('activated_at')
            ->first();*/

        // Visitor
        Visitor::create([
            'url' => url()->current(),
            'ip' => $request->ip(),
            'user_id' => Auth::user()->id,
            'route' => $request->route()->getName(),
            'vendor_meta_id' => $item->vendor_meta_id
        ]);

        return view('pages.item_details', compact('item', 'suggestItems', 'sizes', 'itemInPack', 'vendor',
            'smallBannerPath', 'isInWishlist', 'totalItem', 'cartCount', 'recentlyViewItems', 'nextItem', 'previousItem'));
    }

    public function getItemsSubCategory(Request $request) {
        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();
        $query = Item::query();

        $query->where('status', 1);
        $query->whereIn('vendor_meta_id', $activeVendors);

        if ($request->secondCategory && $request->secondCategory != '')
            $query->where('default_second_category', $request->secondCategory);

        if ($request->categories && sizeof($request->categories) > 0)
            $query->whereIn('default_third_category', $request->categories);

        if ($request->vendors && sizeof($request->vendors) > 0)
            $query->whereIn('vendor_meta_id', $request->vendors);


        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        if ($request->bodySizes && sizeof($request->bodySizes) > 0)
            $query->whereIn('body_size_id', $request->bodySizes);

        if ($request->patterns && sizeof($request->patterns) > 0)
            $query->whereIn('pattern_id', $request->patterns);

        if ($request->lengths && sizeof($request->lengths) > 0)
            $query->whereIn('length_id', $request->lengths);

        if ($request->styles && sizeof($request->styles) > 0)
            $query->whereIn('style_id', $request->styles);

        if ($request->fabrics && sizeof($request->fabrics) > 0)
            $query->whereIn('master_fabric_id', $request->fabrics);

        // Search
        if ($request->searchText && $request->searchText != ''){
            if ($request->searchOption == '1')
                $query->where('style_no', 'like','%'.$request->searchText.'%');
            if ($request->searchOption == '2')
                $query->where('description', 'like','%'.$request->searchText.'%');
        }

        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=',$request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=',$request->priceMax);



        // Sorting
        if ($request->sorting){
            if ($request->sorting == '1')
                $query->orderBy('activated_at', 'desc');
            else if ($request->sorting == '2')
                $query->orderBy('price');
            else if ($request->sorting == '3')
                $query->orderBy('price', 'desc');
            else if ($request->sorting == '4')
                $query->orderBy('style_no');
        }



        /*$query = Item::where('status', 1)
            ->whereIn('default_third_category', $request->categories)
            ->whereIn('vendor_meta_id', $request->vendors)
            ->whereHas('colors', function ($query) use($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            })
            ->with('images', 'vendor')
            ->query();*/

        $items = $query->with('images', 'vendor', 'colors')->paginate(54);
        $paginationView = $items->links('others.pagination');

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

        $blockedVendorIds = Auth::user()->blockedVendorIds();

        $results = [];

        foreach($items as $item) {
            $tmp = null;
            $tmp['id'] = $item->id;
            $tmp['price_text'] = '';
            $tmp['availability'] = $item->availability;
            $tmp['available_on'] = $item->available_on;

            // Price
            $price = '';

            /*if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }*/

            // Image
            $imagePath = '';
            $price = '';

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                $imagePath = cdn('images/no-image.png');

                if (sizeof($item->images) > 0)
                    $imagePath = cdn($item->images[0]->list_image_path);

                //$item->price_text = '$'.sprintf('%0.2f', $item->price);

            } else {
                $imagePath = cdn('images/no-image.jpg');
                //$item->price_text = '';
            }


            $tmp['imagePath'] = $imagePath;
            $tmp['detailsUrl'] = route('item_details_page', ['item' => $item->id]);
            $tmp['vendorUrl'] = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);
            //$item->imagePath = $imagePath;
            //$item->detailsUrl = route('item_details_page', ['item' => $item->id]);
            //$item->vendorUrl = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);

            $wishListButton = '';
            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if (in_array($item->id, $wishListItems)) {
                    $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="'.$item->id.'"><i class="icon-heart"></i></button>';
                } else {
                    $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="'.$item->id.'"><i class="icon-heart"></i></button>';
                }

                /*if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');*/

                if ($item->getOriginal('orig_price') != null)
                    $price .= '<del>$' . number_format($item->getOriginal('orig_price'), 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->getOriginal('price'), 2, '.', '');
            }

            $tmp['price'] = $price;
            $tmp['wishListButton'] = $wishListButton;
            $tmp['company_name'] = $item->vendor->company_name;
            //$item->wishListButton = $wishListButton;
            //$item->price_text = $price;

            if (sizeof($item->colors) > 1)
                $tmp['multiColor'] = true;
            else
                $tmp['multiColor'] = false;

            // Blocked Check
            if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
                $tmp['imagePath'] = cdn('images/blocked.jpg');
                $tmp['company_name'] = '';
                $tmp['vendorUrl'] = '';
                $tmp['style_no'] = '';
                $tmp['available_on'] = '';
                $tmp['multiColor'] = false;
                $tmp['detailsUrl'] = '';
                $tmp['price'] = '';

                //$item->imagePath = cdn('images/blocked.jpg');
                //$item->vendor->company_name = '';
                //$item->vendorUrl = '';
                //$item->style_no = '';
                //$item->price = 0;
                //$item->price_text = '';
                //$item->available_on = '';
                //$item->colors->splice(0);
            }

            $results[] = $tmp;
        }

        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        return ['items' => $results, 'pagination' => $paginationView, 'total' => $items->total()];
    }

    public function vendorPage($vendor) {
        $vendor->load('parentCategories');

        // Visitor
        Visitor::create([
            'user_id' => (Auth::check() && Auth::user()->role == Role::$BUYER) ? Auth::user()->id : null,
            'url' => url()->current(),
            'ip' => request()->ip(),
        ]);

        // Product Count
        $totalItem = 0;
        /*$totalItem = Item::where('status', 1)->where('vendor_meta_id', $vendor->id)->count();

        foreach ($vendor->parentCategories as &$cat) {
            $withSub = [$cat->id];

            foreach ($cat->subCategories as $sub)
                $withSub[] = $sub->id;

            $count = 0;

            $cat->count = $count;

            foreach ($cat->subCategories as &$sub) {
                $count = 0;

                $sub->count = $count;
            }
        }*/

        // Best Selling Products
        $bestItemIds = BestItem::where('vendor_meta_id', $vendor->id)->orderBy('sort')->pluck('item_id')->toArray();

        if (sizeof($bestItemIds) == 0) {
            $sql = "SELECT items.id, items.name, items.style_no, items.price, t.count, items.available_on, items.availability
                FROM items 
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items GROUP BY item_id) t ON items.id = t.item_id 
                WHERE items.deleted_at IS NULL AND items.vendor_meta_id = " . $vendor->id . " AND items.status = 1
                ORDER BY count DESC
                LIMIT 6";

            $bestItems = DB::select($sql);

            $bestItemIds = [];
            foreach ($bestItems as $item)
                $bestItemIds[] = $item->id;
        }

        $bestItems = [];
        if (sizeof($bestItemIds) > 0) {
            $bestItems = Item::whereIn('id', $bestItemIds)
                ->with('images', 'vendor', 'colors')
                ->where('status', 1)
                ->limit(6)
                ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $bestItemIds) . " )"))
                ->get();
        }

        // Latest Items
        $newestItems = Item::where('vendor_meta_id', $vendor->id)
            ->where('status', 1)
            ->with('vendor', 'images', 'colors')
            ->orderBy('activated_at', 'desc')
            ->limit(24)
            ->get();

        // Small banner
        $smallBannerPath = '';
        $vendorSmallBanner = VendorImage::where('vendor_meta_id', $vendor->id)
            ->where('type', VendorImageType::$MOBILE_MAIN_BANNER)
            ->where('status', 1)->first();

        if ($vendorSmallBanner)
            $smallBannerPath = cdn($vendorSmallBanner->image_path);

        // Banner
        $bannerPath = '';
        $vendorBanner = VendorImage::where('vendor_meta_id', $vendor->id)
            ->where('type', VendorImageType::$HOME_PAGE_BANNER)
            ->where('status', 1)->first();

        if ($vendorBanner)
            $bannerPath = cdn($vendorBanner->image_path);

        // Rating
        $rating = Review::where('meta_vendor_id', $vendor->id)->avg('star');
        $reviews = Review::where('meta_vendor_id', $vendor->id)
            ->with('user')
            ->orderBy('created_at', 'desc')
            ->get();

        $totalReviews = sizeof($reviews);

        $star = [0, 0, 0, 0, 0];

        foreach ($reviews as $review) {
            if ($review->star != 0)
                $star[$review->star - 1]++;
        }


        $orders = [];

        if (Auth::check() && Auth::user()->role == Role::$BUYER) {
            $orders = DB::table('orders')
                ->select('orders.id', 'order_number', 'meta_vendors.company_name', 'vendor_meta_id', 'review', 'star', 'orders.created_at')
                ->join('meta_vendors', 'meta_vendors.id', '=', 'orders.vendor_meta_id')
                ->leftJoin('reviews', 'orders.id', '=', 'reviews.order_id')
                ->where('orders.user_id', Auth::user()->id)
                ->where('orders.deleted_at', NULL)
                ->where('orders.vendor_meta_id', $vendor->id)
                ->orderBy('orders.created_at', 'desc')
                ->get();
        }

        // Blocked check
        $blockedVendorIds = [];

        if (Auth::check())
            $blockedVendorIds = Auth::user()->blockedVendorIds();

        if (in_array($vendor->id, $blockedVendorIds)) {
            return view('errors.403', compact('vendor', 'bannerPath', 'smallBannerPath'));
        }


        return view('pages.vendor', compact('vendor', 'smallBannerPath', 'bannerPath', 'bestItems', 'newestItems',
            'rating', 'totalReviews', 'reviews', 'star', 'orders', 'totalItem'));
    }

    public function vendorCategoryPage(Category $category) {
        $category->load('vendor');

        if (Auth::check() && Auth::user()->vendor_meta_id == $category->vendor->id && (Auth::user()->role == Role::$VENDOR || Auth::user()->role == Role::$VENDOR_EMPLOYEE)) {
            //$activeVendors[] = Auth::user()->vendor_meta_id;
        } else {
            if ($category->vendor->active == 0 || $category->vendor->verified == 0 || $category->vendor->live == 0)
                abort(404);
        }


        $categories = Category::where('vendor_meta_id', $category->vendor_meta_id)
            ->where('parent', 0)
            ->with('subCategories')
            ->orderBy('sort')
            ->get();

        // Logo
        $logoPath = '';
        $vendorLogo = VendorImage::where('vendor_meta_id', $category->vendor->id)
            ->where('type', VendorImageType::$LOGO)
            ->where('status', 1)->first();

        if ($vendorLogo)
            $logoPath = cdn($vendorLogo->image_path);

        // Product Count
        $totalItem = 0;
        /*$totalItem = Item::where('status', 1)->where('vendor_meta_id', $category->vendor_meta_id)->count();
        foreach ($categories as &$cat) {
            $withSub = [$cat->id];

            foreach ($cat->subCategories as $sub)
                $withSub[] = $sub->id;

            $count = Item::where('vendor_meta_id', $cat->vendor_meta_id)
                ->where('status', 1)
                ->whereHas('categories', function ($query) use ($withSub) {
                    $query->whereIn('id', $withSub);
                })
                ->count();

            $cat->count = $count;

            foreach ($cat->subCategories as &$sub) {
                $count = Item::where('status', 1)
                    ->whereHas('categories', function ($query) use ($sub) {
                        $query->where('id', $sub->id);
                    })
                    ->count();

                $sub->count = $count;
            }
        }*/

        // Banner
        $vendor = $category->vendor;
        $bannerPath = '';
        $vendorBanner = VendorImage::where('vendor_meta_id', $vendor->id)
            ->where('type', VendorImageType::$HOME_PAGE_BANNER)
            ->where('status', 1)->first();

        if ($vendorBanner)
            $bannerPath = cdn($vendorBanner->image_path);

        return view('pages.vendor_category', compact('category', 'categories', 'logoPath', 'vendor', 'bannerPath', 'totalItem'));
    }

    public function vendorCategoryAllPage(MetaVendor $vendor) {
        if (Auth::check() && Auth::user()->vendor_meta_id == $vendor->id && (Auth::user()->role == Role::$VENDOR || Auth::user()->role == Role::$VENDOR_EMPLOYEE)) {
            //$activeVendors[] = Auth::user()->vendor_meta_id;
        } else {
            if ($vendor->active == 0 || $vendor->verified == 0 || $vendor->live == 0)
                abort(404);
        }


        $categories = Category::where('vendor_meta_id', $vendor->id)
            ->where('parent', 0)
            ->orderBy('sort')
            ->get();

        // Logo
        $logoPath = '';
        $vendorLogo = VendorImage::where('vendor_meta_id', $vendor->id)
            ->where('type', VendorImageType::$LOGO)
            ->where('status', 1)->first();

        if ($vendorLogo)
            $logoPath = cdn($vendorLogo->image_path);

        // Product Count
        $totalItem = Item::where('status', 1)->where('vendor_meta_id', $vendor->id)->count();
        foreach ($categories as &$cat) {
            $withSub = [$cat->id];

            foreach ($cat->subCategories as $sub)
                $withSub[] = $sub->id;

            $count = Item::where('vendor_meta_id', $cat->vendor_meta_id)
                ->where('status', 1)
                ->whereHas('categories', function ($query) use ($withSub) {
                    $query->whereIn('id', $withSub);
                })
                ->count();

            $cat->count = $count;

            foreach ($cat->subCategories as &$sub) {
                $count = Item::where('status', 1)
                    ->whereHas('categories', function ($query) use ($sub) {
                        $query->where('id', $sub->id);
                    })
                    ->count();

                $sub->count = $count;
            }
        }

        // Banner
        $bannerPath = '';
        $vendorBanner = VendorImage::where('vendor_meta_id', $vendor->id)
            ->where('type', VendorImageType::$HOME_PAGE_BANNER)
            ->where('status', 1)->first();

        if ($vendorBanner)
            $bannerPath = cdn($vendorBanner->image_path);

        return view('pages.vendor_category', compact( 'items', 'categories', 'logoPath', 'vendor', 'bannerPath', 'totalItem'));
    }

    public function vendorCategoryGetItems(Request $request) {
        $query = Item::query();
        $query->where('vendor_meta_id', $request->vendor);

        if ($request->category) {
            $category = Category::where('id', $request->category)->with('subCategories')->first();

            $withSub = [$category->id];
            foreach ($category->subCategories as $sub)
                $withSub[] = $sub->id;

            $query->whereHas('categories', function ($query) use ($withSub) {
                $query->whereIn('id', $withSub);
            });
        }

        $query->where('status', 1);

        // Search
        if ($request->searchText && $request->searchText != ''){
            if ($request->searchOption == '1')
                $query->where('style_no', 'like','%'.$request->searchText.'%');
            if ($request->searchOption == '2')
                $query->where('description', 'like','%'.$request->searchText.'%');
        }

        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=',$request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=',$request->priceMax);

        // Sorting
        if ($request->sorting){
            if ($request->sorting == '1')
                $query->orderBy('activated_at', 'desc');
            else if ($request->sorting == '2')
                $query->orderBy('price');
            else if ($request->sorting == '3')
                $query->orderBy('price', 'desc');
            else if ($request->sorting == '4')
                $query->orderBy('style_no');
        }

        $items = $query->with('images', 'vendor', 'colors')->paginate(42);
        $paginationView = $items->links('others.pagination');
        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        // Blocked check
        $blockedVendorIds = [];

        if (Auth::check())
            $blockedVendorIds = Auth::user()->blockedVendorIds();

        $results = [];

        foreach($items as &$item) {
            $tmp = null;
            $tmp['id'] = $item->id;
            $tmp['price_text'] = '';
            $tmp['availability'] = $item->availability;
            $tmp['available_on'] = $item->available_on;

            // Price
            $price = '';

            if ($item->getOriginal('orig_price') != null)
                $price .= '<del>$' . number_format($item->getOriginal('orig_price'), 2, '.', '') . '</del> ';

            $price .= '$' . number_format($item->getOriginal('price'), 2, '.', '');

            /*if ($item->orig_price != null)
                $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';


            $price .= '$' . number_format($item->price, 2, '.', '');*/

            $imagePath = cdn('images/no-image.png');

            if (sizeof($item->images) > 0)
                $imagePath = cdn($item->images[0]->list_image_path);

            $tmp['imagePath'] = $imagePath;
            $tmp['detailsUrl'] = route('item_details_page', ['item' => $item->id]);
            $tmp['vendorUrl'] = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);
            $tmp['price'] = $price;
            $tmp['company_name'] = $item->vendor->company_name;

            if (sizeof($item->colors) > 1)
                $tmp['multiColor'] = true;
            else
                $tmp['multiColor'] = false;

            //$item->imagePath = $imagePath;
            //$item->detailsUrl = route('item_details_page', ['item' => $item->id]);
            //$item->price_text = $price;

            // Blocked Check
            if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
                $tmp['imagePath'] = cdn('images/blocked.jpg');
                $tmp['company_name'] = '';
                $tmp['vendorUrl'] = '';
                $tmp['style_no'] = '';
                $tmp['available_on'] = '';
                $tmp['multiColor'] = false;
                $tmp['detailsUrl'] = '';
                $tmp['price'] = '';

                /*$item->imagePath = cdn('images/blocked.jpg');
                $item->vendor->company_name = '';
                $item->vendorUrl = '';
                $item->style_no = '';
                $item->price_text = '';
                $item->price = 0;
                $item->available_on = '';
                $item->colors->splice(0);*/
            }

            $results[] = $tmp;
        }


        return ['items' => $results, 'pagination' => $paginationView, 'total' => $items->total()];
    }

    public function allVendorPage(Request $request) {
        $query = MetaVendor::query();
        $query->where('active', 1);
        $query->where('verified', 1);
        $query->where('live', 1);
        $query->orderBy('activated_at', 'desc');
        $appends = [];


        if ($request->s && $request->s != '') {
            $query->where('company_name', 'like', $request->s . '%');
            $appends['s'] = $request->s;
        }

        if ($request->search && $request->search != '') {
            $query->where('company_name', 'like', '%' . $request->search . '%');
            $appends['search'] =  $request->search;
        }

        $query->with('coupons');

        $vendors = $query->with(['images' => function($q) {
            return $q->where('type', VendorImageType::$MOBILE_MAIN_BANNER)->where('status', 1);
        }])->paginate(10);

        // Top Banner
        $topBannerUrl = '';
        $topBannerVendor = '';
        $bannerVendor = CategoryBanner::where('type', CategoryBannerType::$TOP_BANNER)
            ->where('category_id', -1)
            ->first();

        if ($bannerVendor) {
            $banner = VendorImage::where('type', VendorImageType::$TOP_BANNER)
                ->where('vendor_meta_id', $bannerVendor->vendor_id)
                ->where('status', 1)
                ->with('vendor')
                ->first();

            if ($banner) {
                $topBannerVendor = $banner->vendor->company_name;
                $topBannerUrl = cdn($banner->image_path);
            }
        }

        return view('pages.vendor_all', compact('vendors', 'appends', 'topBannerUrl', 'topBannerVendor'))->with('page_title', 'All Vendor List');
    }

    public function checkVendorOrCategory($text) {
        $category = null;
        $categories = DefaultCategory::where('parent', 0)
            ->get();

        foreach ($categories as $cat) {
            if (changeSpecialChar($cat->name) == $text) {
                $category = $cat;
                break;
            }
        }

        if ($category)
            return $this->categoryPage($category);

        $vendor = null;

        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();

        if (Auth::check() && (Auth::user()->role == Role::$VENDOR || Auth::user()->role == Role::$VENDOR_EMPLOYEE)) {
            $activeVendors[] = Auth::user()->vendor_meta_id;
        }

        $vendors = MetaVendor::whereIn('id', $activeVendors)->get();

        foreach ($vendors as $v) {
            if (changeSpecialChar($v->company_name) == $text) {
                $vendor = $v;
                break;
            }
        }

        if ($vendor)
            return $this->vendorPage($vendor);

        abort(404);
    }

    public function secondCategory($parent, $category) {
        $categories = DefaultCategory::where('parent', '!=', 0)
            ->get();

        $c = null;

        foreach ($categories as $cat) {
            if (changeSpecialChar($cat->name) == $category) {
                if ($cat->parentCategory && changeSpecialChar($cat->parentCategory->name) == $parent) {
                    $c = $cat;
                    break;
                }
            }
        }

        if ($c) {
            return $this->subCategoryPage($c);
        }

        abort(404);
    }

    public function thirdCategory($parent, $second, $category) {
        $categories = DefaultCategory::where('parent', '!=', 0)
            ->get();

        $c = null;

        foreach ($categories as $cat) {
            if (changeSpecialChar($cat->name) == $category) {
                if ($cat->parentCategory && changeSpecialChar($cat->parentCategory->name) == $second) {
                    if ($cat->parentCategory->parentCategory && changeSpecialChar($cat->parentCategory->parentCategory->name) == $parent) {
                        $c = $cat;
                        break;
                    }
                }
            }
        }

        if ($c) {
            return $this->catalogPage($c);
        }

        abort(404);
    }

    public function searchPage(Request $request) {
        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();

        $items = Item::where('status', 1)
            ->whereIn('vendor_meta_id', $activeVendors)
            ->where(function ($query) use($request) {
                $query->where('style_no', 'like', '%'.$request->s.'%')
                    ->orWhere('name', 'like', '%'.$request->s.'%')
                    ->orWhere('description', 'like', '%'.$request->s.'%');
            })
            ->with('images')->paginate(30);

        return view('pages.search', compact('items'));
    }

    public function getMessageFile($name) {
        $f = MessageFile::where('filename', '=', $name)->firstOrFail();

        return Storage::disk('message_files')->download($name, $f->original_filename);
    }

    public function getItemPrice(Request $request) {
        $item = Item::where('id', $request->id)->first();
        $price = '';

        if ($item && Auth::check() && Auth::user()->role == Role::$BUYER) {
            if ($item->orig_price != null)
                $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

            $price .= '$' . number_format($item->price, 2, '.', '');
        }

        return $price;
    }
}
