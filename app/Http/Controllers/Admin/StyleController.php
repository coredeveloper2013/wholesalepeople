<?php

namespace App\Http\Controllers\Admin;

use App\Model\DefaultCategory;
use App\Model\Style;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StyleController extends Controller
{
    public function index() {
        $styles = Style::orderBy('name')->with('category')->get();

        // Default Categories
        $defaultCategories = [];

        $categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name
                        ];

                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        return view('admin.style.index', compact('styles', 'defaultCategories'))
            ->with('page_title', 'Style');
    }

    public function addPost(Request $request) {
        Style::create([
            'name' => $request->name,
            'sub_category_id' => $request->d_second_parent_category,
        ]);

        return redirect()->route('admin_style')->with('message', 'Style Added!');
    }

    public function editPost(Request $request) {
        $style = Style::where('id', $request->id)->first();

        $style->name = $request->name;
        $style->sub_category_id = $request->d_second_parent_category;
        $style->save();

        return redirect()->route('admin_style')->with('message', 'Style Updated!');
    }

    public function delete(Request $request) {
        $size = Style::where('id', $request->id)->first();
        $size->delete();
    }
}
