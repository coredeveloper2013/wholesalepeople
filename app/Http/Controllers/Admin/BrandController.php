<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\Role;
use App\Model\CartItem;
use App\Model\Country;
use App\Model\Item;
use App\Model\ItemView;
use App\Model\MetaVendor;
use App\Model\State;
use App\Model\User;
use App\Model\WishListItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use File;

class BrandController extends Controller
{
    public function all() {
        $vendors = MetaVendor::all();

        return view('admin.brand.all', compact('vendors'))->with('page_title', 'All Brand');
    }

    public function changeStatus(Request $request) {
        $vendor = MetaVendor::where('id', $request->id)->first();
        $vendor->active = $request->status;

        if ($request->status == '1')
            $vendor->activated_at = Carbon::now()->toDateTimeString();
        else {
            $itemIds = Item::where('vendor_meta_id', $vendor->id)->pluck('id')->toArray();

            CartItem::whereIn('item_id', $itemIds)->delete();
            WishListItem::whereIn('item_id', $itemIds)->delete();
            ItemView::whereIn('item_id', $itemIds)->delete();

            $vendor->activated_at = null;
        }

        $vendor->save();
    }

    public function changeVerified(Request $request) {
        $vendor = MetaVendor::where('id', $request->id)->first();
        $vendor->verified = $request->status;

        if ($request->status == '1')
            $vendor->verified_at = Carbon::now()->toDateTimeString();
        else {
            $itemIds = Item::where('vendor_meta_id', $vendor->id)->pluck('id')->toArray();

            CartItem::whereIn('item_id', $itemIds)->delete();
            WishListItem::whereIn('item_id', $itemIds)->delete();
            ItemView::whereIn('item_id', $itemIds)->delete();

            $vendor->verified_at = null;
        }

        $vendor->save();
    }

    public function changeMainSlider(Request $request) {
        $vendor = MetaVendor::where('id', $request->id)->first();
        $vendor->show_in_main_slider = $request->status;

        if ($request->status == '1')
            $vendor->main_slider_at = Carbon::now()->toDateTimeString();

        $vendor->save();
    }

    public function changeMobileMainSlider(Request $request) {
        $vendor = MetaVendor::where('id', $request->id)->first();
        $vendor->show_in_mobile_main_slider = $request->status;
        $vendor->save();
    }

    public function changeFeatureVendor(Request $request) {
        $vendor = MetaVendor::where('id', $request->id)->first();
        $vendor->feature_vendor = $request->status;
        $vendor->save();
    }

    public function addBrand() {
        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();

        return view('admin.brand.add', compact('countries', 'usStates', 'caStates'))->with('page_title', 'Create Brand');
    }

    public function addBrandPost(Request $request) {
        $messages = [
            'required' => 'This field is required.',
        ];

        $rules = [
            'companyName' => 'required|string|max:255',
            'firstName' => 'required|string|max:255',
            'userId' => 'required|string|max:255|unique:users,user_id',
            'website' => 'nullable|string|max:255',
            'businessName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'password' => 'required|string|min:6',
            'email' => 'required|string|email|max:255|unique:users',
            'address' => 'required|string|max:255',
            'unit' => 'nullable|string|max:255',
            'city' => 'required|string|max:255',
            'zipCode' => 'required|string|max:255',
            'country' => 'required',
            'phone' => 'required|max:255',
            'fax' => 'nullable|max:255',
            'factoryAddress' => 'required|string|max:255',
            'factoryUnit' => 'nullable|string|max:255',
            'factoryCity' => 'required|string|max:255',
            'factoryZipCode' => 'required|string|max:255',
            'factoryCountry' => 'required',
            'factoryPhone' => 'required|max:255',
            'factoryEveningPhone' => 'nullable|max:255',
            'factoryAlternatePhone' => 'nullable|max:255',
            'factoryFax' => 'nullable|max:255',
            'companyInformation' => 'nullable|max:1000',
            'hearAboutUs' => 'required',
        ];

        if ($request->location == "INT")
            $rules['state'] = 'required|string|max:255';
        else
            $rules['stateSelect'] = 'required';

        if ($request->factoryLocation == "INT")
            $rules['factoryState'] = 'required|string|max:255';
        else
            $rules['factoryStateSelect'] = 'required';

        if ($request->hearAboutUs && $request->hearAboutUs == "other")
            $rules['hearAboutUsOtherText'] = 'required|string|max:255';

        $request->validate($rules, $messages);


        $state_id = null;
        $state = null;
        $factory_state_id = null;
        $factory_state = null;
        $hearFromOtherText = null;

        if ($request->location == "INT")
            $state = $request->state;
        else
            $state_id = $request->stateSelect;

        if ($request->factoryLocation == "INT")
            $factory_state = $request->factoryState;
        else
            $factory_state_id = $request->factoryStateSelect;

        if ($request->hearAboutUs && $request->hearAboutUs == "other")
            $hearFromOtherText = $request->hearAboutUsOtherText;

        $meta = MetaVendor::create([
            'company_name' => $request->companyName,
            'business_name' => $request->businessName,
            'website' => $request->website,
            'billing_location' => $request->location,
            'billing_address' => $request->address,
            'billing_unit' => $request->unit,
            'billing_city' => $request->city,
            'billing_state_id' => $state_id,
            'billing_state' => $state,
            'billing_zip' => $request->zipCode,
            'billing_country_id' => $request->country,
            'billing_phone' => $request->phone,
            'billing_fax' => $request->fax,
            'billing_commercial' => ($request->showroomCommercial == null) ? 0 : 1,
            'factory_location' => $request->factoryLocation,
            'factory_address' => $request->factoryAddress,
            'factory_unit' => $request->factoryUnit,
            'factory_city' => $request->factoryCity,
            'factory_state_id' => $factory_state_id,
            'factory_state' => $factory_state,
            'factory_zip' => $request->factoryZipCode,
            'factory_country_id' => $request->factoryCountry,
            'factory_phone' => $request->factoryPhone,
            'factory_alternate_phone' => $request->factoryAlternatePhone,
            'factory_evening_phone' => $request->factoryEveningPhone,
            'factory_fax' => $request->factoryFax,
            'factory_commercial' => ($request->factoryCommercial == null) ? 0 : 1,
            'company_info' => $request->companyInformation,
            'hear_about_us' => $request->hearAboutUs,
            'hear_about_us_other' => $hearFromOtherText,
            'receive_offers' => $request->receiveSpecialOffers,
            'verified' => 0,
            'active' => 0
        ]);

        $user = User::create([
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'email' => $request->email,
            'user_id' => $request->userId,
            'password' => Hash::make($request->password),
            'role' => Role::$VENDOR,
            'vendor_meta_id' => $meta->id,
        ]);

        File::makeDirectory(public_path('images/vendors/'.$meta->id));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/list'));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/original'));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/thumbs'));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/colors'));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/colors/thumbs'));

        return redirect()->route('admin_brand_all');
    }

    public function activeBrand() {
        $vendors = MetaVendor::where('active', 1)->get();

        return view('admin.brand.active', compact('vendors'))->with('page_title', 'Active Brand');
    }

    public function deactivateBrand() {
        $vendors = MetaVendor::where('active', 0)->get();

        return view('admin.brand.deactivate', compact('vendors'))->with('page_title', 'Deactivate Brand');
    }

    public function pendingBrand() {
        $vendors = MetaVendor::where('verified', 0)->get();

        return view('admin.brand.pending', compact('vendors'))->with('page_title', 'Pending Brand');
    }

    public function changeVerifiedStatus(Request $request) {
        $vendor = MetaVendor::where('id', $request->id)->first();
        $vendor->active = $request->status;
        $vendor->verified = $request->status;
        $vendor->save();
    }

    public function changeLive(Request $request) {
        $vendor = MetaVendor::where('id', $request->id)->first();
        $vendor->live = $request->status;

        if ($request->status == '0') {
            $itemIds = Item::where('vendor_meta_id', $vendor->id)->pluck('id')->toArray();

            CartItem::whereIn('item_id', $itemIds)->delete();
            WishListItem::whereIn('item_id', $itemIds)->delete();
            ItemView::whereIn('item_id', $itemIds)->delete();
        }

        $vendor->save();
    }

    public function edit(MetaVendor $vendor) {
        $vendor->load('user');

        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();

        return view('admin.brand.edit', compact('countries', 'usStates', 'caStates', 'vendor'))->with('page_title', 'Edit Brand');
    }

    public function editPost(MetaVendor $vendor, Request $request) {
        $messages = [
            'required' => 'This field is required.',
        ];

        $rules = [
            'companyName' => 'required|string|max:255',
            'firstName' => 'required|string|max:255',
            'userId' => 'required|string|max:255|unique:users,user_id,'.$vendor->user->id,
            'website' => 'nullable|string|max:255',
            'businessName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'password' => 'nullable|string|min:6',
            'email' => 'required|string|email|max:255|unique:users,email,'.$vendor->user->id,
            'address' => 'required|string|max:255',
            'unit' => 'nullable|string|max:255',
            'city' => 'required|string|max:255',
            'zipCode' => 'required|string|max:255',
            'country' => 'required',
            'phone' => 'required|max:255',
            'fax' => 'nullable|max:255',
            'factoryAddress' => 'required|string|max:255',
            'factoryUnit' => 'nullable|string|max:255',
            'factoryCity' => 'required|string|max:255',
            'factoryZipCode' => 'required|string|max:255',
            'factoryCountry' => 'required',
            'factoryPhone' => 'required|max:255',
            'factoryEveningPhone' => 'nullable|max:255',
            'factoryAlternatePhone' => 'nullable|max:255',
            'factoryFax' => 'nullable|max:255',
            'companyInformation' => 'nullable|max:1000',
            'hearAboutUs' => 'required',
        ];

        if ($request->location == "INT")
            $rules['state'] = 'required|string|max:255';
        else
            $rules['stateSelect'] = 'required';

        if ($request->factoryLocation == "INT")
            $rules['factoryState'] = 'required|string|max:255';
        else
            $rules['factoryStateSelect'] = 'required';

        if ($request->hearAboutUs && $request->hearAboutUs == "other")
            $rules['hearAboutUsOtherText'] = 'required|string|max:255';

        $request->validate($rules, $messages);

        $state_id = null;
        $state = null;
        $factory_state_id = null;
        $factory_state = null;
        $hearFromOtherText = null;

        if ($request->location == "INT")
            $state = $request->state;
        else
            $state_id = $request->stateSelect;

        if ($request->factoryLocation == "INT")
            $factory_state = $request->factoryState;
        else
            $factory_state_id = $request->factoryStateSelect;

        if ($request->hearAboutUs && $request->hearAboutUs == "other")
            $hearFromOtherText = $request->hearAboutUsOtherText;


        $vendor->company_name = $request->companyName;
        $vendor->business_name = $request->businessName;
        $vendor->website = $request->website;
        $vendor->billing_address = $request->address;
        $vendor->billing_unit = $request->unit;
        $vendor->billing_city = $request->city;
        $vendor->billing_state_id = $state_id;
        $vendor->billing_state = $state;
        $vendor->billing_zip = $request->zipCode;
        $vendor->billing_country_id = $request->country;
        $vendor->billing_phone = $request->phone;
        $vendor->billing_fax = $request->fax;
        $vendor->billing_commercial = ($request->showroomCommercial == null) ? 0 : 1;
        $vendor->factory_address = $request->factoryAddress;
        $vendor->factory_unit = $request->factoryUnit;
        $vendor->factory_city = $request->factoryCity;
        $vendor->factory_state_id = $factory_state_id;
        $vendor->factory_state = $factory_state;
        $vendor->factory_zip = $request->factoryZipCode;
        $vendor->factory_country_id = $request->factoryCountry;
        $vendor->factory_phone = $request->factoryPhone;
        $vendor->factory_alternate_phone = $request->factoryAlternatePhone;
        $vendor->factory_evening_phone = $request->factoryEveningPhone;
        $vendor->factory_fax = $request->factoryFax;
        $vendor->factory_commercial = ($request->factoryCommercial == null) ? 0 : 1;
        $vendor->company_info = $request->companyInformation;
        $vendor->hear_about_us = $request->hearAboutUs;
        $vendor->hear_about_us_other = $hearFromOtherText;
        $vendor->receive_offers = $request->receiveSpecialOffers;
        $vendor->verified = 0;
        $vendor->active = 0;

        $vendor->save();

        $vendor->user->first_name = $request->firstName;
        $vendor->user->last_name = $request->lastName;
        $vendor->user->email = $request->email;
        $vendor->user->user_id = $request->userId;

        if ($request->password && $request->password != '')
            $vendor->user->password = Hash::make($request->password);

        $vendor->user->save();

        return redirect()->route('admin_brand_all');
    }

    public function delete(Request $request) {
        MetaVendor::where('id', $request->id)->delete();
    }
}
