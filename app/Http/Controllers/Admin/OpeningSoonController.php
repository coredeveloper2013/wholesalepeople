<?php

namespace App\Http\Controllers\Admin;

use App\Model\OpeningSoon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uuid;

class OpeningSoonController extends Controller
{
    public function index() {
        $os = OpeningSoon::orderBy('sort')->paginate(10);

        return view('admin.opening_soon.index', compact('os'))->with('page_title', 'Opening Soon');
    }

    public function addPost(Request $request) {
        $request->validate([
            'name' => 'required',
            'sort' => 'nullable|integer',
            'photo' => 'nullable|mimes:jpeg,jpg,png'
        ]);

        $imagePath = null;
        $thumbsPath = null;

        if ($request->photo) {
            $filename = Uuid::generate()->string;
            $file = $request->file('photo');
            $ext = $file->getClientOriginalExtension();

            $destinationPath = 'images/opening_soon';

            $file->move(public_path($destinationPath), $filename.".".$ext);

            $imagePath = $destinationPath."/".$filename.".".$ext;
        }


        OpeningSoon::create([
            'name' => $request->name,
            'sort' => $request->sort,
            'description' => $request->description,
            'image_path' => $imagePath
        ]);

        return redirect()->route('admin_opening_soon')->with('message', 'Added!');
    }

    public function editPost(Request $request) {
        $request->validate([
            'name' => 'required',
            'sort' => 'nullable|integer',
            'photo' => 'nullable|mimes:jpeg,jpg,png'
        ]);

        $imagePath = null;
        $thumbsPath = null;

        $item = OpeningSoon::where('id', $request->osId)->first();

        if ($request->photo) {
            if ($item->image_path != null)
                unlink($item->image_path);

            $filename = Uuid::generate()->string;
            $file = $request->file('photo');
            $ext = $file->getClientOriginalExtension();

            $destinationPath = 'images/opening_soon';

            $file->move(public_path($destinationPath), $filename.".".$ext);

            $imagePath = $destinationPath."/".$filename.".".$ext;
            $item->image_path = $imagePath;
        }


        $item->name = $request->name;
        $item->sort = $request->sort;
        $item->description = $request->description;
        $item->save();

        return redirect()->route('admin_opening_soon')->with('message', 'Updated!');
    }

    public function delete(Request $request) {
        $os = OpeningSoon::where('id', $request->id)->first();

        if ($os->image_path != null)
            unlink($os->image_path);

        $os->delete();
    }
}
