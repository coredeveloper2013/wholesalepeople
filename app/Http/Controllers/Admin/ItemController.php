<?php

namespace App\Http\Controllers\Admin;

use App\Model\BodySize;
use App\Model\Category;
use App\Model\Color;
use App\Model\DefaultCategory;
use App\Model\Fabric;
use App\Model\Item;
use App\Model\Length;
use App\Model\MadeInCountry;
use App\Model\MasterColor;
use App\Model\MasterFabric;
use App\Model\MetaVendor;
use App\Model\Pack;
use App\Model\Pattern;
use App\Model\Style;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;

class ItemController extends Controller
{
    public function itemList(Request $request) {
        // Active Items
        $activeItemsQuery = Item::query();
        $activeItemsQuery->where('status', 1)
            ->with('categories', 'images');

        // Search
        if (isset($request->text) && $request->text != '') {
            if (isset($request->style) && $request->style == '1')
                $activeItemsQuery->where('style_no', 'like', '%' . $request->text . '%');

            if (isset($request->vendorStyle) && $request->vendorStyle == '1')
                $activeItemsQuery->where('vendor_style_no', 'like', '%' . $request->text . '%');

            if (isset($request->des) && $request->des == '1')
                $activeItemsQuery->where('description', 'like', '%' . $request->text . '%');

            if (isset($request->name) && $request->name == '1')
                $activeItemsQuery->where('name', 'like', '%' . $request->text . '%');
        }

        if ($request->v && $request->v != '')
            $activeItemsQuery->where('vendor_meta_id', $request->v);

        // Active Items Order
        if (isset($request->s1) && $request->s1 != '') {
            if ($request->s1 == '4')
                $activeItemsQuery->orderBy('price');
            else if ($request->s1 == '1')
                $activeItemsQuery->orderBy('updated_at', 'desc');
            else if ($request->s1 == '2')
                $activeItemsQuery->orderBy('created_at', 'desc');
            else if ($request->s1 == '3')
                $activeItemsQuery->orderBy('activated_at', 'desc');
            else if ($request->s1 == '5')
                $activeItemsQuery->orderBy('price', 'desc');
            else if ($request->s1 == '6')
                $activeItemsQuery->orderBy('style_no');
        } else {
            $activeItemsQuery->orderBy('activated_at', 'desc');
        }

        $activeItems = $activeItemsQuery->paginate(40, ['*'], 'p1');

        // Inactive Items
        $inactiveItemsQuery = Item::query();
        $inactiveItemsQuery->where('status', 0)
            ->with('categories', 'images');

        // Search
        if (isset($request->text) && $request->text != '') {
            if (isset($request->style) && $request->style == '1')
                $inactiveItemsQuery->where('style_no', 'like', '%' . $request->text . '%');

            if (isset($request->vendorStyle) && $request->vendorStyle == '1')
                $inactiveItemsQuery->where('vendor_style_no', 'like', '%' . $request->text . '%');

            if (isset($request->des) && $request->des == '1')
                $inactiveItemsQuery->where('description', 'like', '%' . $request->text . '%');

            if (isset($request->name) && $request->name == '1')
                $inactiveItemsQuery->where('name', 'like', '%' . $request->text . '%');
        }

        if ($request->v && $request->v != '')
            $inactiveItemsQuery->where('vendor_meta_id', $request->v);

        // Inactive order
        if (isset($request->s2) && $request->s2 != '') {
            if ($request->s2 == '4')
                $inactiveItemsQuery->orderBy('price');
            else if ($request->s2 == '1')
                $inactiveItemsQuery->orderBy('updated_at', 'desc');
            else if ($request->s2 == '2')
                $inactiveItemsQuery->orderBy('created_at', 'desc');
            else if ($request->s2 == '3') {
                $inactiveItemsQuery->orderBy('activated_at', 'desc');
                $inactiveItemsQuery->orderBy('created_at', 'desc');
            } else if ($request->s2 == '5')
                $inactiveItemsQuery->orderBy('price', 'desc');
            else if ($request->s2 == '6')
                $inactiveItemsQuery->orderBy('style_no');
        } else {
            $inactiveItemsQuery->orderBy('activated_at', 'desc');
            $inactiveItemsQuery->orderBy('created_at', 'desc');
        }

        $inactiveItems = $inactiveItemsQuery->paginate(40, ['*'], 'p2');

        $appends = [
            'p1' => $activeItems->currentPage(),
            'p2' => $inactiveItems->currentPage(),
        ];

        foreach ($request->all() as $key => $value) {
            if ($key != 'p1' && $key != 'p2')
                $appends[$key] = ($value == null) ? '' : $value;
        }

        // Vendors
        $vendors = MetaVendor::orderBy('company_name')->get();

        return view('admin.item.item_list', compact( 'activeItems', 'inactiveItems', 'appends', 'vendors'))
            ->with('page_title', 'Edit All Items');
    }

    public function itemEdit(Item $item) {
        $item->load('colors', 'images');
        $vendor = $item->vendor;

        $categories = Category::where('vendor_meta_id', $vendor->id)
            ->where('status', 1)->where('parent', 0)
            ->with('subCategories')->orderBy('name')->get();

        $packs = Pack::where('vendor_meta_id', $vendor->id)->where('status', 1)->orderBy('name')->get();
        $fabrics = Fabric::where('vendor_meta_id', $vendor->id)->where('status', 1)->orderBy('name')->get();
        $madeInCountries = MadeInCountry::where('vendor_meta_id', $vendor->id)->where('status', 1)->orderBy('name')->get();
        $bodySizes = BodySize::orderBy('name')->get();
        $patterns = Pattern::orderBy('name')->get();
        $lengths = Length::orderBy('name')->get();
        $styles = Style::orderBy('name')->get();
        $colors = Color::where('vendor_meta_id', $vendor->id)->where('status', 1)->orderBy('name')->get();

        // Images color id
        $imagesColorIds = [];
        foreach($item->images as $img)
            $imagesColorIds[] = $img->color_id;

        // Master Fabric
        $masterFabricsIds = [];

        foreach($fabrics as $fabric)
            $masterFabricsIds[] = $fabric->master_fabric_id;

        $masterFabricsIds = array_unique($masterFabricsIds);

        $masterFabrics = MasterFabric::whereIn('id', $masterFabricsIds)->orderBy('name')->get();

        // Default Categories
        $defaultCategories = [];

        $categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $cat) {
                    if ($cat->parent == $cc->id) {
                        $data2 = [
                            'id' => $cat->id,
                            'name' => $cat->name
                        ];

                        $data3 = [];
                        foreach($categoriesCollection as $item2) {
                            if ($item2->parent == $cat->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        // Master Color
        $masterColors = MasterColor::orderBy('name')->get();

        if (session('message') == null) {
            session(['back_url' => URL::previous()]);
        }

        return view('admin.item.edit', compact('categories', 'packs', 'fabrics', 'madeInCountries',
            'defaultCategories', 'bodySizes', 'patterns', 'lengths', 'styles', 'colors', 'masterFabrics', 'item', 'imagesColorIds', 'masterColors'))
            ->with('page_title', 'Item Edit');
    }
}
