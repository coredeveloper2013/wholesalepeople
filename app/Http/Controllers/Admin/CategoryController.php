<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DefaultCategory;

class CategoryController extends Controller
{
    public function index() {
        $categories = [];

    	$categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

    	foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name
                        ];

                        $data3 = [];
                        foreach($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $categories[] = $data;
            }
        }

        return view('admin.default_category.index', compact('categories'))->with('page_title', 'Default Category');
    }

    public function addCategory(Request $request) {
        $parentID  = 0;

        if ($request->secondaryParentID != "0")
            $parentID = $request->secondaryParentID;
        elseif ($request->parentID != "0")
            $parentID = $request->parentID;

        $sort = 1;
        $category = DefaultCategory::where('parent', $parentID)->orderBy('sort', 'desc')->first();

        if ($category)
            $sort = $category->sort + 1;

        $category = DefaultCategory::create([
            'name' => $request->categoryName,
            'parent' => $parentID,
            'sort' => $sort
        ]);
    }

    public function deleteCategory(Request $request) {
        $category = DefaultCategory::where('id', $request->id)->first();
        $category->delete();
    }

    public function updateCategory(Request $request) {
        $category = DefaultCategory::where('id', $request->id)->first();

        $parentID  = 0;
        if ($request->secondaryParentID != "0")
            $parentID = $request->secondaryParentID;
        elseif ($request->parentID != "0")
            $parentID = $request->parentID;

        $sort = 1;
        $tmp = DefaultCategory::where('parent', $parentID)->orderBy('sort', 'desc')->where('id', '!=', $request->id)->first();

        if ($tmp)
            $sort = $tmp->sort + 1;

        $category->parent = $parentID;
        $category->name = $request->categoryName;
        $category->sort = $sort;
        $category->save();
    }

    public function updateCategoryParent(Request $request) {
        $category = DefaultCategory::where('id', $request->id)->first();

        $category->parent = $request->parent;
        $category->save();
    }

    public function sortCategory(Request $request) {
        $parentSort = 1;

        foreach($request->itemArray as $parent) {
            DefaultCategory::where('id', $parent['id'])->update(['sort' => $parentSort, 'parent' => 0]);

            if (isset($parent['children'])) {
                $children1 = 1;

                foreach($parent['children'] as $item) {
                    DefaultCategory::where('id', $item['id'])->update(['sort' => $children1, 'parent' => $parent['id']]);

                    if (isset($item['children'])) {
                        $children2 = 1;

                        foreach($item['children'] as $item2) {
                            DefaultCategory::where('id', $item2['id'])->update(['sort' => $children2, 'parent' => $item['id']]);
                            $children2++;
                        }

                    }

                    $children1++;
                }
            }

            $parentSort++;
        }
    }
}
