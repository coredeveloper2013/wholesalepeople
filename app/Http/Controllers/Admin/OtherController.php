<?php

namespace App\Http\Controllers\Admin;

use App\Model\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OtherController extends Controller
{
    public function buyerHome() {
        $setting = Setting::where('name', 'buyer_home')->first();

        if (!$setting) {
            Setting::create([
                'name' => 'buyer_home',
            ]);
        }

        return view('admin.buyer_home.index', compact('setting'))->with('page_title', 'Buyer Home');
    }

    public function buyerHomeSave(Request $request) {
        $setting = Setting::where('name', 'buyer_home')->first();

        if ($setting) {
            $setting->value = $request->buyer_home;
            $setting->save();
        } else {
            Setting::create([
                'name' => 'buyer_home',
                'value' => $request->buyer_home,
            ]);
        }

        return redirect()->back()->with('message', 'Saved!');
    }

    public function welcomeNotification() {
        $setting = Setting::where('name', 'welcome_notification')->first();

        if (!$setting) {
            Setting::create([
                'name' => 'welcome_notification',
            ]);
        }

        $os = Setting::where('name', 'opening_soon')->first();

        if (!$os) {
            Setting::create([
                'name' => 'welcome_notification',
                'value' => '0',
            ]);
        }

        $openingDate = Setting::where('name', 'opening_date')->first();

        if (!$openingDate) {
            Setting::create([
                'name' => 'opening_date',
            ]);
        }

        return view('admin.welcome_notification.index', compact('setting', 'os', 'openingDate'))->with('page_title', 'Welcome Notification');
    }

    public function welcomeNotificationSave(Request $request) {
        $setting = Setting::where('name', 'welcome_notification')->first();

        if ($setting) {
            $setting->value = $request->data;
            $setting->save();
        } else {
            Setting::create([
                'name' => 'welcome_notification',
                'value' => $request->data,
            ]);
        }

        // Opening soon
        $setting = Setting::where('name', 'opening_soon')->first();

        if ($setting) {
            $setting->value = ($request->default ? 1 : 0);
            $setting->save();
        } else {
            Setting::create([
                'name' => 'opening_soon',
                'value' =>  ($request->default ? 1 : 0)
            ]);
        }

        // Opening Date
        $setting = Setting::where('name', 'opening_date')->first();

        if ($setting) {
            $setting->value = $request->date;
            $setting->save();
        } else {
            Setting::create([
                'name' => 'opening_soon',
                'value' =>  $request->date
            ]);
        }

        return redirect()->back()->with('message', 'Saved!');
    }
}
