<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\OrderStatus;
use App\Model\CartItem;
use App\Model\Item;
use App\Model\MetaVendor;
use App\Model\Notification;
use App\Model\Order;
use App\Model\ShippingMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class OrderController extends Controller
{
    public function allOrders() {
        ini_set('memory_limit', '-1');
        $vendors = MetaVendor::with('user')->get();

        foreach ($vendors as &$vendor) {
            /*$sales = 0;

            foreach ($vendor->orders as $order)
                $sales += $order->total;*/

            $vendor->sales = Order::where('vendor_meta_id', $vendor->id)->where('status', '!=', OrderStatus::$INIT)->sum('total');
            $vendor->items_count = Item::where('vendor_meta_id', $vendor->id)->count();
            $vendor->orders_count = Order::where('vendor_meta_id', $vendor->id)->where('status', '!=', OrderStatus::$INIT)->count();
        }

        return view('admin.order.all_orders', compact('vendors'))->with('page_title', 'All Orders');
    }
    public function allNewOrders() {
        ini_set('memory_limit', '-1');
        $vendors = MetaVendor::with('user')->get();

        foreach ($vendors as &$vendor) {
            /*$sales = 0;

            foreach ($vendor->orders as $order)
                $sales += $order->total;*/

            $vendor->sales = Order::where('vendor_meta_id', $vendor->id)->where('status', OrderStatus::$NEW_ORDER)->sum('total');
            $vendor->items_count = Item::where('vendor_meta_id', $vendor->id)->count();
            $vendor->orders_count = Order::where('vendor_meta_id', $vendor->id)->where('status', '!=', OrderStatus::$NEW_ORDER)->count();
        }

        return view('admin.order.all_orders', compact('vendors'))->with('page_title', 'New Orders');
    }

    public function vendorOrders(MetaVendor $vendor, Request $request) {
        $parameters = [];
        $userIds = [];

        // Search
        if (isset($request->text) && $request->text !='') {
            if (isset($request->search) && $request->search == '1') {
                $metaBuyers = MetaBuyer::where('company_name', 'like', '%' . $request->text . '%')->get();

                foreach ($metaBuyers as $buyer)
                    $userIds[] = $buyer->user_id;
            } else if (isset($request->search) && $request->search == '2') {
                $parameters[] = ['order_number', 'like', '%' . $request->text . '%'];
            } else if (isset($request->search) && $request->search == '3') {
                $parameters[] = ['tracking_number', 'like', '%' . $request->text . '%'];
            }
        }

        if (isset($request->ship) && $request->ship !='') {
            if ($request->ship == '1')
                $parameters[] = ['status', '=', OrderStatus::$PARTIALLY_SHIPPED_ORDER];
            else if ($request->ship == '2')
                $parameters[] = ['status', '=', OrderStatus::$FULLY_SHIPPED_ORDER];
        }


        if (isset($request->date) && $request->date !='') {
            if ($request->date == '1') {
                $parameters[] = ['created_at', '>', date('Y-m-d 23:59:59', strtotime('-1 days'))];
            } else if ($request->date == '2') {
                $day = date('w');
                $week_start = date('Y-m-d', strtotime('-'.$day.' days'));
                $parameters[] = ['created_at', '>=', $week_start];
            } else if ($request->date == '3') {
                $parameters[] = ['created_at', '>', date('Y-m-01')];
            } else if ($request->date == '5') {
                $parameters[] = ['created_at', '>', date('Y-01-01')];
            } else if ($request->date == '6') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-1 days'))];
                $parameters[] = ['created_at', '<=', date('Y-m-d 23:59:59', strtotime('-1 days'))];
            } else if ($request->date == '8') {
                $month_ini = new DateTime("first day of last month");
                $month_end = new DateTime("last day of last month");

                $parameters[] = ['created_at', '>=', $month_ini->format('Y-m-d')];
                $parameters[] = ['created_at', '<=', $month_end->format('Y-m-d')];
            } else if ($request->date == '10') {
                $parameters[] = ['created_at', '>=', date("Y-m-d",strtotime("last year January 1st"))];
                $parameters[] = ['created_at', '<=', date("Y-m-d",strtotime("last year December 31st"))];
            } else if ($request->date == '13') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-7 days'))];
            } else if ($request->date == '14') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-30 days'))];
            } else if ($request->date == '15') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-90 days'))];
            } else if ($request->date == '16') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-365 days'))];
            } else if ($request->date == '0') {
                $starDate = DateTime::createFromFormat('m/d/Y', $request->startDate);
                $endDate = DateTime::createFromFormat('m/d/Y', $request->endDate);

                $parameters[] = ['created_at', '>=', $starDate->format('Y-m-d 00:00:00')];
                $parameters[] = ['created_at', '<=', $endDate->format('Y-m-d 23:59:59')];
            }
        } else {
            $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-30 days'))];
        }

        // New Orders
        $newOrdersQuery = Order::query();
        $newOrdersQuery->where('vendor_meta_id', $vendor->id)
            ->where('status', OrderStatus::$NEW_ORDER)
            ->where($parameters);

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $newOrdersQuery->whereIn('user_id', $userIds);
        }

        $newOrders = $newOrdersQuery->paginate(10, ['*'], 'p1');

        foreach($newOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        // Confirm Orders
        $confirmOrdersQuery = Order::query();
        $confirmOrdersQuery->where('vendor_meta_id', $vendor->id)
            ->where('status', OrderStatus::$CONFIRM_ORDER)
            ->where($parameters);

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $confirmOrdersQuery->whereIn('user_id', $userIds);
        }

        $confirmOrders = $confirmOrdersQuery->paginate(10, ['*'], 'p2');

        foreach($confirmOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }


        // Back Orders
        $backOrdersQuery = Order::query();

        $backOrdersQuery->where('vendor_meta_id', $vendor->id)
            ->where('status', OrderStatus::$BACK_ORDER)
            ->where($parameters);

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $backOrdersQuery->whereIn('user_id', $userIds);
        }

        $backOrders = $backOrdersQuery->paginate(10, ['*'], 'p3');

        foreach($backOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        // Shipped Orders
        $shippedOrdersQuery = Order::query();
        $shippedOrdersQuery->where('vendor_meta_id', $vendor->id)
            ->whereIn('status', [OrderStatus::$FULLY_SHIPPED_ORDER, OrderStatus::$PARTIALLY_SHIPPED_ORDER])
            ->where($parameters);

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $shippedOrdersQuery->whereIn('user_id', $userIds);
        }

        $shippedOrders = $shippedOrdersQuery->paginate(10, ['*'], 'p4');

        foreach($shippedOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        // Cancel Orders
        $cancelOrdersQuery = Order::query();
        $cancelOrdersQuery->where('vendor_meta_id', $vendor->id)
            ->whereIn('status', [OrderStatus::$CANCEL_BY_AGREEMENT, OrderStatus::$CANCEL_BY_BUYER, OrderStatus::$CANCEL_BY_VENDOR])
            ->where($parameters);

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $shippedOrdersQuery->whereIn('user_id', $userIds);
        }

        $cancelOrders = $cancelOrdersQuery->paginate(10, ['*'], 'p5');

        foreach($cancelOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        $appends = [
            'p1' => $newOrders->currentPage(),
            'p2' => $confirmOrders->currentPage(),
            'p3' => $confirmOrders->currentPage(),
            'p4' => $confirmOrders->currentPage(),
            'p5' => $confirmOrders->currentPage(),
        ];

        foreach ($request->all() as $key => $value) {
            if ($key != 'p1' && $key != 'p2' && $key != 'p3' && $key != 'p4')
                $appends[$key] = ($value == null) ? '' : $value;
        }

        $url = route('admin_vendor_orders', ['vendor' => $vendor->id]);

        return view('admin.order.vendor_orders', compact('newOrders', 'confirmOrders', 'backOrders',
            'appends', 'shippedOrders', 'cancelOrders', 'url'))
            ->with('page_title', $vendor->company_name.' - Orders');
    }

    public function orderDetails(Order $order) {
        $allItems = [];
        $order->load('user', 'items', 'shippingMethod');
        $itemIds = [];

        $shippingMethods = ShippingMethod::where('status', 1)
            ->where('vendor_meta_id', $order->vendor_meta_id)
            ->with('shipMethod')
            ->orderBy('list_order')
            ->get();

        $count = Order::where('status', '!=', OrderStatus::$INIT)
            ->where('user_id', $order->user->id)
            ->where('vendor_meta_id', $order->vendor_meta_id)
            ->where('created_at', '<=', $order->created_at)
            ->count();

        $countText = 'This is the '.$this->addOrdinalNumberSuffix($count).' order.';

        foreach($order->items as $item) {
            $allItems[$item->item_id][] = $item;
            $itemIds[] = $item->id;
        }

        return view('admin.order.order_details', compact('order', 'allItems', 'countText', 'itemIds', 'shippingMethods'))
            ->with('page_title', 'Order Details');
    }

    public function orderDetailsPost(Order $order, Request $request) {
        $order->load('items');

        $rules = [
            'input_discount' => 'required|numeric|min:0',
            'input_shipping_cost' => 'required|numeric|min:0',
            'tracking_number' => 'nullable|max:191',
            'shipping_method_id' => 'required'
        ];

        foreach ($order->items as $item) {
            $rules['size_'.$item->id.'.*'] = 'required|integer|min:0';
            $rules['pack_'.$item->id] = 'required|integer|min:0';
            $rules['unit_price_'.$item->id] = 'required|regex:/^[0-9]+(?:\.[0-9]{1,2})?$/';
        }

        $request->validate($rules);

        $subTotal = 0;

        foreach ($order->items as $item) {
            $sizeVar = 'size_'.$item->id;
            $packVar = 'pack_'.$item->id;
            $perUnitPriceVar = 'unit_price_'.$item->id;
            $itemPerPack = 0;
            $size = '';

            foreach ($request->$sizeVar as $s) {
                $size .= $s.'-';
                $itemPerPack += (int) $s;
            }

            $size = rtrim($size, "-");

            $item->pack = $size;
            $item->qty = $request->$packVar;
            $item->item_per_pack = $itemPerPack;
            $item->total_qty = $itemPerPack * (int) $request->$packVar;
            $item->per_unit_price = $request->$perUnitPriceVar;
            $item->amount = $request->$perUnitPriceVar * $itemPerPack * $request->$packVar;
            $item->save();

            $subTotal += $item->amount;
        }

        $order->subtotal = $subTotal;
        $order->discount = $request->input_discount;
        $order->shipping_cost = $request->input_shipping_cost;
        $order->total = $order->subtotal + (float) $request->input_shipping_cost - (float) $request->input_discount;
        $order->status = $request->order_status;
        $order->tracking_number = $request->tracking_number;
        $order->shipping_method_id = $request->shipping_method_id;
        $order->save();

        if($request->notify_user && $request->notify_user) {
            Notification::create([
                'user_id' => $order->user_id,
                'text' => 'Admin updated order no. '.$order->order_number,
                'link' => route('show_order_details', ['order' => $order->id]),
                'view' => 0
            ]);
        }

        return redirect()->back()->with('message', 'Order Updated!');
    }

    public function addOrdinalNumberSuffix($num) {
        if (!in_array(($num % 100),array(11,12,13))){
            switch ($num % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:  return $num.'st';
                case 2:  return $num.'nd';
                case 3:  return $num.'rd';
            }
        }
        return $num.'th';
    }

    public function incompleteOrders(Request $request) {
        $appends = [];

        if ($request->brand && $request->brand != '') {
            $result = CartItem::select(DB::raw('user_id, vendor_meta_id, meta_vendors.id, company_name, MAX(cart_items.updated_at) d'))
                ->join('meta_vendors', 'meta_vendors.id', '=', 'cart_items.vendor_meta_id')
                ->where('cart_items.vendor_meta_id', $request->brand)
                ->groupBy('user_id', 'vendor_meta_id', 'meta_vendors.id', 'company_name')
                ->orderBy('d', 'desc')
                ->paginate(10);

            $appends['brand'] = $request->brand;
        } else {
            $result = CartItem::select(DB::raw('user_id, vendor_meta_id, meta_vendors.id, company_name, MAX(cart_items.updated_at) d'))
                ->join('meta_vendors', 'meta_vendors.id', '=', 'cart_items.vendor_meta_id')
                ->groupBy('user_id', 'vendor_meta_id', 'meta_vendors.id', 'company_name')
                ->orderBy('d', 'desc')
                ->paginate(10);
        }

        $orders = [];

        foreach ($result as $r) {
            $total = 0;

            $cartItems = CartItem::where('user_id', $r->user_id)
                ->where('vendor_meta_id', $r->vendor_meta_id)
                ->orderBy('updated_at', 'desc')
                ->with('item', 'user')
                ->get();

            foreach ($cartItems as $item) {
                $sizes = explode("-", $item->item->pack->name);
                $itemInPack = 0;

                for($i=1; $i <= sizeof($sizes); $i++) {
                    $var = 'pack'.$i;

                    if ($item->item->pack->$var != null)
                        $itemInPack += (int) $item->item->pack->$var;
                }

                $total += $itemInPack * $item->quantity * $item->item->price;
            }

            $orders [] = [
                'user_id' => $r->user_id,
                'company' => $cartItems[0]->user->buyer->company_name,
                'vendor' => $r->company_name,
                'vendor_id' => $r->vendor_meta_id,
                'total' => $total,
                'updated_at' => $cartItems[0]->updated_at
            ];
        }

        $vendors = MetaVendor::orderBy('company_name')->get();

        /*$parameters = [];
        $appends = [];

        if ($request->brand && $request->brand != '') {
            $parameters[] = ['vendor_meta_id', '=', $request->brand];
            $appends['brand'] = $request->brand;
        }

        $orders = Order::where('status', OrderStatus::$INIT)
            ->where($parameters)
            ->orderBy('created_at', 'desc')
            ->with('vendor')
            ->paginate(10);

        $vendors = MetaVendor::orderBy('company_name')->get();*/

        return view('admin.order.incomplete', compact('orders', 'vendors', 'appends', 'result'))
            ->with('page_title', 'Incomplete Checkouts');
    }

    public function incompleteOrderDetails($user, $vendor) {
        $cartItems = CartItem::where('vendor_meta_id', $vendor)
            ->where('user_id', $user)
            ->with('item', 'color')
            ->get();

        //$order->load('user', 'items');
        $allItems = [];

        foreach($cartItems as $item)
            $allItems[$item->item_id][] = $item;

        /*$order->load('user', 'items');
        $allItems = [];

        foreach($order->items as $item)
            $allItems[$item->item_id][] = $item;*/

        return view('admin.order.incomplete_details', compact('order', 'allItems'))->with('page_title', 'Incomplete Checkouts');
    }
}
