<?php

namespace App\Http\Controllers\Admin;

use App\Model\Industry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndustryController extends Controller
{
    public function index() {
        $industries = Industry::orderBy('name')->get();

        return view('admin.industry.index', compact('industries'))->with('page_title', 'Industry');
    }

    public function addPost(Request $request) {
        Industry::create([
            'name' => $request->name
        ]);

        return redirect()->route('admin_industry')->with('message', 'Industry Added!');
    }

    public function delete(Request $request) {
        $industry = Industry::where('id', $request->id)->first();
        $industry->delete();
    }

    public function update(Request $request) {
        $industry = Industry::where('id', $request->id)->first();
        $industry->name = $request->name;
        $industry->save();

        return redirect()->route('admin_industry')->with('message', 'Industry Updated!');
    }
}
