<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\CategoryBannerType;
use App\Model\CategoryBanner;
use App\Model\DefaultCategory;
use App\Model\MetaVendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryBannerController extends Controller
{
    public function index(DefaultCategory $category) {
        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->orderBy('company_name')
            ->get();

        $topBanners = CategoryBanner::where('type', CategoryBannerType::$TOP_BANNER)
            ->where('category_id', $category->id)
            ->with('vendor')
            ->get();

        $topItems = CategoryBanner::where('type', CategoryBannerType::$TOP_SLIDER)
            ->where('category_id', $category->id)
            ->with('vendor')
            ->get();

        $bannerItems = CategoryBanner::where('type', CategoryBannerType::$SMALL_BANNER)
            ->where('category_id', $category->id)
            ->with('vendor')
            ->get();

        $secondSliderItems = CategoryBanner::where('type', CategoryBannerType::$SECOND_SLIDER)
            ->where('category_id', $category->id)
            ->with('vendor')
            ->get();

        $title = $category->name;

        return view('admin.category_banner.index', compact('category', 'vendors', 'topItems', 'bannerItems', 'secondSliderItems', 'topBanners', 'title'))
            ->with('page_title', 'Category Banner');
    }

    public function newArrival() {
        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->orderBy('company_name')
            ->get();

        $topBanners = CategoryBanner::where('type', CategoryBannerType::$TOP_BANNER)
            ->where('category_id', 0)
            ->with('vendor')
            ->get();

        $topItems = CategoryBanner::where('type', CategoryBannerType::$TOP_SLIDER)
            ->where('category_id', 0)
            ->with('vendor')
            ->get();

        $bannerItems = CategoryBanner::where('type', CategoryBannerType::$SMALL_BANNER)
            ->where('category_id', 0)
            ->with('vendor')
            ->get();

        $secondSliderItems = CategoryBanner::where('type', CategoryBannerType::$SECOND_SLIDER)
            ->where('category_id', 0)
            ->with('vendor')
            ->get();

        $title = 'New Arrival';

        return view('admin.category_banner.index', compact( 'vendors', 'topItems', 'bannerItems', 'secondSliderItems', 'topBanners', 'title'))
            ->with('page_title', 'Category Banner');
    }

    public function vendorAll() {
        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->orderBy('company_name')
            ->get();

        $topBanners = CategoryBanner::where('type', CategoryBannerType::$TOP_BANNER)
            ->where('category_id', -1)
            ->with('vendor')
            ->get();

        $title = 'Vendor All';

        return view('admin.category_banner.index', compact( 'vendors', 'topBanners', 'title'))
            ->with('page_title', 'Category Banner');
    }

    public function add(Request $request) {
        $item = CategoryBanner::where('type', $request->type)
            ->where('vendor_id', $request->vendorId)
            ->where('category_id', $request->categoryId)
            ->first();

        if ($item) {
            return response()->json(['success' => false, 'message' => 'Already Added!']);
        } else {
            if ($request->type == CategoryBannerType::$TOP_BANNER) {
                $item = CategoryBanner::where('type', $request->type)
                    ->where('category_id', $request->categoryId)
                    ->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already have a vendor.']);
            }

            CategoryBanner::create([
                'type' => $request->type,
                'vendor_id' => $request->vendorId,
                'category_id' => $request->categoryId,
            ]);

            return response()->json(['success' => true, 'message' => 'Added!']);
        }
    }

    public function delete(Request $request) {
        CategoryBanner::where('id', $request->id)->delete();
    }
}
