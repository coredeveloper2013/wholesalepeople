<?php

namespace App\Http\Controllers\Vendor;

use App\Model\Category;
use App\Model\DefaultCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function index() {
        $categories = [];
        $parentCategories = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('parent', 0)->orderBy('name')->get();

        $categoriesCollection = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name,
                    'sort' => $cc->sort
                ];

                $subCategories = [];
                foreach($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name,
                            'sort' => $item->sort
                        ];

                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $categories[] = $data;
            }
        }

        // Default Categories
        $defaultCategories = [];

        $categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name
                        ];

                        $data3 = [];
                        foreach($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        return view('vendor.dashboard.item_settings.category', compact('categories', 'parentCategories', 'defaultCategories'))
            ->with('page_title', 'Category');
    }

    public function sortCategory(Request $request) {
        $parentSort = 1;


        foreach($request->itemArray as $parent) {
            Category::where('id', $parent['id'])->update(['sort' => $parentSort, 'parent' => 0]);

            if (isset($parent['children'])) {
                $children1 = 1;

                foreach($parent['children'] as $item) {
                    Category::where('id', $item['id'])->update(['sort' => $children1, 'parent' => $parent['id']]);
                    $children1++;
                }
            }

            $parentSort++;
        }
    }

    public function addPostCategory(Request $request) {
        $request->validate([
            'category_name' => 'required|max:255',
            'd_parent_category' => 'required',
            'sort_no' => 'required|integer',
            'discount' => 'nullable|numeric|max:100'
        ]);

        $parent = 0;

        if ($request->parent_category && $request->parent_category != '')
            $parent = $request->parent_category;

        Category::create([
            'name' => $request->category_name,
            'parent' => $parent,
            'sort' => $request->sort_no,
            'd_category_id' => $request->d_parent_category,
            'd_category_second_id' => $request->d_second_parent_category,
            'd_category_third_id' => $request->d_third_parent_category,
            'status' => $request->status,
            'vendor_meta_id' => Auth::user()->vendor_meta_id,
            'discount' => $request->discount
        ]);

        return redirect()->route('vendor_category')->with('message', 'Category Added!');
    }

    public function editPostCategory(Request $request) {
        $request->validate([
            'category_name' => 'required|max:255',
            'd_parent_category' => 'required',
            'sort_no' => 'required|integer',
            'discount' => 'nullable|numeric|max:100'
        ]);

        $category = Category::where('id', $request->categoryId)->first();
        $parent = 0;

        if ($request->parent_category && $request->parent_category != '')
            $parent = $request->parent_category;


        $category->name = $request->category_name;
        $category->parent = $parent;
        $category->sort = $request->sort_no;
        $category->d_category_id = $request->d_parent_category;
        $category->d_category_second_id = $request->d_second_parent_category;
        $category->d_category_third_id = $request->d_third_parent_category;
        $category->status = $request->status;
        $category->vendor_meta_id = Auth::user()->vendor_meta_id;
        $category->discount = $request->discount;

        $category->save();

        return redirect()->route('vendor_category')->with('message', 'Category Updated!');
    }

    public function categoryDetail(Request $request) {
        $category = Category::where('id', $request->id)->first();

        return $category->toArray();
    }

    public function deleteCategory(Request $request) {
        $category = Category::where('id', $request->id)->first();
        $category->delete();
    }
}
