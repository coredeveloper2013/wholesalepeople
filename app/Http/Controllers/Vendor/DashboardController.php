<?php

namespace App\Http\Controllers\Vendor;

use App\Enumeration\OrderStatus;
use App\Model\Item;
use App\Model\ItemImages;
use App\Model\Order;
use App\Model\Visitor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index() {
        $vendor = Auth::user()->vendor;

        $homePageVisitor = Visitor::where('url', route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]))
            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-30 days')))->count();

        $homePageVisitor += Visitor::where('route', 'item_details_page')
            ->where('vendor_meta_id', $vendor->id)
            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-30 days')))->count();

        $homePageVisitorUnique = Visitor::where('url', route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]))
            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-30 days')))
            ->distinct('ip')
            ->count('ip');

        $homePageVisitorUnique += Visitor::where('route', 'item_details_page')
            ->where('vendor_meta_id', $vendor->id)
            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-30 days')))
            ->distinct('ip')
            ->count('ip');

        $totalSaleAmount = Order::whereIn('status', [OrderStatus::$FULLY_SHIPPED_ORDER])
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-30 days')))
            ->sum('total');

        $totalPendingOrder = Order::whereIn('status', [OrderStatus::$PARTIALLY_SHIPPED_ORDER, OrderStatus::$BACK_ORDER, OrderStatus::$CONFIRM_ORDER, OrderStatus::$NEW_ORDER])
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-30 days')))
            ->sum('total');

        $todayOrderAmount = Order::where('status', '!=', OrderStatus::$INIT)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->whereDate('created_at', date('Y-m-d'))
            ->sum('total');

        $yesterdayVisitor = Visitor::where('url', route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]))
            ->whereDate('created_at', date('Y-m-d', strtotime('-1 days')))
            ->count();

        $yesterdayVisitor += Visitor::where('route', 'item_details_page')
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->whereDate('created_at', date('Y-m-d', strtotime('-1 days')))
            ->count();

        $yesterdayOrderAmount = Order::where('status', '!=', OrderStatus::$INIT)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->whereDate('created_at', date('Y-m-d', strtotime('-1 days')))
            ->sum('total');

        $itemUploadedThisMonth = Item::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count();

        $itemUploadedLastMonth = Item::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)
            ->where('created_at', '>=', new Carbon('first day of last month'))
            ->where('created_at', '<=', new Carbon('last day of last month'))
            ->count();

        // Best Selling Items
        $sql = "SELECT items.id, items.name, items.style_no, meta_vendors.id vendor_id, meta_vendors.company_name, items.price, t.count 
                FROM items 
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items GROUP BY item_id) t ON items.id = t.item_id 
                JOIN meta_vendors ON items.vendor_meta_id = meta_vendors.id
                WHERE items.deleted_at IS NULL AND items.status = 1 AND meta_vendors.id = ".Auth::user()->vendor_meta_id."
                ORDER BY count DESC
                LIMIT 6";

        $bestItems = DB::select($sql);

        foreach ($bestItems as &$item) {
            $image = ItemImages::where('item_id', $item->id)
                ->orderBy('sort')
                ->first();

            $imagePath = asset('images/no-image.png');

            if ($image)
                $imagePath = asset($image->list_image_path);

            $item->image_path = $imagePath;
        }

        // Order Count By Month
        $startDate = [];
        $endDate = [];
        $orderCountLabel = [];
        $orderCount = [];
        $returnOrder = [];

        for($i=5; $i >= 0; $i--) {
            $orderCountLabel[] = date('Y/m', strtotime("-$i month"));
            $startDate[] = date('Y-m-01', strtotime("-$i month"));
            $endDate[] = date('Y-m-t', strtotime("-$i month"));
        }

        for($i=0; $i < 6; $i++) {
            $orderCount[] = Order::where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('status', '!=', OrderStatus::$INIT)
                ->where('status', '!=', OrderStatus::$RETURNED)
                ->where('status', '!=', OrderStatus::$CANCEL_BY_VENDOR)
                ->where('status', '!=', OrderStatus::$CANCEL_BY_BUYER)
                ->where('status', '!=', OrderStatus::$CANCEL_BY_AGREEMENT)
                ->where('created_at', '>=', $startDate[$i])
                ->where('created_at', '<=', $endDate[$i])
                ->count();

            $returnOrder[] = Order::where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('status', '=', OrderStatus::$RETURNED)
                ->where('created_at', '>=', $startDate[$i])
                ->where('created_at', '<=', $endDate[$i])
                ->count();
        }

        // Item Upload chart
        $uploadCount = [];

        for($i=0; $i < 6; $i++) {
            $uploadCount[] = Item::where('status', 1)
                ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('created_at', '>=', $startDate[$i])
                ->where('created_at', '<=', $endDate[$i])
                ->count();
        }

        $data = [
            'homePageVisitor' => $homePageVisitor,
            'homePageVisitorUnique' => $homePageVisitorUnique,
            'totalSaleAmount' => $totalSaleAmount,
            'totalPendingOrder' => $totalPendingOrder,
            'todayOrderAmount' => $todayOrderAmount,
            'yesterdayVisitor' => $yesterdayVisitor,
            'yesterdayOrderAmount' => $yesterdayOrderAmount,
            'itemUploadedThisMonth' => $itemUploadedThisMonth,
            'itemUploadedLastMonth' => $itemUploadedLastMonth,
            'orderCountLabel' => json_encode($orderCountLabel),
            'orderCount' => json_encode($orderCount),
            'returnOrder' => json_encode($returnOrder),
            'uploadCount' => json_encode($uploadCount),
        ];

        return view('vendor.dashboard.index', compact('data', 'bestItems'))->with('page_title', 'Dashboard');
    }
}
