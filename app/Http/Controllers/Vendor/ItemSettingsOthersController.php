<?php

namespace App\Http\Controllers\Vendor;

use App\Model\Fabric;
use App\Model\MadeInCountry;
use App\Model\MasterFabric;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ItemSettingsOthersController extends Controller
{
    public function index() {
        $madeInCountries = MadeInCountry::where('vendor_meta_id', Auth::user()->vendor_meta_id)->orderBy('name')->get();
        $fabrics = Fabric::where('vendor_meta_id', Auth::user()->vendor_meta_id)->orderBy('name')->with('masterFabric')->get();
        $masterFabrics = MasterFabric::orderBy('name')->get();

        return view('vendor.dashboard.item_settings.others', compact('madeInCountries', 'masterFabrics', 'fabrics'))
            ->with('page_title', 'Other: Fabric, Made In, Supplying Vendor, Default Item Setting');
    }

    public function madeInCountryAdd(Request $request) {
        if ($request->defaultVal == '1')
            MadeInCountry::where('vendor_meta_id', Auth::user()->vendor_meta_id)->update([ 'default' => 0 ]);

        $country = MadeInCountry::create([
            'name' => $request->name,
            'status' => $request->status,
            'default' => $request->defaultVal,
            'vendor_meta_id' => Auth::user()->vendor_meta_id,
        ]);

        return $country->toArray();
    }

    public function madeInCountryUpdate(Request $request) {
        $country = MadeInCountry::where('id', $request->id)->first();

        if ($request->defaultVal == '1')
            MadeInCountry::where('vendor_meta_id', Auth::user()->vendor_meta_id)->update([ 'default' => 0 ]);

        $country->name = $request->name;
        $country->status = $request->status;
        $country->default = $request->defaultVal;
        $country->save();

        return $country->toArray();
    }

    public function madeInCountryDelete(Request $request) {
        $country = MadeInCountry::where('id', $request->id)->first();
        $country->delete();
    }

    public function madeInCountryChangeStatus(Request $request) {
        $country = MadeInCountry::where('id', $request->id)->first();
        $country->status = $request->status;
        $country->save();
    }

    public function madeInCountryChangeDefault(Request $request) {
        MadeInCountry::where('vendor_meta_id', Auth::user()->vendor_meta_id)->update([ 'default' => 0 ]);
        $country = MadeInCountry::where('id', $request->id)->first();
        $country->default = 1;
        $country->save();
    }

    public function fabricAdd(Request $request) {
        if ($request->defaultVal == '1')
            Fabric::where('vendor_meta_id', Auth::user()->vendor_meta_id)->update([ 'default' => 0 ]);

        $fabric = Fabric::create([
            'name' => $request->name,
            'status' => $request->status,
            'default' => $request->defaultVal,
            'master_fabric_id' => $request->masterFabricId,
            'vendor_meta_id' => Auth::user()->vendor_meta_id,
        ]);

        $fabric->load('masterFabric');

        return $fabric->toArray();
    }

    public function fabricUpdate(Request $request) {
        $fabric = Fabric::where('id', $request->id)->first();

        if ($request->defaultVal == '1')
            Fabric::where('vendor_meta_id', Auth::user()->vendor_meta_id)->update([ 'default' => 0 ]);

        $fabric->name = $request->name;
        $fabric->status = $request->status;
        $fabric->default = $request->defaultVal;
        $fabric->master_fabric_id = $request->masterFabricId;
        $fabric->save();

        $fabric->load('masterFabric');

        return $fabric->toArray();
    }

    public function fabricDelete(Request $request) {
        $fabric = Fabric::where('id', $request->id)->first();
        $fabric->delete();
    }

    public function fabricChangeStatus(Request $request) {
        $fabric = Fabric::where('id', $request->id)->first();
        $fabric->status = $request->status;
        $fabric->save();
    }

    public function fabricChangeDefault(Request $request) {
        Fabric::where('vendor_meta_id', Auth::user()->vendor_meta_id)->update([ 'default' => 0 ]);
        $fabric = Fabric::where('id', $request->id)->first();
        $fabric->default = 1;
        $fabric->save();
    }
}
