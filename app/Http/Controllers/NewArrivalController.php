<?php

namespace App\Http\Controllers;

use App\Enumeration\Role;
use App\Model\DefaultCategory;
use App\Model\Item;
use App\Model\MasterColor;
use App\Model\MetaVendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewArrivalController extends Controller
{
    public function showItems(Request $request) {
        $date = '';
        $cat = '';
        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();

        $query = Item::query();
        $query->where('status', 1);

        if (isset($request->D)) {
            if ($request->D != 'A') {
                $date = date('Y-m-d', strtotime("-$request->D day"));
                $query->whereDate('activated_at', $date);
            }
        }

        if (isset($request->C)) {
            $cat = $request->C;
            $query->where('default_parent_category', $request->C);
        }

        $query->orderBy('activated_at', 'desc');
        //$items = $query->paginate(30);
        $items = [];

        // Category
        $categories = DefaultCategory::where('parent', 0)
            ->orderBy('sort')
            ->get();

        foreach ($categories as &$category) {
            $categoryCountQuery = Item::query();
            $categoryCountQuery->where('status', 1)->whereIn('vendor_meta_id', $activeVendors)->where('default_parent_category', $category->id);

            if ($date == '') {
                $category->count = $categoryCountQuery->count();
            } else {
                $category->count = $categoryCountQuery->whereDate('created_at', $date)->count();
            }
        }

        // Arrival Dates
        $totalCount = Item::where('status', 1)->whereIn('vendor_meta_id', $activeVendors)->count();
        $byArrivalDate = [];
        $byArrivalDate[] = [
            'name' => 'All',
            'count' => number_format($totalCount),
            'day' => 'A'
        ];

        for($i=0; $i <= 6; $i++) {
            $count = Item::where('status', 1)
                ->whereIn('vendor_meta_id', $activeVendors)
                ->whereDate('activated_at', date('Y-m-d', strtotime("-$i day")))
                ->count();

            $byArrivalDate[] = [
                'name' => date('F j', strtotime("-$i day")),
                'count' => number_format($count),
                'day' => $i
            ];
        }

        // Vendors
        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->orderBy('company_name')->get();

        /*foreach($vendors as &$vendor) {
            $vendorCountQuery = Item::query();
            $vendorCountQuery->where('status', 1);
            $vendorCountQuery->where('vendor_meta_id', $vendor->id);

            if ($date != '')
                $vendorCountQuery->whereDate('created_at', $date);

            if ($cat != '') {
                $vendorCountQuery->where('default_parent_category', $cat);
            }

            $count = $vendorCountQuery->count();
            $vendor->totalItem = $count;
        }*/

        // Master Colors
        $masterColors = MasterColor::orderBy('name')->get();

        return view('pages.new_arrival', compact('items', 'byArrivalDate', 'categories', 'vendors', 'masterColors'));
    }

    public function getNewArrivalItems(Request $request) {
        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();

        $query = Item::query();

        $query->where('status', 1);
        $query->whereIn('vendor_meta_id', $activeVendors);


        if ($request->D != '') {
            if ($request->D != 'A') {
                $date = date('Y-m-d', strtotime("-$request->D day"));


                $query->whereDate('activated_at', $date);
            }
        }

        if ($request->categories && sizeof($request->categories) > 0)
            $query->whereIn('default_parent_category', $request->categories);

        if ($request->vendors && sizeof($request->vendors) > 0)
            $query->whereIn('vendor_meta_id', $request->vendors);


        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        // Search
        if ($request->searchText && $request->searchText != ''){
            if ($request->searchOption == '1')
                $query->where('style_no', 'like','%'.$request->searchText.'%');
            if ($request->searchOption == '2')
                $query->where('description', 'like','%'.$request->searchText.'%');
        }

        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=',$request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=',$request->priceMax);


        // Sorting
        if ($request->sorting){
            if ($request->sorting == '1')
                $query->orderBy('activated_at', 'desc');
            else if ($request->sorting == '2')
                $query->orderBy('price');
            else if ($request->sorting == '3')
                $query->orderBy('price', 'desc');
            else if ($request->sorting == '4')
                $query->orderBy('style_no');
        }


        $items = $query->with('images', 'vendor', 'colors')->paginate(42);
        $paginationView = $items->links('others.pagination');
        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        // Blocked check
        $blockedVendorIds = [];

        if (Auth::check())
            $blockedVendorIds = Auth::user()->blockedVendorIds();

        $results = [];

        foreach($items as $item) {
            $tmp = null;
            $tmp['id'] = $item->id;
            $tmp['price_text'] = '';
            $tmp['availability'] = $item->availability;
            $tmp['available_on'] = $item->available_on;


            // Price
            $price = '';

            /*if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }*/

            // Image
            $imagePath = cdn('images/no-image.png');

            if (sizeof($item->images) > 0)
                $imagePath = cdn($item->images[0]->list_image_path);

            /*if ($item->orig_price != null)
                $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

            $price .= '$' . number_format($item->price, 2, '.', '');*/

            if ($item->getOriginal('orig_price') != null)
                $price .= '<del>$' . number_format($item->getOriginal('orig_price'), 2, '.', '') . '</del> ';

            $price .= '$' . number_format($item->getOriginal('price'), 2, '.', '');

            $tmp['imagePath'] = $imagePath;
            $tmp['detailsUrl'] = route('item_details_page', ['item' => $item->id]);
            $tmp['vendorUrl'] = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);
            $tmp['price'] = $price;
            $tmp['company_name'] = $item->vendor->company_name;

            if (sizeof($item->colors) > 1)
                $tmp['multiColor'] = true;
            else
                $tmp['multiColor'] = false;

            /*$item->imagePath = $imagePath;
            $item->detailsUrl = route('item_details_page', ['item' => $item->id]);
            $item->vendorUrl = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);
            $item->price_text = $price;*/

            // Blocked Check
            if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
                $tmp['imagePath'] = cdn('images/blocked.jpg');
                $tmp['company_name'] = '';
                $tmp['vendorUrl'] = '';
                $tmp['style_no'] = '';
                $tmp['available_on'] = '';
                $tmp['multiColor'] = false;
                $tmp['detailsUrl'] = '';
                $tmp['price'] = '';

                /*$item->imagePath = cdn('images/blocked.jpg');
                $item->vendor->company_name = '';
                $item->vendorUrl = '';
                $item->style_no = '';
                $item->price = 0;
                $item->price_text = '';
                $item->available_on = '';
                $item->colors->splice(0);*/
            }

            $results[] = $tmp;
        }


        return ['items' => $results, 'pagination' => $paginationView, 'total' => $items->total()];
    }
}
