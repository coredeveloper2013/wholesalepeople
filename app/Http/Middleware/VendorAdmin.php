<?php

namespace App\Http\Middleware;

use App\Enumeration\Role;
use Closure;
use Auth;

class VendorAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check() || Auth::user()->role != Role::$VENDOR) {
            return redirect()->route('login_vendor');
        }

        return $next($request);
    }
}
