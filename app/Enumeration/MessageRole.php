<?php
namespace App\Enumeration;

class MessageRole {
    public static $BUYER = 1;
    public static $VENDOR = 2;
    public static $ADMIN = 3;
}