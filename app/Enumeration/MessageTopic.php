<?php
namespace App\Enumeration;

class MessageTopic {
    public static $GENERAL = 1;
    public static $PRODUCT = 2;
    public static $ORDER = 3;
    public static $PAYMENT = 4;
    public static $SHIPMENT = 5;
    public static $RETURN = 6;
    public static $OTHER = 7;
}