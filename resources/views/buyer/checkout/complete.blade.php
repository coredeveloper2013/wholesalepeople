@extends('layouts.app')

@section('content')
    <div class="container content">
        {{--<div class="row">
            <div class="col-md-12">
                <div class="checkout-steps hidden-xs-down">
                    <a class="active">5. Complete</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>4. Review</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>3. Payment</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>2. Shipping Method</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>1. Address</a>
                </div>

                <div class="checkout-steps d-block d-sm-none">
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>1. Address</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>2. Shipping Method</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>3. Payment</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>4. Review</a>
                    <a class="active">5. Complete</a>
                </div>
            </div>
        </div>--}}

        <h3>Thank you for your order</h3>
        <p>
            <b>Your Order Number is: </b>

            @foreach($orders as $order)
                <a href="{{ route('show_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a>&nbsp;
            @endforeach
        </p>

        <p>We will proceed your order soon.</p>
    </div>
    <script>
        @foreach($orders as $order)
            gtag('event', 'conversion', {
                'send_to': 'AW-794646121/NERHCJeq14cBEOms9foC',
                'value': {{ $order->total }},
                'currency': 'USD',
                'transaction_id': '{{ $order->order_number }}'
            });
        @endforeach
</script>
@stop