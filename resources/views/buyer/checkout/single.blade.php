@extends('layouts.app')

@section('additionalCSS')
    <style>
        .btnApplyCoupon {
            margin-top: 0px;
        }
    </style>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="checkout mb-1">
                <h4>Order Checkout</h4>
            </div>
        </div>
        <form action="{{ route('buyer_single_checkout_post') }}" method="POST">
            @csrf

            <input type="hidden" name="orders" value="{{ request()->get('id') }}" id="orders">

            @foreach($orders as $order)
                <div class="row greyb margin-bottom-1x">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>{{ $order->vendor->company_name }}</h4>

                                <p>
                                    {{ $order->vendor->billing_address }},<br>
                                    {{ $order->vendor->billing_city }},
                                    @if ($order->vendor->billingState == null)
                                        {{ $order->vendor->billing_state }},
                                    @else
                                        {{ $order->vendor->billingState->name }},
                                    @endif
                                    {{ $order->vendor->billingCountry->name }} - {{ $order->vendor->billing_zip }}<br>
                                    Phone: {{ $order->vendor->billing_phone }}
                                </p>
                            </div>

                            <div class="col-md-6 text-right">
                                <a href="" class="btn-default" data-toggle="modal" data-target="#modal_details_{{ $order->id }}">View Details</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{ $errors->has('shipping_method.'.$loop->index) ? 'has-danger' : '' }}">
                                            <label for="small-rounded-input">Shipping Method <span class="required">*</span></label>
                                            <select class="form-control form-control-rounded form-control-sm" name="shipping_method[{{ $loop->index }}]">
                                                <option value="">Select Ship Method</option>
                                                @foreach($order->vendor->shippingMethods as $shipping_method)
                                                    <option value="{{ $shipping_method->id }}" {{ $shipping_method->default == 1 ? 'selected' : '' }}>
                                                        {{ $shipping_method->courier_id == 0 ? 'Other' : $shipping_method->courier->name }} -
                                                        {{ $shipping_method->courier_id == 0 ? $shipping_method->ship_method_text : $shipping_method->shipMethod->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Coupon" id="coupon_{{ $order->id }}" value="{{ $order->coupon }}">
                                    </div>

                                    <div class="col-md-2">
                                        <button class="btn btn-primary btnApplyCoupon" data-order-id="{{ $order->id }}">APPLY</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="small-rounded-input">Message to Vendor</label>
                                <textarea class="mb-2 form-control" placeholder="{{ 'Note for '.$order->vendor->company_name }}" name="note_{{ $order->id }}"></textarea>

                                @if ($order->discount == null || $order->discount == 0)
                                    <span class="check text-right"><b>Order Total: </b>${{ number_format($order->total, 2, '.', '') }}</span>
                                @else
                                    <span class="check text-right"><b>Original Price: </b>${{ number_format($order->total + $order->discount, 2, '.', '') }}</span>
                                    <span class="check text-right"><b>Discount: </b>-${{ number_format($order->discount, 2, '.', '') }}</span>
                                    <span class="check text-right"><b>Order Total: </b>${{ number_format($order->total, 2, '.', '') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="row margin-bottom-2x">
                <div class="col-md-4 mb-3 greyb">
                    <h3>Billing Address</h3>

                    <b>Name: </b>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}<br>
                    <b>Company name: </b>{{ Auth::user()->buyer->company_name }}
                    <p>
                        {{ Auth::user()->buyer->billing_address }}, {{ Auth::user()->buyer->billing_city }}, {{ (Auth::user()->buyer->billingState == null) ? Auth::user()->buyer->billing_state : Auth::user()->buyer->billingState->name }},
                        <br>
                        {{ Auth::user()->buyer->billingCountry->name }} - {{ Auth::user()->buyer->billing_zip }}
                    </p>
                </div>

                <div class="col-md-4 mb-3 greyb">
                    <h3>Shipping Address</h3>

                    <b>Name: </b>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}<br>
                    <b>Company name: </b>{{ Auth::user()->buyer->company_name }}
                    <p id="address_text">
                        @if ($address != null)
                            {{ $address->address }}, {{ $address->city }}, {{ ($address->state == null) ? $address->state_text : $address->state->name }},
                            <br>
                            {{ $address->country->name }} - {{ $address->zip }}
                        @endif
                    </p>

                    <a class="text-info" role="button" id="btnChangeAddress">Change</a> |
                    <a class="text-info" role="button" id="btnAddShippingAddress">Add New Shipping Address</a>
                    <input type="hidden" name="address_id" value="{{ ($address != null) ? $address->id : '' }}" id="address_id">
                </div>

                <div class="col-md-4">
                    <h3>Payment Information</h3>
                    <div class="lockicon"><img src="{{ asset('images/lockicon.png') }}"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('card_id') ? ' has-danger' : '' }}">
                                <select class="form-control" id="select_card" name="card_id">
                                    <option value="">Select Credit Card</option>

                                    @foreach($cards as $card)
                                        <option value="{{ $card->id }}" {{ $card->default == 1 ? 'selected' : '' }}>{{ $card->card_type }} - {{ $card->mask }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <a role="button" id="btnAddNewCard">Add New Card</a>
                        </div>
                    </div>


                    <div class="greyb margin-top-1x mb-3">
                        <h3>Order(s) Total: ${{ number_format($allOrdersTotal, 2, '.', '') }}</h3>

                        <input type="submit" class="btn btn-danger" value="PLACE ORDER">
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('mobile_filter')
    <div class="modal fade" id="addEditShippingModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <form id="modalForm">
                <input type="hidden" id="editAddressId" name="id">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Shipping Address</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="small-rounded-input">Location</label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input location" type="radio" id="locationUS" name="location" value="US" checked>
                                        <label class="custom-control-label" for="locationUS">United States</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input location" type="radio" id="locationCA" name="location" value="CA">
                                        <label class="custom-control-label" for="locationCA">Canada</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input location" type="radio" id="locationInt" name="location" value="INT">
                                        <label class="custom-control-label" for="locationInt">International</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Store No.</label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="store_no" name="store_no">
                                </div>
                            </div>

                            <div class="col-md-7">
                                <div class="form-group" id="form-group-address">
                                    <label for="small-rounded-input">Address <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="address" name="address">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="small-rounded-input">Unit #</label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="unit" name="unit">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="form-group-city">
                                    <label for="small-rounded-input">City <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="city" name="city">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" id="form-group-state">
                                    <label for="small-rounded-input">State <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="state" name="state">
                                </div>

                                <div class="form-group" id="form-group-state-select">
                                    <label for="small-rounded-input">State <span class="required">*</span></label>
                                    <select class="form-control form-control-rounded form-control-sm" id="stateSelect" name="stateSelect">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="form-group-country">
                                    <label for="small-rounded-input">Country <span class="required">*</span></label>
                                    <select class="form-control form-control-rounded form-control-sm" id="country" name="country">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option data-code="{{ $country->code }}" value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" id="form-group-zip">
                                    <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="zipCode" name="zipCode">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="form-group-phone">
                                    <label for="small-rounded-input">Phone <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="phone" name="phone">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="small-rounded-input">Fax</label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="fax" name="fax">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="showroomCommercial" name="showroomCommercial" value="1">
                                        <label class="custom-control-label" for="showroomCommercial">This address is commercial.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary btn-sm" type="button" id="modalBtnAdd">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="selectShippingModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Shipping Address</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-bordered">
                                @foreach($shippingAddresses as $address)
                                    <tr>
                                        <td>
                                            <b>{{ $address->store_no }}</b>
                                            {{ $address->address }}, {{ $address->city }}, {{ ($address->state == null) ? $address->state_text : $address->state->name }},
                                            <br>
                                            {{ $address->country->name }} - {{ $address->zip }}
                                        </td>

                                        <td class="text-center">
                                            <button class="btn btn-primary btnSelectAddress" data-index="{{ $loop->index }}" data-id="{{ $address->id }}">Select</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addCardModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Card</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form id="card-add-form">
                        @csrf

                        <h6>Credit Card Information</h6>
                        <hr class="padding-bottom-1x">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('card_number') ? ' has-danger' : '' }}">
                                    <label for="small-rounded-input">Card Number <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="card_number" name="card_number"
                                           value="{{ old('card_number') }}">
                                </div>

                                <div class="form-group{{ $errors->has('expiration_date') ? ' has-danger' : '' }}">
                                    <label for="small-rounded-input">Expiration Date <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="expiration_date" name="expiration_date"
                                           value="{{ old('expiration_date') }}" placeholder="MM/YY"
                                           data-inputmask="'mask': '99/99'">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('card_name') ? ' has-danger' : '' }}">
                                    <label for="small-rounded-input">Name  <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="card_name" name="card_name"
                                           value="{{ old('card_name') }}">
                                </div>

                                <div class="form-group{{ $errors->has('cvc') ? ' has-danger' : '' }}">
                                    <label for="small-rounded-input">CVC  <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="cvc" name="cvc"
                                           value="{{ old('cvc') }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="default" name="default" value="1"
                                                {{ old('default') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="default">This is default card</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h6>Billing Address</h6>
                        <hr class="padding-bottom-1x">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="small-rounded-input">Location</label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationUS" name="factoryLocation" value="US" {{ (old('factoryLocation') == 'US' || empty(old('factoryLocation'))) ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="factoryLocationUS">United States</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationCA" name="factoryLocation" value="CA" {{ old('factoryLocation') == 'CA' ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="factoryLocationCA">Canada</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationInt" name="factoryLocation" value="INT" {{ old('factoryLocation') == 'INT' ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="factoryLocationInt">International</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('factoryAddress') ? ' has-danger' : '' }}">
                                    <label for="small-rounded-input">Address <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="factoryAddress" name="factoryAddress" value="{{ old('factoryAddress') }}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('factoryCity') ? ' has-danger' : '' }}">
                                    <label for="small-rounded-input">City <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="factoryCity" name="factoryCity" value="{{ old('factoryCity') }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('factoryState') ? ' has-danger' : '' }}" id="form-group-factory-state">
                                    <label for="small-rounded-input">State <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="factoryState" name="factoryState" value="{{ old('factoryState') }}">
                                </div>

                                <div class="form-group{{ $errors->has('factoryStateSelect') ? ' has-danger' : '' }}" id="form-group-factory-state-select">
                                    <label for="small-rounded-input">State <span class="required">*</span></label>
                                    <select class="form-control form-control-rounded form-control-sm" id="factoryStateSelect" name="factoryStateSelect">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('factoryZipCode') ? ' has-danger' : '' }}">
                                    <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="factoryZipCode" name="factoryZipCode" value="{{ old('factoryZipCode') }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('factoryCountry') ? ' has-danger' : '' }}">
                                    <label for="small-rounded-input">Country <span class="required">*</span></label>
                                    <select class="form-control form-control-rounded form-control-sm" id="factoryCountry" name="factoryCountry">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option data-code="{{ $country->code }}" value="{{ $country->id }}" {{ old('factoryCountry') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <a id="btnAddCardSave" class="btn btn-primary">ADD CARD</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @foreach($orders as $order)
        <div class="pdetail modal fade" id="modal_details_{{ $order->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Order Details</h4>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="product-thumbnail"><b>Image</b></th>
                                    <th><b>Style No.</b></th>
                                    <th class="text-center"><b>Color</b></th>
                                    <th class="text-center" colspan="10"><b>Size</b></th>
                                    <th class="text-center"><b>Pack</b></th>
                                    <th class="text-center"><b>Total Qty</b></th>
                                    <th class="text-center"><b>Unit Price</b></th>
                                    <th class="text-center"><b>Amount</b></th>
                                </tr>

                                <tbody>
                                <?php $subTotal = 0; ?>
                                @foreach($order->cartItems as $item_index => $items)
                                    <tr>
                                        <td rowspan="{{ sizeof($items)+1  }}">
                                            @if (sizeof($items[0]->item->images) > 0)
                                                <img src="{{ asset($items[0]->item->images[0]->list_image_path) }}" alt="Product" width="80px">
                                            @else
                                                <img src="{{ asset('images/no-image.png') }}" alt="Product" width="80px">
                                            @endif
                                        </td>

                                        <td rowspan="{{ sizeof($items)+1 }}">
                                            <a href="{{ route('item_details_page', ['item' => $items[0]->item->id]) }}">{{ $items[0]->item->style_no }}</a>
                                        </td>

                                        <td class="text-center text-lg text-medium">
                                            &nbsp;
                                        </td>

                                        <?php
                                        $sizes = explode("-", $items[0]->item->pack->name);
                                        $itemInPack = 0;

                                        for($i=1; $i <= sizeof($sizes); $i++) {
                                            $var = 'pack'.$i;

                                            if ($items[0]->item->pack->$var != null)
                                                $itemInPack += (int) $items[0]->item->pack->$var;
                                        }
                                        ?>

                                        @foreach($sizes as $size)
                                            <th colspan="{{ $loop->last ? 10-sizeof($sizes) +1 : '' }}"><b>{{ $size }}</b></th>
                                        @endforeach

                                        <td>
                                            &nbsp;
                                        </td>

                                        <td>
                                            &nbsp;
                                        </td>

                                        <td>
                                            &nbsp;
                                        </td>

                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>

                                    @foreach($items as $item)
                                        <tr>
                                            <td>
                                                {{ $item->color->name }}
                                            </td>

                                            @for($i=1; $i <= sizeof($sizes); $i++)
                                                <?php $p = 'pack'.$i; ?>
                                                <td colspan="{{ $i == sizeof($sizes) ? 10-$i +1 : '' }}">{{ ($items[0]->item->pack->$p == null) ? '0' : $items[0]->item->pack->$p }}</td>
                                            @endfor

                                            <td>
                                                {{ $item->quantity }}
                                            </td>

                                            <td>
                                                <span class="total_qty">{{ $itemInPack * $item->quantity }}</span>
                                            </td>

                                            <td>
                                                ${{ sprintf('%0.2f', $item->item->price) }}
                                            </td>

                                            <td>
                                                <?php $subTotal += $item->item->price * $itemInPack * $item->quantity; ?>
                                                <span class="total_amount">${{ sprintf('%0.2f', $item->item->price * $itemInPack * $item->quantity) }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="shopping-cart-footer">
                            <div class="column"></div>
                            <div class="column text-lg">
                                Subtotal: <span class="text-medium"><b>${{ sprintf('%0.2f', $order->subtotal) }}</b></span><br>
                                Discount: <span class="text-medium"><b>${{ sprintf('%0.2f', $order->discount) }}</b></span><br>
                                Store Credit: <span class="text-medium"><b>${{ sprintf('%0.2f', $order->store_credit) }}</b></span><br>
                                Total: <span class="text-medium"><b>${{ sprintf('%0.2f', $order->total) }}</b></span><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/inputmask/js/inputmask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/inputmask/js/jquery.inputmask.js') }}"></script>
    <script>
        $(function () {
            var usStates = <?php echo json_encode($usStates); ?>;
            var caStates = <?php echo json_encode($caStates); ?>;
            var shippingAddresses = <?php echo json_encode($shippingAddresses); ?>;
            var oldFactoryState = '';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#expiration_date').inputmask();

            $('#btnContinue').click(function (e) {
                e.preventDefault();

                if ($('#address_id').val() == '') {
                    alert ('Select a Shipping Address.');
                    return;
                } else {
                    $('#form').submit();
                }
            });

            $('#btnChangeAddress').click(function () {
                $('#selectShippingModal').modal('show');
            });

            $('.btnSelectAddress').click(function () {
                var id = $(this).data('id');
                var index = $(this).data('index');

                $('#address_id').val(id);

                var address = shippingAddresses[index];

                $('#address_text').html(address.address + ', ' + address.city + ', ');

                if (address.state == null) {
                    $('#address_text').append(address.state_text + ', ');
                } else {
                    $('#address_text').append(address.state.name + ', ');
                }

                $('#address_text').append('<br>' + address.country.name + ' - ' + address.zip);

                $('#selectShippingModal').modal('hide');
            });

            // Add Shipping Address
            $('#btnAddShippingAddress').click(function () {
                $('#addEditShippingModal').modal('show');
            });

            $('.location').change(function () {
                var location = $('.location:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#country').val('1');
                    else
                        $('#country').val('2');

                    $('#country').prop('disabled', 'disabled');
                    $('#form-group-state-select').show();
                    $('#stateSelect').val('');
                    $('#form-group-state').hide();

                    $('#stateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#country').prop('disabled', false);
                    $('#form-group-state-select').hide();
                    $('#form-group-state').show();
                    $('#country').val('');
                }
            });

            $('.location').trigger('change');

            $('#country').change(function () {
                var countryId = $(this).val();

                if (countryId == 1) {
                    $("#locationUS").prop("checked", true);
                    $('.location').trigger('change');
                } else if (countryId == 2) {
                    $("#locationCA").prop("checked", true);
                    $('.location').trigger('change');
                }
            });

            $('#modalBtnAdd').click(function () {
                if (!shippingAddressValidate()) {
                    $('#country').prop('disabled', false);

                    $.ajax({
                        method: "POST",
                        url: "{{ route('buyer_add_shipping_address') }}",
                        data: $('#modalForm').serialize(),
                    }).done(function( data ) {
                        setAddressId(data.id);
                    });

                    $('#country').prop('disabled', true);
                }
            });

            function setAddressId(id) {
                var orders = $('#orders').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('checkout_address_select') }}",
                    data: { shippingId: id, id: orders },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            }

            $('#addEditShippingModal').on('hide.bs.modal', function (event) {
                $("#locationUS").prop("checked", true);
                $('.location').trigger('change');

                $('#store_no').val('');
                $('#address').val('');
                $('#unit').val('');
                $('#city').val('');
                $('#stateSelect').val('');
                $('#state').val('');
                $('#zipCode').val('');
                $('#phone').val('');
                $('#fax').val('');
                $('#showroomCommercial').prop('checked', false);

                clearModalForm();
            });

            $('.btnApplyCoupon').click(function (e) {
                e.preventDefault();

                var orderId = $(this).data('order-id');
                var coupon = $('#coupon_'+orderId).val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('buyer_apply_coupon') }}",
                    data: { id: orderId, coupon: coupon }
                }).done(function( data ) {
                    if (data.success) {
                        location.reload();
                    } else {
                        alert(data.message);
                    }
                });
            });

            function clearModalForm() {
                $('#form-group-address').removeClass('has-danger');
                $('#form-group-city').removeClass('has-danger');
                $('#form-group-state-select').removeClass('has-danger');
                $('#form-group-state').removeClass('has-danger');
                $('#form-group-country').removeClass('has-danger');
                $('#form-group-zip').removeClass('has-danger');
                $('#form-group-phone').removeClass('has-danger');
            }

            function shippingAddressValidate() {
                var error = false;
                var location = $('.location:checked').val();

                clearModalForm();

                if ($('#address').val() == '') {
                    $('#form-group-address').addClass('has-danger');
                    error = true;
                }

                if ($('#city').val() == '') {
                    $('#form-group-city').addClass('has-danger');
                    error = true;
                }

                if ((location == 'US' || location == 'CA') && $('#stateSelect').val() == '') {
                    $('#form-group-state-select').addClass('has-danger');
                    error = true;
                }

                if (location == 'INT' && $('#state').val() == '') {
                    $('#form-group-state').addClass('has-danger');
                    error = true;
                }

                if ($('#country').val() == '') {
                    $('#form-group-country').addClass('has-danger');
                    error = true;
                }

                if ($('#zipCode').val() == '') {
                    $('#form-group-zip').addClass('has-danger');
                    error = true;
                }

                if ($('#phone').val() == '') {
                    $('#form-group-phone').addClass('has-danger');
                    error = true;
                }

                return error;
            }

            // Credit Card Add
            $('.factoryLocation').change(function () {
                var location = $('.factoryLocation:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#factoryCountry').val('1');
                    else
                        $('#factoryCountry').val('2');

                    $('#factoryCountry').prop('disabled', 'disabled');
                    $('#form-group-factory-state-select').show();
                    $('#factoryStateSelect').val('');
                    $('#form-group-factory-state').hide();

                    $('#factoryStateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#factoryCountry').prop('disabled', false);
                    $('#form-group-factory-state-select').hide();
                    $('#form-group-factory-state').show();
                }
            });

            $('.factoryLocation').trigger('change');

            $('#btnAddCardSave').click(function (e) {
                $('#factoryCountry').prop('disabled', false);

                $.ajax({
                    method: "POST",
                    url: "{{ route('buyer_single_checkout_add_card_post') }}",
                    data: $('#card-add-form').serialize(),
                }).done(function( data ) {
                    if (data.success) {
                        $('#addCardModal').modal('hide');
                        $('#select_card').append('<option selected value="'+data.message.id+'">'+data.message.card_type + ' - ' +data.message.mask+'</option>');
                    } else {
                        alert(data.message);
                    }
                });

                $('#factoryCountry').prop('disabled', true);
            });

            $('#btnAddNewCard').click(function () {
                $('#addCardModal').modal('show');
            });

            $("#addCardModal").on("show", function () {
                $("body").addClass("modal-open");
            }).on("hidden", function () {
                $("body").removeClass("modal-open")
            });
        });
    </script>
@stop