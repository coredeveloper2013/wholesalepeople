@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="" id="accordionSearch" role="tablist">
                <div class="card">
                    <div class="card-header" role="tab" id="headingSearch">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#collapseSearch" role="button" aria-expanded="true" aria-controls="collapseSearch" class="">Search</a>
                        </h5>
                    </div>
                    <div id="collapseSearch" class="collapse show" role="tabpanel" aria-labelledby="headingSearch" data-parent="#accordionSearch" style="">
                        <div class="card-body">
                            <form action="{{ route('admin_all_buyer') }}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <b>Search: &nbsp;&nbsp;</b>
                                        <label class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="searchStyleNo" type="checkbox" name="name" value="1" checked>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Name</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="searchVendorstyleNo" type="checkbox" value="1" name="company_name" {{ request()->get('company_name') == '1' ? 'checked' : '' }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Company Name</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="searchDescription" type="checkbox" value="1" name="active" {{ request()->get('active') == '1' ? 'checked' : '' }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Active</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="searchItemName" type="checkbox" value="1" name="verified" {{ request()->get('verified') == '1' ? 'checked' : '' }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Verified</span>
                                        </label>

                                        <label class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="searchItemName" type="checkbox" value="1" name="block" {{ request()->get('block') == '1' ? 'checked' : '' }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Block</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <input class="form-control" id="inputText" value="{{ request()->get('s') }}" name="s" autocomplete="off">
                                    </div>

                                    <div class="col-md-8">
                                        <input class="btn btn-primary" id="btnSearch" value="Search" type="submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Company Name</th>
                    <th>Active</th>
                    <th>Verified</th>
                    <th>Block</th>
                    <th>Created At</th>
                    <th>Files</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($buyers as $buyer)
                    <tr>
                        <td>{{ $buyer->user->first_name.' '.$buyer->user->last_name }}</td>
                        <td>{{ $buyer->company_name }}</td>
                        <td>
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" data-id="{{ $buyer->id }}" class="custom-control-input status" value="1" {{ $buyer->active == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>
                        </td>
                        <td>
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" data-id="{{ $buyer->id }}" class="custom-control-input verified" value="1" {{ $buyer->verified == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>
                        </td>
                        <td>
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" data-id="{{ $buyer->id }}" class="custom-control-input block" value="1" {{ $buyer->block == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>
                        </td>
                        <td>{{ date('m/d/Y g:i:s a', strtotime($buyer->created_at)) }}</td>
                        <td>
                            @if ($buyer->ein_path != null)
                                @if (pathinfo($buyer->ein_path, PATHINFO_EXTENSION) != 'pdf')
                                    <a class="show-image" data-href="{{ asset($buyer->ein_path) }}" href="#">EIN</a>
                                @else
                                    <a href="{{ asset($buyer->ein_path) }}" download>EIN</a> &nbsp;
                                @endif
                            @endif

                            @if ($buyer->sales1_path != null)
                                @if (pathinfo($buyer->sales1_path, PATHINFO_EXTENSION) != 'pdf')
                                    <a class="show-image" data-href="{{ asset($buyer->sales1_path) }}" href="#">Sales 1</a>
                                @else
                                    <a href="{{ asset($buyer->sales1_path) }}" download>Sales 1</a> &nbsp;
                                @endif
                            @endif

                            @if ($buyer->sales2_path != null)
                                @if (pathinfo($buyer->sales2_path, PATHINFO_EXTENSION) != 'pdf')
                                    <a class="show-image" data-href="{{ asset($buyer->sales2_path) }}" href="#">Sales 2</a>
                                @else
                                    <a href="{{ asset($buyer->sales2_path) }}" download>Sales 2</a> &nbsp;
                                @endif
                            @endif
                        </td>
                        <td>
                            <a class="btnEdit" href="{{ route('admin_buyer_edit', ['buyer' => $buyer->id]) }}" style="color: blue">Edit</a> |
                            <a class="btnDelete" data-id="{{ $buyer->id }}" role="button" style="color: red">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="pagination">
                {{ $buyers->appends($appends)->links() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalShowImage" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelLarge">Document</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="img" src="" width="100%">
                </div>

                <div class="modal-footer">
                    <a id="btnDownload" class="btn btn-light" download>Download</a>
                    <button class="btn btn-light" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.status').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_buyer_change_status') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

            $('.verified').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_buyer_change_verified') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

            $('.block').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_buyer_change_block') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

            $('.show-image').click(function (e) {
                e.preventDefault();

                var url = $(this).data('href');

                $('#img').attr('src', url);
                $('#btnDownload').attr('href', url);
                $('#modalShowImage').modal('show');
            });

            var selectedId;

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_buyer_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        });
    </script>
@stop