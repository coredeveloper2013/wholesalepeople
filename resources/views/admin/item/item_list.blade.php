@extends('admin.layouts.admin')

@section('additionalCSS')

@stop

@section('content')
    <div id="accordionSearch" role="tablist">
        <div class="card">
            <div class="card-header" role="tab" id="headingSearch">
                <h5 class="mb-0">
                    <a data-toggle="collapse" href="#collapseSearch" role="button" aria-expanded="true" aria-controls="collapseSearch" class="">Search</a>
                </h5>
            </div>
            <div id="collapseSearch" class="collapse show" role="tabpanel" aria-labelledby="headingSearch" data-parent="#accordionSearch" style="">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            <b>Search: &nbsp;&nbsp;</b>
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="searchStyleNo"
                                        {{ (request()->get('style') == '1' || request()->get('style') == null) ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Style No.</span>
                            </label>

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="searchVendorstyleNo"
                                        {{ (request()->get('vendorStyle') == '1') ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Vendor Style No.</span>
                            </label>

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="searchDescription"
                                        {{ (request()->get('des') == '1') ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Description</span>
                            </label>

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="searchItemName"
                                        {{ (request()->get('name') == '1') ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Item Name</span>
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <input class="form-control" id="inputText" value="{{ request()->get('text') }}">
                        </div>

                        <div class="col-md-2">
                            <select class="form-control" id="select-vendor">
                                <option value="">All Vendor</option>

                                @foreach($vendors as $vendor)
                                    <option value="{{ $vendor->id }}" {{ request()->get('v') == $vendor->id ? 'selected' : '' }}>{{ $vendor->company_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-4">
                            <button class="btn btn-primary" id="btnSearch">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br>

    <div id="accordion" role="tablist">
        <div class="card">
            <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                    <a data-toggle="collapse" href="#collapseOne" role="button" aria-expanded="true" aria-controls="collapseOne" class="">Active Items - {{ sizeof($activeItems) }} Items</a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <select class="form-control" id="selectSortActiveItems">
                                <option value="1" {{ request()->get('s1') == '1' ? 'selected' : '' }}>Last Update</option>
                                <option value="2" {{ request()->get('s1') == '2' ? 'selected' : '' }}>Upload Date</option>
                                <option value="3" {{ (request()->get('s1') == null || request()->get('s1') == '3') ? 'selected' : ''  }}>Activation Date</option>
                                <option value="4" {{ request()->get('s1') == '4' ? 'selected' : '' }}>Price Low to High</option>
                                <option value="5" {{ request()->get('s1') == '5' ? 'selected' : '' }}>Price High to Low</option>
                                <option value="6" {{ request()->get('s1') == '6' ? 'selected' : '' }}>Style No.</option>
                            </select>
                        </div>

                        <div class="col-md-10 text-right">
                            <button class="btn btn-default" id="btnSelectAllActive">Select All</button>
                            <button class="btn btn-default" id="btnDeselectAllActive">Deselect All</button>
                            <button class="btn btn-primary" id="btnDeactive">Deactivate</button>
                        </div>
                    </div>

                    <br>
                    <hr>

                    <ul class="il-product-container">
                        @foreach($activeItems as $item)
                            <li>
                                <div class="il-item-container">
                                    <div class="il-item-top">
                                        <div class="left">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input checkbox-active-items" data-id="{{ $item->id }}">
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                            <br>

                                            <a class="btnEditItem" href="{{ route('admin_item_edit', ['item' => $item->id]) }}" title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>

                                        <div class="right">
                                            <div class="il-item-img">
                                                <a href="{{ route('admin_item_edit', ['item' => $item->id]) }}" class="text-primary il-item-style-no btnEditItem">
                                                    @if (sizeof($item->images) > 0)
                                                        <img src="{{ asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                                    @else
                                                        <img src="{{ asset('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                                    @endif
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="il-item-footer text-center">
                                        <div class="catname">{{ $item->categories->last()->name or '' }}</div><br>
                                        <a href="{{ route('admin_item_edit', ['item' => $item->id]) }}" class="text-primary il-item-style-no btnEditItem">{{ $item->style_no }}</a>
                                        @if ($item->orig_price != null)
                                            <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                        @endif
                                        ${{ number_format($item->price, 2, '.', '') }} <br>
                                        <span class="text-muted">{{ date('m/d/Y - h:i:s a', strtotime($item->created_at)) }}</span>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>

                    {{ $activeItems->appends($appends)->links() }}
                </div>
            </div>
        </div>
    </div><br>

    <div id="accordionTwo" role="tablist">
        <div class="card">
            <div class="card-header" role="tab" id="headingTwo">
                <h5 class="mb-0">
                    <a data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="true" aria-controls="collapseTwo" class="">Inactive Items - {{ sizeof($inactiveItems) }} Items</a>
                </h5>
            </div>

            <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionTwo" style="">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <select class="form-control" id="selectSortInactiveItems">
                                <option value="1" {{ request()->get('s2') == '1' ? 'selected' : '' }}>Last Update</option>
                                <option value="2" {{ request()->get('s2') == '2' ? 'selected' : '' }}>Upload Date</option>
                                <option value="3" {{ (request()->get('s2') == null || request()->get('s2') == '3') ? 'selected' : ''  }}>Activation Date</option>
                                <option value="4" {{ request()->get('s2') == '4' ? 'selected' : '' }}>Price Low to High</option>
                                <option value="5" {{ request()->get('s2') == '5' ? 'selected' : '' }}>Price High to Low</option>
                                <option value="6" {{ request()->get('s2') == '6' ? 'selected' : '' }}>Style No.</option>
                            </select>
                        </div>


                        <div class="col-md-10 text-right">
                            <button class="btn btn-default" id="btnSelectAllInactive">Select All</button>
                            <button class="btn btn-default" id="btnDeselectAllInactive">Deselect All</button>
                            <button class="btn btn-danger" id="btnDelete">Delete</button>
                            <button class="btn btn-primary" id="btnActive">Active</button>
                        </div>
                    </div>

                    <br>
                    <hr>

                    <ul class="il-product-container">
                        @foreach($inactiveItems as $item)
                            <li>
                                <div class="il-item-container">
                                    <div class="il-item-top">
                                        <div class="left">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input checkbox-inactive-items" data-id="{{ $item->id }}">
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                            <br>

                                            <a class="btnEditItem" href="{{ route('admin_item_edit', ['item' => $item->id]) }}" title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>

                                        <div class="right">
                                            <div class="il-item-img">
                                                <a href="{{ route('admin_item_edit', ['item' => $item->id]) }}" class="text-primary il-item-style-no btnEditItem">
                                                    @if (sizeof($item->images) > 0)
                                                        <img src="{{ asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                                    @else
                                                        <img src="{{ asset('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                                    @endif
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="il-item-footer text-center">
                                        <div class="catname">{{ $item->categories->last()->name or '' }}</div><br>
                                        <a href="{{ route('admin_item_edit', ['item' => $item->id]) }}" class="text-primary il-item-style-no btnEditItem">{{ $item->style_no }}</a>
                                        ${{ number_format($item->price, 2, '.', '') }} <br>
                                        <span class="text-muted">{{ date('m/d/Y - h:i:s a', strtotime($item->created_at)) }}</span>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>

                    {{ $inactiveItems->appends($appends)->links() }}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#selectSortActiveItems, #selectSortInactiveItems, #select-vendor').change(function () {
                checkParameters();
            });

            $('#btnSearch').click(function () {
                search();
            });

            $('#inputText').keypress(function(e) {
                if(e.which == 13) {
                    search();
                }
            });

            $('#btnSelectAllActive').click(function () {
                $('.checkbox-active-items').prop('checked', true);
            });

            $('#btnDeselectAllActive').click(function () {
                $('.checkbox-active-items').prop('checked', false);
            });

            $('#btnSelectAllInactive').click(function () {
                $('.checkbox-inactive-items').prop('checked', true);
            });

            $('#btnDeselectAllInactive').click(function () {
                $('.checkbox-inactive-items').prop('checked', false);
            });

            $('#btnDeactive').click(function () {
                var ids = [];

                $('.checkbox-active-items').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('vendor_item_list_change_to_inactive') }}",
                        data: {ids: ids}
                    }).done(function (msg) {
                        window.location.reload();
                    });
                }
            });

            $('#btnActive').click(function () {
                var ids = [];

                $('.checkbox-inactive-items').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('vendor_item_list_change_to_active') }}",
                        data: {ids: ids}
                    }).done(function (msg) {
                        window.location.reload();
                    });
                }
            });

            var selectedIds = [];

            $('#btnDelete').click(function () {
                var ids = [];

                $('.checkbox-inactive-items').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                selectedIds = ids;
                $('#deleteModal').modal('show');
            });

            $('#modalBtnDelete').click(function () {
                if (selectedIds.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('vendor_item_list_delete') }}",
                        data: {ids: selectedIds}
                    }).done(function (msg) {
                        window.location.reload();
                    });
                }
            });

            function checkParameters() {
                var s1 = $('#selectSortActiveItems').val();
                var s2 = $('#selectSortInactiveItems').val();
                var v = $('#select-vendor').val();

                var parameters = <?php echo json_encode(request()->all()); ?>;
                var url = '{{ route('admin_item_list') }}' + '?s1=' + s1 + '&s2=' + s2 + '&v=' + v;

                $.each(parameters, function (key, value) {
                    if (key != 's1' && key != 's2' && key != 'p1' && key != 'p2' && key != 'v') {
                        var val = '';

                        if (value != null)
                            val = value;

                        url += '&' + key + '=' + val;
                    }
                });
                window.location.replace(url);
            }

            function search() {
                var s1 = $('#selectSortActiveItems').val();
                var s2 = $('#selectSortInactiveItems').val();
                var v = $('#select-vendor').val();
                var text = $('#inputText').val();
                var searchStyleNo = ($('#searchStyleNo').is(':checked')) ? 1 : 0;
                var vendorStyle = ($('#searchVendorstyleNo').is(':checked')) ? 1 : 0;
                var description = ($('#searchDescription').is(':checked')) ? 1 : 0;
                var name = ($('#searchItemName').is(':checked')) ? 1 : 0;

                var url = '{{ route('admin_item_list') }}' + '?s1=' + s1 + '&s2=' + s2 + '&text=' + text + '&style=' + searchStyleNo +
                    '&vendorStyle=' + vendorStyle + '&des=' + description + '&name=' + name + '&v=' + v;
                window.location.replace(url);
            }

            $('.btnEditItem').click(function () {
                localStorage['item_list_position'] = $(document).scrollTop()+'';
            });

            var changePosition = localStorage['change_position'];
            if (changePosition) {
                localStorage.removeItem('change_position');

                var position = parseInt(localStorage.getItem('item_list_position'));

                window.scrollTo(0, position);
            }
        });
    </script>
@stop
