@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Business Name</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($vendors as $vendor)
                    <tr>
                        <td>{{ $vendor->company_name }}</td>
                        <td>{{ $vendor->business_name }}</td>
                        <td>{{ date('m/d/Y g:i:s a') }}</td>
                        <td>
                            <button class="btn btn-primary btnVerify" data-id="{{ $vendor->id }}">Verify</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.btnVerify').click(function () {
                var id = $(this).data('id');
                var tr = $(this).closest('tr');

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_brand_change_verified_status') }}",
                    data: { id: id, status: 1 }
                }).done(function( msg ) {
                    tr.remove();
                    toastr.success('Verified!');
                });
            });
        });
    </script>
@stop