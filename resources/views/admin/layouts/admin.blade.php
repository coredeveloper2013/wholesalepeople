<?php
    use App\Enumeration\Page;
?>

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Wholesalepeople') }}</title>

    <!-- Styles -->
    <link href="{{ asset('themes/admire/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/admire/css/custom.css') }}" rel="stylesheet">
    @yield('additionalCSS')
</head>

<body class="body">
    <div id="top">
        <!-- .navbar -->
        <nav class="navbar navbar-static-top">
            <div class="container-fluid m-0">
                <a class="navbar-brand" href="{{ route('admin_dashboard') }}">
                    <h4>ADMIN PANEL</h4>
                </a>
                <div class="menu mr-sm-auto">
                    <span class="toggle-left" id="menu-toggle">
                        <i class="fa fa-bars"></i>
                    </span>
                </div>
                <div class="topnav dropdown-menu-right">
                    <a class="btn btn-default btn-sm messages btn-message" href="{{ route('admin_view_messages') }}">
                        <i class="fa fa-envelope-o fa-1x"></i>

                        @if ($unread_msg > 0)
                            <span class="badge badge-pill badge-warning notifications_badge_top">{{ $unread_msg }}</span>
                        @endif
                    </a>

                    <div class="btn-group">
                        <div class="user-settings no-bg">
                            <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
                                <img src="{{ asset('images/default-avatar.png') }}" class="admin_img2 img-thumbnail rounded-circle avatar-img"
                                     alt="avatar"> <strong>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</strong>
                                <span class="fa fa-sort-down white_bg"></span>
                            </button>
                            <div class="dropdown-menu admire_admin">
                                <a class="dropdown-item title" href="#">
                                    Admin</a>
                                <a id="btnLogOut" class="dropdown-item" href="#"><i class="fa fa-sign-out"></i>
                                    Log Out</a>

                                <form id="logoutForm" class="" action="{{ route('logout_admin') }}" method="post">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- /.navbar -->
        <!-- /.head -->
    </div>
    <!-- /#top -->

    <div class="wrapper">
        <div id="left">
            <div class="menu_scroll">
                <ul id="menu">
                    <li class="collapse">
                        <a href="javascript:;">
                            <span class="link-title menu_hide">&nbsp; Main Setting</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul class="collapse">
                            <li>
                                <a href="{{ route('admin_category') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Top Menu Categories
                                </a>
                            </li>

                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Attribute
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="sub-menu sub-submenu">
                                    <li>
                                        <a href="{{ route('admin_master_color') }}">
                                            <span class="link-title menu_hide">&nbsp;Master Color</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin_master_fabric') }}">
                                            <span class="link-title menu_hide">&nbsp;Master Fabric</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin_industry') }}">
                                            <span class="link-title menu_hide">&nbsp;Industry</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin_body_size') }}">
                                            <span class="link-title menu_hide">&nbsp;Body Size</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin_pattern') }}">
                                            <span class="link-title menu_hide">&nbsp;Pattern</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin_length') }}">
                                            <span class="link-title menu_hide">&nbsp;Length</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin_style') }}">
                                            <span class="link-title menu_hide">&nbsp;Style</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin_courier') }}">
                                            <span class="link-title menu_hide">&nbsp;Courier</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin_ship_method') }}">
                                            <span class="link-title menu_hide">&nbsp;Ship Method</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Banner
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Announcement
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_buyer_home') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Buyer Home Page
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_welcome_notification') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Home Page
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_opening_soon') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Opening Soon
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="collapse">
                        <a href="javascript:;">
                            <span class="link-title menu_hide">&nbsp; Brand</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul class="collapse">
                            <li>
                                <a href="{{ route('admin_brand_all') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;All Brand
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_create_brand') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Create Brand
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_active_brand') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Active Brand
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_deactivate_brand') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Deactivate Brand
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_pending_brand') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Pending Brand
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="collapse">
                        <a href="javascript:;">
                            <span class="link-title menu_hide">&nbsp; Items</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Item Upload
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Data Import
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Create a Item
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_item_list') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Item List
                                </a>
                            </li>

                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Item Setting
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="sub-menu sub-submenu">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Category
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin_color') }}">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Color
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin_pack') }}">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Pack
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin_fabric') }}">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Fabric
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="collapse">
                        <a href="javascript:;">
                            <span class="link-title menu_hide">&nbsp; Orders</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul class="collapse">
                            <li>
                                <a href="{{ route('admin_all_orders') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;All Orders
                                </a>
                            </li>

                            <!--<li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Create a New Order
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Order by Brand
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Order Complete
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Order Pending
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Order Cancelled
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Order Processed
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Order Pre Order
                                </a>
                            </li>-->

                            <li>
                                <a href="{{ route('admin_new_orders') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;New Order 
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin_incomplete_orders') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Incomplete Checkout 
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="collapse">
                        <a href="javascript:;">
                            <span class="link-title menu_hide">&nbsp; Customers</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul class="collapse">
                            <li>
                                <a href="{{ route('admin_all_buyer') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;All Customer
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Active Customer
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Deactivate Customer
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Blocked Customer
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Pending Customer
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="collapse">
                        <a href="javascript:;">
                            <span class="link-title menu_hide">&nbsp; Feedback</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul class="collapse">
                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;All Feedback
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Received Feedback
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Reply Feedback
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="collapse">
                        <a href="javascript:;">
                            <span class="link-title menu_hide">&nbsp; Category Banner</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul class="collapse">
                            <li>
                                <a href="{{ route('admin_category_banner_vendor_all') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Vendor All
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin_category_banner_new_arrivals') }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;New Arrivals
                                </a>
                            </li>
                            @foreach($categories as $category)
                                <li>
                                    <a href="{{ route('admin_category_banner', ['category' => $category->id]) }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;{{ $category->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>

                    <li class="collapse">
                        <a href="javascript:;">
                            <span class="link-title menu_hide">&nbsp; Meta</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul class="collapse">
                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$HOME]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Home
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$VENDOR_ALL]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Vendor All
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$NEW_ARRIVAL]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;New Arrival
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$ABOUT_US]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;About Us
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$CONTACT_US]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Contact Us
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$RETURN_EXCHANGE]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Returns & Exchange
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$SHOW_SCHEDULE]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Show Schedule
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$PHOTO_SHOOT]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Photo Shoot
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$SHIPPING_INFORMATION]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Shipping information
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$REFUND_POLICY]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Refunds Policy
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$PRIVACY_POLICY]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Privacy Policy
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin_meta_page', ['page' => Page::$TERMS_CONDITIONS]) }}">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Terms and Conditions
                                </a>
                            </li>

                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Category
                                    <span class="fa arrow"></span>
                                </a>

                                <ul class="sub-menu sub-submenu">
                                    @foreach($categories as $category)
                                        <li>
                                            <a href="{{ route('admin_meta_category', ['category' => $category->id]) }}">
                                                <span class="link-title menu_hide">&nbsp;{{ $category->name }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Vendor
                                    <span class="fa arrow"></span>
                                </a>

                                <ul class="sub-menu sub-submenu">
                                    @foreach($vendors as $vendor)
                                        <li>
                                            <a href="{{ route('admin_meta_vendor', ['vendor' => $vendor->id]) }}">
                                                <span class="link-title menu_hide">&nbsp;{{ $vendor->company_name }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- /#menu -->
            </div>
        </div>
        <!-- /#left -->

        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                {{ $page_title or '' }}
                            </h4>
                        </div>
                    </div>
                </div>
            </header>

            <div class="outer">
            	<div class="inner bg-container">
            		@yield('content')
        		</div>
                <!-- /.inner -->
            </div>
            <!-- /.outer -->
       	</div>
        <!-- /#content -->
    </div>
    <!--wrapper-->
	

    <!-- global scripts-->
	<script type="text/javascript" src="{{ asset('themes/admire/js/components.js') }}"></script>
	<script type="text/javascript" src="{{ asset('themes/admire/js/custom.js') }}"></script>
	<!--end of global scripts-->
    <script>
        $(function () {
            $('#btnLogOut').click(function () {
                $('#logoutForm').submit();
            });
        })
    </script>
	@yield('additionalJS')
</body>
</html>
