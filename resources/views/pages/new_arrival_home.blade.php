<?php
use App\Enumeration\Availability;
use App\Enumeration\Role;
?>

@extends('layouts.app')

@section('additionalCSS')

@stop

@section('content')
    <div class="container-fluid">
        @if ($topBannerUrl != '')
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($topBannerVendor)]) }}">
                        <img src="{{ $topBannerUrl }}" width="100%">
                    </a>
                </div>
            </div>
        @endif

        {{ Breadcrumbs::render('new_arrival_home') }}
        <div class="row">
            <div class="col-xl-2 col-lg-2 order-lg-1">
                <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
                <aside class="sidebar-offcanvas">
                    <section class="widget widget-categories">
                        <h3 class="widget-title">CATEGORY</h3>
                        <ul>
                            @foreach($parentCategories as $category)
                                <li><a href="{{ route('new_arrival_page', ['C' => $category->id]) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>
                    </section>

                    <section class="widget widget-categories">
                        <h3 class="widget-title">DATES</h3>
                        <ul>
                            @foreach($byArrivalDate as $item)
                                <li><a href="{{ route('new_arrival_page', ['D' => $item['day']]) }}">{{ $item['name'] }}</a><span>({{ $item['count'] }})</span></li>
                            @endforeach
                        </ul>
                    </section>

                    <section class="widget widget-categories">
                        <h3 class="widget-title">SEARCH</h3>

                        <form action="{{ route('new_arrival_page') }}" method="GET">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" id="search-style-no" name="search-by" value="1" checked>
                                <label class="custom-control-label" for="search-style-no">Style No.</label>
                            </div>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" id="search-description" name="search-by" value="2">
                                <label class="custom-control-label" for="search-description">Description</label>
                            </div>

                            <div class="form-group">
                                <input class="form-control" id="search-input" type="text" placeholder="Search" name="search">
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <input class="form-control" id="search-price-min" type="text" placeholder="Price Min" name="price_min">
                                </div>

                                <div class="col-md-6">
                                    <input class="form-control" id="search-price-max" type="text" placeholder="Price Max" name="price_max">
                                </div>
                            </div>

                            <button class="btn btn-primary" id="btn-search">SEARCH</button>
                        </form>
                    </section>

                    <section class="widget">
                        <h3 class="widget-title">COLORS</h3>

                        <ul class="sidecolor">
                            @foreach($masterColors as $mc)
                                <li class="item-color" title="{{ $mc->name }}">

                                    <a href="{{ route('new_arrival_page', ['color' => $mc->id]) }}">
                                        <img src="{{ cdn($mc->image_path) }}" width="30px" height="20px">
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </section>
                </aside>
            </div>
            <div class="col-xl-10 col-lg-10 order-lg-2">
                @if (sizeof($topSliderVendors) > 0)
                    <section class="category-top-slider margin-bottom-1x d-none d-sm-block">
                        @foreach($topSliderVendors as $vendor)
                            <h4 id="cts_title_{{ $vendor->id }}" class="cts_title d-none">{{ $vendor->company_name }}</h4>
                        @endforeach

                        <div class="cts-item-container">
                            @foreach($topSliderVendors as $vendor)
                                <ul id="cts_items_ul_{{ $vendor->id }}" class="d-none cts_items_ul">
                                    @foreach($vendor->items as $item)
                                        <li>
                                            <a href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                                @if (sizeof($item->images) > 0)
                                                    <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }} - Stylepick.net">
                                                @else
                                                    <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                                @endif

                                                <div class="cts-style-no text-center">
                                                    {{ $item->style_no }}
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endforeach

                            <div class="cts-navigation text-center">
                                @foreach($topSliderVendors as $vendor)
                                    <span class="cts-dot cts_nav" data-id="{{ $vendor->id }}"></span>
                                @endforeach
                            </div>
                        </div>
                    </section> {{--End top slider--}}
                @endif

                @if (sizeof($smallBannerVendors) > 0)
                    <section class="category-page-small-banners margin-top-1x margin-bottom-1x">
                        <ul>
                            @foreach($smallBannerVendors as $vendor)
                                <li>
                                    <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                                        <img src="{{ cdn($vendor->images[0]->image_path) }}" alt="{{ $vendor->company_name }}">
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </section>
                @endif

                @if (sizeof($secondSliderVendors) > 0)
                    <section class="category-slider-2 d-none d-sm-block">
                        <div class="cs2-title">
                            <div class="row">
                                <div class="col-md-6">
                                    @foreach($secondSliderVendors as $vendor)
                                        <h4 class="cs2_title d-none" id="cs2_title_{{ $vendor->id }}">{{ $vendor->company_name }}</h4>
                                    @endforeach
                                </div>

                                <div class="col-md-6 text-right">
                                    @foreach($secondSliderVendors as $vendor)
                                        <span class="cts-dot cs2_nav" data-id="{{ $vendor->id }}"></span>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="cts-item-container">
                            @foreach($secondSliderVendors as $vendor)
                                <ul class="cs2_items_ul d-none" id="cs2_items_ul_{{ $vendor->id }}">
                                    @foreach($vendor->items as $item)
                                        <li>
                                            <a href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                                @if (sizeof($item->images) > 0)
                                                    <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }} - Stylepick.net">
                                                @else
                                                    <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                                @endif
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endforeach
                        </div>
                    </section> {{--category slider 2--}}
                @endif

                @if (sizeof($newVendors) > 0)
                    <section class="section-vendor margin-top-1x margin-bottom-1x hidden-md-down">
                        <div class="block-title">
                            <h3>New Vendors</h3>
                            <span><a href="/new_arrival/home">+ More</a></span>
                        </div>
                        <div class="new-vendor-slider">
                            <ul class="nvs-tab">
                                @foreach($newVendors as $vendor)
                                    <li class="nvs-tab-item" data-id="{{ $vendor->id }}"><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">{{ $vendor->company_name }}</a></li>
                                @endforeach
                            </ul>

                            @foreach($newVendors as $vendor)
                                <div class="nvs-items-container d-none" id="nvs-items-container-{{ $vendor->id }}">
                                    <div class="nvs-banner">
                                        <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                                            <img src="{{ cdn($vendor->images[0]->image_path) }}" alt="{{ $vendor->company_name }}">
                                        </a>
                                    </div>

                                    <ul class="nvs-items nvs-items-4x">
                                        @foreach($vendor->items as $item)
                                            <li>
                                                <a href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                                    @if (sizeof($item->images) > 0)
                                                        <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }} - Stylepick.net">
                                                    @else
                                                        <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                                    @endif
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach
                        </div>
                    </section>
                @endif

                @if (sizeof($newArrivals) > 0)
                    <section class="section-new-arrival margin-bottom-1x">
                        <div class="block-title">
                            <h3>New Arrivals</h3>
                            <span><a href="/new_arrival/home">+ More</a></span>
                        </div>

                        <ul class="product-container-6x">
                            <?php $s = (sizeof($newArrivals) >= 6 ? 6 : sizeof($newArrivals)); ?>

                            @for($i=0; $i<$s; $i++)
                                <?php $item = $newArrivals[$i]; ?>
                                <li>
                                    <div class="product-card">
                                        <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                                @if (sizeof($item->images) > 0)
                                                    <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }} - Stylepick.net">
                                                @else
                                                    <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                                @endif
                                        </a>

                                        <div class="product-title"><b><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($item->vendor->company_name)]) }}" class="vendor-name">{{ $item->vendor->company_name }}</a></b></div>
                                        <h3 class="product-title">
                                            <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>

                                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                <span class="price">
                                                    @if ($item->orig_price != null)
                                                        <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                                    @endif

                                                    ${{ sprintf('%0.2f', $item->price) }}
                                                </span>
                                            @endif
                                        </h3>
                                        <div class="product-extra-info">
                                            @if (sizeof($item->colors) > 1)
                                                <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">
                                            @endif
                                            @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                                <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @endfor
                        </ul>

                        @if (sizeof($newArrivals) > 6)
                            <ul class="product-container-6x">
                                @for($i=6; $i<sizeof($newArrivals); $i++)
                                    <?php $item = $newArrivals[$i]; ?>
                                    <li>
                                        <div class="product-card">
                                            <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                                    @if (sizeof($item->images) > 0)
                                                        <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }} - Stylepick.net">
                                                    @else
                                                        <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                                    @endif
                                            </a>

                                            <div class="product-title">
                                                <b><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($item->vendor->company_name)]) }}" class="vendor-name">{{ $item->vendor->company_name }}</a></b>
                                            </div>
                                            <h3 class="product-title">
                                                <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>

                                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                    <span class="price">
                                                        @if ($item->orig_price != null)
                                                            <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                                        @endif
                                                        ${{ sprintf('%0.2f', $item->price) }}
                                                    </span>
                                                @endif
                                            </h3>
                                            <div class="product-extra-info">
                                                @if (sizeof($item->colors) > 1)
                                                    <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">
                                                @endif
                                                @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                                    <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                @endfor
                            </ul>
                        @endif
                    </section>
                @endif
            </div>
        </div>
    </div>
@stop

@section('mobile_filter')
<div class="modal fade" id="modalShopFilters" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filters</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <section class="widget widget-categories">
                    <h3 class="widget-title">CATEGORY</h3>
                    <ul>
                        @foreach($parentCategories as $category)
                            <li><a href="{{ route('new_arrival_page', ['C' => $category->id]) }}">{{ $category->name }}</a></li>
                        @endforeach
                    </ul>
                </section>

                <section class="widget widget-categories">
                    <h3 class="widget-title">DATES</h3>
                    <ul>
                        @foreach($byArrivalDate as $item)
                            <li><a href="{{ route('new_arrival_page', ['D' => $item['day']]) }}">{{ $item['name'] }}</a><span>({{ $item['count'] }})</span></li>
                        @endforeach
                    </ul>
                </section>

                <section class="widget widget-categories">
                    <h3 class="widget-title">SEARCH</h3>

                    <form action="{{ route('new_arrival_page') }}" method="GET">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="mobile-search-style-no" name="search-by" value="1" checked>
                            <label class="custom-control-label" for="mobile-search-style-no">Style No.</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="mobile-search-description" name="search-by" value="2">
                            <label class="custom-control-label" for="mobile-search-description">Description</label>
                        </div>

                        <div class="form-group">
                            <input class="form-control" id="search-input" type="text" placeholder="Search" name="search">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control" id="search-price-min" type="text" placeholder="Price Min" name="price_min">
                            </div>

                            <div class="col-md-6">
                                <input class="form-control" id="search-price-max" type="text" placeholder="Price Max" name="price_max">
                            </div>
                        </div>

                        <button class="btn btn-primary" id="btn-search">SEARCH</button>
                    </form>
                </section>

                <section class="widget">
                    <h3 class="widget-title">COLORS</h3>

                    <ul class="sidecolor">
                        @foreach($masterColors as $mc)
                            <li class="item-color" title="{{ $mc->name }}">

                                <a href="{{ route('new_arrival_page', ['color' => $mc->id]) }}">
                                    <img src="{{ cdn($mc->image_path) }}" width="30px" height="20px">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </section>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ cdn('js/slider.js') }}"></script>
@stop