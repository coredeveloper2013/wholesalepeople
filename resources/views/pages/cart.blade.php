@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ cdn('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 text-right">
            <button class="btn btn-primary" id="btnCheckOutAll">Check Out All Vendor</button>
        </div>
    </div>

    <div class="row">
    @foreach($cartItems as $vendor_index => $vendor)
        <?php $subTotal = 0; ?>
        <div class="vendorwrap margin-bottom-2x">
            <h3>{{ $vendor[0][0]->item->vendor->company_name }}</h3>
            <div class="table-responsive cart-table">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="product-thumbnail"><b>Image</b></th>
                        <th><b>Style No.</b></th>
                        <th class="text-center"><b>Color</b></th>
                        <th class="text-center" colspan="10"><b>Size</b></th>
                        <th class="text-center"><b>Pack</b></th>
                        <th class="text-center"><b>Total Qty</b></th>
                        <th class="text-center"><b>Unit Price</b></th>
                        <th class="text-center"><b>Amount</b></th>
                        <th class="text-center"></th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($vendor as $item_index => $items)
                            <tr>
                                <td rowspan="{{ sizeof($items)+1  }}">
                                    @if (sizeof($items[0]->item->images) > 0)
                                        <img src="{{ cdn($items[0]->item->images[0]->list_image_path) }}" alt="Product" width="80px">
                                    @else
                                        <img src="{{ cdn('images/no-image.png') }}" alt="Product" width="80px">
                                    @endif
                                </td>
                                <td rowspan="{{ sizeof($items)+1 }}">
                                    <a href="{{ route('item_details_page', ['item' => $items[0]->item->id]) }}">{{ $items[0]->item->style_no }}</a>
                                </td>

                                <td class="text-center text-lg text-medium">
                                    &nbsp;
                                </td>

                                <?php
                                $sizes = explode("-", $items[0]->item->pack->name);
                                $itemInPack = 0;

                                for($i=1; $i <= sizeof($sizes); $i++) {
                                    $var = 'pack'.$i;

                                    if ($items[0]->item->pack->$var != null)
                                        $itemInPack += (int) $items[0]->item->pack->$var;
                                }
                                ?>

                                @foreach($sizes as $size)
                                    <th colspan="{{ $loop->last ? 10-sizeof($sizes) +1 : '' }}"><b>{{ $size }}</b></th>
                                @endforeach

                                <td>
                                    &nbsp;
                                </td>

                                <td>
                                    &nbsp;
                                </td>

                                <td>
                                    &nbsp;
                                </td>

                                <td>
                                    &nbsp;
                                </td>

                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            @foreach($items as $item)
                                <tr>
                                    <td>
                                        {{ $item->color->name }}
                                    </td>

                                    @for($i=1; $i <= sizeof($sizes); $i++)
                                        <?php $p = 'pack'.$i; ?>
                                        <td colspan="{{ $i == sizeof($sizes) ? 10-$i +1 : '' }}">{{ ($items[0]->item->pack->$p == null) ? '0' : $items[0]->item->pack->$p }}</td>
                                    @endfor

                                    <td class="text-center">
                                        <input class="cart-pack-input input_qty input_qty_{{ $item->item->vendor_meta_id }}"
                                               value="{{ $item->quantity }}"
                                               data-per-pack="{{ $itemInPack }}"
                                               data-price="{{ $item->item->price }}"
                                               data-id="{{ $item->id }}">
                                    </td>

                                    <td>
                                        <span class="total_qty">{{ $itemInPack * $item->quantity }}</span>
                                    </td>

                                    <td>
                                        ${{ sprintf('%0.2f', $item->item->price) }}
                                    </td>

                                    <td>
                                        <span class="total_amount">${{ sprintf('%0.2f', $item->item->price * $itemInPack * $item->quantity) }}</span>
                                        <?php $subTotal += $item->item->price * $itemInPack * $item->quantity; ?>
                                    </td>

                                    <td>
                                        <a class="remove-from-cart btnDelete" href="#"
                                           data-toggle="tooltip"
                                           title="Remove item" data-id="{{ $item->id }}"><i class="icon-cross"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="shopping-cart-footer">
                <div class="column">
                    @if ($vendor[0][0]->item->vendor->buyerStoreCredit() > 0)
                        <b>Store Credit: </b>${{ number_format($vendor[0][0]->item->vendor->buyerStoreCredit(), 2, '.', '') }}
                    @endif
                </div>
                <div class="column text-lg">
                    <b>Subtotal: </b><span class="text-medium sub_total" data-vendor-id="{{ $vendor[0][0]->item->vendor_meta_id }}"></span><br>

                    @if ($vendor[0][0]->item->vendor->buyerStoreCredit() > 0)
                        <b>Store Credit: </b><input type="text" class="store_credit store_credit_{{ $vendor[0][0]->item->vendor_meta_id }}" placeholder="$" data-vendor-id="{{ $vendor[0][0]->item->vendor_meta_id }}" style="width: 75px"><br>
                        <b>Total: </b><span class="text-medium total total_{{ $vendor[0][0]->item->vendor_meta_id }}" data-vendor-id="{{ $vendor[0][0]->item->vendor_meta_id }}"></span><br>
                    @endif
                </div>
            </div>

            <div class="shopping-cart-footer">
                <div class="column">
                    <a class="btn btn-primary btnUpdate" href="#" data-vendor-id="{{ $vendor[0][0]->item->vendor_meta_id }}">Update Cart</a>

                    @if ($subTotal >= $vendor[0][0]->item->vendor->min_order)
                        <a class="btn btn-success btnCheckout" data-vendor-id="{{ $vendor[0][0]->item->vendor_meta_id }}">Checkout</a>
                    @else
                        <b>Min. Order: </b>${{ number_format($vendor[0][0]->item->vendor->min_order, 2, '.', '') }}
                    @endif
                </div>
            </div>
        </div>
    @endforeach
    </div>
</div>
@stop


@section('additionalJS')
    <script type="text/javascript" src="{{ cdn('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('.input_qty').keyup(function () {
                var index = $('.input_qty').index($(this));
                var perPack = parseInt($(this).data('per-pack'));
                var price = $(this).data('price');
                var i = 0;
                var val = $(this).val();

                if (isInt(val)) {
                    i = parseInt(val);

                    if (i < 0)
                        i = 0;
                }


                $('.total_qty:eq('+index+')').html(perPack * i);
                $('.total_amount:eq('+index+')').html('$' + (perPack * i * price).toFixed(2));

                calculate();
            });

            $('.btnUpdate').click(function () {
                var vendorId = $(this).data('vendor-id');
                var ids = [];
                var qty = [];

                var valid = true;
                $('.input_qty_'+vendorId).each(function () {
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            return valid = false;
                    } else {
                        return valid = false;
                    }

                    ids.push($(this).data('id'));
                    qty.push(i);
                });

                if (!valid) {
                    alert('Invalid Quantity.');
                    return;
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('update_cart') }}",
                    data: { ids: ids, qty: qty }
                }).done(function( data ) {
                    window.location.replace("{{ route('update_cart_success') }}");
                });
            });

            $('.btnDelete').click(function () {
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('delete_cart') }}",
                    data: { id: id }
                }).done(function( data ) {
                    location.reload();
                });
            });
            
            $('.btnCheckout').click(function () {
                var vendorId = [$(this).data('vendor-id')];
                var storeCredit = [$('.store_credit_'+vendorId[0]).val()];

                $.ajax({
                    method: "POST",
                    url: "{{ route('create_checkout') }}",
                    data: { ids: vendorId, storeCredit: storeCredit }
                }).done(function( data ) {
                    if (data.success) {
                        window.location.replace("{{ route('show_checkout') }}" + "?id=" + data.message);
                    } else {
                        alert(data.message);
                    }
                });
            });

            $('#btnCheckOutAll').click(function () {
                var vendorIds = [];
                var storeCredit = [];

                $('.btnCheckout').each(function () {
                    vendorIds.push($(this).data('vendor-id'));
                });

                $.each(vendorIds, function (i, id) {
                    storeCredit.push($('.store_credit_'+id).val());
                });


                if (vendorIds.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('create_checkout') }}",
                        data: {ids: vendorIds, storeCredit: storeCredit}
                    }).done(function (data) {
                        if (data.success) {
                            window.location.replace("{{ route('show_checkout') }}" + "?id=" + data.message);
                        } else {
                            alert(data.message);
                        }
                    });
                }
            });
            
            function calculate() {
                $('.sub_total').each(function () {
                    var vendorId = $(this).data('vendor-id');
                    var sub_total = 0;
                    var index = $('.sub_total').index($(this));

                    $('.input_qty_'+vendorId).each(function () {
                        var perPack = parseInt($(this).data('per-pack'));
                        var price = $(this).data('price');
                        var i = 0;
                        var val = $(this).val();

                        if (isInt(val)) {
                            i = parseInt(val);

                            if (i < 0)
                                i = 0;
                        }

                        sub_total += perPack * i * price;
                    });

                    $(this).html('$' + sub_total.toFixed(2));

                    var store_credit = parseFloat($('.store_credit_'+vendorId).val());

                    if(isNaN(store_credit))
                        store_credit = 0;


                    var total = sub_total-store_credit;

                    if (total < 0)
                        total = 0;

                    $('.total_'+vendorId).html('$' + total.toFixed(2));
                });
            }

            calculate();

            $('.store_credit').keyup(function () {
                calculate();
            });

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }
        });
    </script>
@stop