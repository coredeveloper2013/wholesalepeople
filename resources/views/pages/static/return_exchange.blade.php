@extends('layouts.app')

@section('content')
    <div class="container margin-top-3x margin-bottom-3x">
    	<div class="return-exchange">
	        <h2>Return & Exchange</h2>
	        <hr>
	        <p class="padding-top-1x">
	            If you are entitled to Exchanges, Refund, or Cancel your order, you must contact vendor first for authorization before contact us. The vendor will issue a refund or credit according to the terms of their policy, after confirming the return or cancellation.
	        </p>
	    </div>
        <div class="payment-term">
        	<h3>Payment Terms</h3>
        	<hr>
        	<p class="padding-top-1x">Payment terms and methods may vary according to vendor. Please check each vendor's payment policy before placing an order.</p>
        </div>
        <div class="order-confirmation">
        	<h3>Order Confirmation</h3>
        	<hr>
        	<p class="padding-top-1x">
        		After clicking "Check Out", you will see an "Order Confirmation" page that shows your billing address, shipping address, payment information, and items ordered. Please print this page and keep the confirmation number for your records. If you have questions concerning your order, please contact Customer Service and refer to your confirmation number.
        	</p>
        	<p>
        		Your vendor will also send you a confirmation email within 24 hours of placing your order. This email will confirm that your order was processed as well as other detailed information. The vendor will also refer to your order confirmation number.
        	</p>
        </div>
        <div class="order-confirmation">
        	<h3>Exchanges and Refunds</h3>
        	<hr>
        	<p class="padding-top-1x">
        		Buyers seeking exchanges or refunds should refer to each vendor's return policy. If you need a refund from an order cancellation or return, you must first contact the vendor for authorization before contacting us. The vendor will issue a refund or credit according to the terms of their policy, after confirming the return or cancellation.
        	</p>
        	<p>Please Note: Exchanges/Returns/Refunds are Not Allowed in the following cases:</p>
        	<ul>
    			<li>Loss or damage of merchandise while in the customer's possession. (Does not apply to removing the packing seals to check contents)</li>
    			<li>Merchandise appearing to be worn, used, or in otherwise non-salable condition.</li>
    			<li>Time since the sale is beyond the vendor's return period. Please refer to vendor's return policy.</li>
    			<li>Breaking of original vendor packaging and re-wrapping.</li>
    		</ul>
        </div>
        <div class="contact-stylepick">
        	<h3>Quesiton regarding Return and exchange</h3>
        	<hr>
        	<p class="padding-top-1x">If you have any questions, Please email us at <a href="mailto:info@stylepick.net?Subject=Return%20and%20exchange%20question" target="_top">info@stylepick.net</a></p>
        </div>
    </div>
@stop