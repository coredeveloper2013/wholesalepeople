@extends('layouts.app')

@section('content')
    <div class="container margin-top-2x margin-bottom-2x">
    	<div class="row">
            <h2>Show Schedule</h2>
		    <section>
		        <div class="card-group">
		            <div class="card margin-bottom-1x"><img class="card-img-top" src="/images/show/UBMFASHION_MAGIC.jpg" alt="UBMFASHION_MAGIC">
		                <div class="card-body">
		                    <h4 class="card-title">MAGIC</h4>
		                    <p class="card-text">MAGIC. Here you will find the latest in apparel, footwear, accessories, and manufacturing. From the height of advanced contemporary luxury brands, to the latest trends in fast fashion, MAGIC fuels the business of fashion.</p>
		                    <p>AUGUST 13-15, 2018</p>
		                </div>
		            </div>
		            <div class="card margin-bottom-1x"><img class="card-img-top" src="/images/show/fame.jpg" alt="Fame">
		                <div class="card-body">
		                    <h4 class="card-title">FAME</h4>
		                    <p class="card-text">Fun, fresh and full of cool trends, FAME is a one-stop shopping destination where the retailers discover ready-to-wear young contemporary and trend-driven fashion for women.</p>
		                    <p>SEPTEMBER 15-17, 2018</p>
		                </div>
		            </div>
		            <div class="card margin-bottom-1x"><img class="card-img-top" src="/images/show/americasmart.jpg" alt="AmerciasMart">
		                <div class="card-body">
		                    <h4 class="card-title">AmericasMart</h4>
		                    <p class="card-text">shop Atlanta Apparel for access to top lines with collections designed to meet the needs of any retail buyer. You will find the latest looks in contemporary, young contemporary, ready-to-wear, fashion accessories and more</p>
		                    <p>AUGUST 07-11, 2018</p>
		                </div>
		            </div>
		            <div class="card margin-bottom-1x"><img class="card-img-top" src="/images/show/dallasapparel.jpg" alt="Dallas Apparel">
		                <div class="card-body">
		                    <h4 class="card-title">Dallas Apparel</h4>
		                    <p class="card-text">Dallas continues to be the premier resource for retailers to find the latest in young contemporary, contemporary, western, children’s, and more.</p>
		                    <p>AUGUST 22-25, 2018</p>
		                </div>
		            </div>
		        </div>
		    </section>
        </div>
    </div>
@stop