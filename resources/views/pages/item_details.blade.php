<?php use App\Enumeration\Availability; ?>

@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ cdn('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ cdn('css/fotorama.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-2 col-lg-3 order-lg-1">
            <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
            <aside class="sidebar-offcanvas">
                @if ($smallBannerPath != '')
                    <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                        <img src="{{ $smallBannerPath }}">
                    </a>
                @endif

                <!-- Widget Categories-->
                <section class="vendorinfo">
                    <ul>
                        <li><b>COMPANY: </b> {{ $vendor->company_name }}</li>
                        <li><b>ADDRESS: </b> {{ $vendor->billing_address }}</li>
                        <li><b>CITY:</b> {{ $vendor->billing_city }}</li>
                        <li><b>ZipCode:</b> {{ $vendor->billing_zip }}</li>
                        <li><b>Phone:</b> {{ $vendor->billing_phone }}</li>
                    </ul>
                </section>
                <section class="widget widget-categories">
                    <h3 class="widget-title">VENDOR CATEGORY</h3>
                    <ul>
                        <li><a href="{{ route('vendor_category_all_page', ['vendor' => $vendor->id]) }}">All Category ({{ $totalItem }})</a></li>
                        @foreach($vendor->parentCategories as $cat)
                            @if (sizeof($cat->subCategories) > 0)
                                <li class="has-children expanded"><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></span>
                                    <ul>
                                        @foreach($cat->subCategories as $sub)
                                            <li>
                                                <a href="{{ route('vendor_category_page', ['category' => $sub->id]) }}">{{ $sub->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </section>

                @if (sizeof($recentlyViewItems) > 0)
                    <section>
                        <h3 class="widget-title">RECENT VIEW</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="isotope-grid cols-2 mb-1">
                                    <div class="gutter-sizer"></div>
                                    <div class="grid-sizer"></div>

                                    @foreach($recentlyViewItems as $recentlyViewItem)
                                        <div class="grid-item">
                                            <div class="product-card">
                                                <a class="product-thumb" href="{{ route('item_details_page', ['item' => $recentlyViewItem->item->id]) }}">
                                                    @if (sizeof($recentlyViewItem->item->images) > 0)
                                                        <img src="{{ cdn($recentlyViewItem->item->images[0]->list_image_path) }}" alt="{{ $recentlyViewItem->item->style_no }}">
                                                    @else
                                                        <img src="{{ cdn('images/no-image.png') }}" alt="{{ $recentlyViewItem->item->style_no }}">
                                                    @endif
                                                </a>
                                                <div class="product-title"><b><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($recentlyViewItem->item->vendor->company_name)]) }}">{{ $recentlyViewItem->item->vendor->company_name }}</a></b></div>
                                                <h3 class="product-title">
                                                    <a href="{{ route('item_details_page', ['item' => $recentlyViewItem->item->id]) }}">{{ $recentlyViewItem->item->style_no }}</a>
                                                </h3>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
            </aside>
        </div>
        <div class="col-xl-10 col-lg-9 order-lg-2">
            {{ Breadcrumbs::render('item_details', $item) }}
            <div class="row">
                <div class="col-md-6">
                    <div class="fotorama" data-nav="thumbs" data-thumbwidth="80" data-thumbheight="120" data-width="100%">
                        @foreach($item->images as $img)
                            <a href="{{ cdn($img->image_path) }}"><img src="{{ cdn($img->list_image_path) }}"></a>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="nextpre text-right">
                            @if ($previousItem)
                                <a class="btn btn-secondary" href="{{ route('item_details_page', ['item' => $previousItem->id]) }}">< Previous</a>
                            @endif

                            @if ($nextItem)
                                <a class="btn btn-secondary" href="{{ route('item_details_page', ['item' => $nextItem->id]) }}">Next ></a>
                            @endif
                        </div>
                    </div>

                    <div class="pt-1 mb-2"><span class="text-medium">Stock:</span>
                        @if ($item->availability == Availability::$ARRIVES_SOON)
                            Arrives soon / Back Order
                        @elseif ($item->availability == Availability::$IN_STOCK)
                            In Stock
                        @else
                            Unspecified
                        @endif
                    </div>
                    <div class="product-title"><h1>{{ $item->name }}</h1></div>
                    <div class="product-sku"><h2 class="text-normal"><span class="text-medium">Style#:</span> {{ $item->style_no }}</h2></div>

                    @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                        <div class="product-sku"><h2 class="text-normal"><span class="text-medium">Available On:</span> {{ date('F j, Y', strtotime($item->available_on)) }}</h2></div>
                    @endif

                    <span class="h2 d-block">
                        @if ($item->orig_price != null)
                            <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                        @endif
                        ${{ sprintf('%0.2f', $item->price) }}
                    </span>

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="bgblack">
                            <tr>
                                <th>Color</th>
                                @foreach($sizes as $size)
                                    <th>{{ $size }}</th>
                                @endforeach
                                <th width="15%">Pack</th>
                                <th class="hidden-sm-down" width="20%">Qty</th>
                                <th width="20%">Amount</th>
                            </tr>
                            </thead>

                            <tbody class="text-sm">
                            @foreach($item->colors as $color)
                                <tr>
                                    <td>{{ $color->name }}</td>
                                    @for ($i = 1; $i <= sizeof($sizes); $i++)
                                        <td>
                                            <?php $p = 'pack'.$i; ?>
                                            {{ ($item->pack->$p != null) ? $item->pack->$p : 0 }}
                                        </td>
                                    @endfor
                                    <td>
                                        <input type="text" class="form-control pack" data-color="{{ $color->id }}" name="input-pack[]">
                                    </td>
                                    <td class="hidden-sm-down"><span class="qty">0</span></td>
                                    <td>
                                        <span class="price">$0.00</span>
                                        <input type="hidden" class="input-price" value="0">
                                    </td>
                                </tr>
                            @endforeach

                            <tr>
                                <td colspan="{{ sizeof($sizes) + 2 }}"><b>Total</b></td>
                                <td class="hidden-sm-down"><b><span id="totalQty">0</span></b></td>
                                <td><b><span id="totalPrice">$0.00</span></b></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                    <div class="d-flex flex-wrap justify-content-between">
                        <div class="sp-buttons mt-2 mb-2">
                            <button class="btn btn-primary" id="btnAddToCart"><i class="icon-bag"></i> Add to Shopping Bag</button>

                            @if (!$isInWishlist)
                                <button class="btn btn-secondary" id="btnAddToWishlist" data-id="{{ $item->id }}"><i class="icon-heart"></i> Wishlish</button>
                            @endif

                            @if ($cartCount > 0)
                                <a href="{{ route('show_cart') }}" class="btn btn-secondary"> Checkout</a>
                            @endif
                        </div>
                    </div>

                    <br>
                    <h4>Description</h4>
                    <p class="text-xs">{!! nl2br($item->description) !!}</p>
                    <h4>Order Notice</h4>
                    <p class="text-xs">{!! $item->vendor->order_notice !!}</p>
                </div>
                <div class="col-md-12 margin-top-2x">
                    <h3>You May Also Like</h3>
                    <div class="row padding-bottom-2x">
                        <div class="content owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 10, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:6}} }">
                            @foreach($suggestItems as $product)
                            <div class="grid-item">
                                <div class="product-card">
                                    <a class="product-thumb" href="{{ route('item_details_page', ['item' => $product->id]) }}">
                                        @if (sizeof($product->images) > 0)
                                            <img src="{{ cdn($product->images[0]->list_image_path) }}" alt="{{ $product->style_no }}">
                                        @else
                                            <img src="{{ cdn('images/no-image.png') }}" alt="{{ $product->style_no }}">
                                        @endif
                                    </a>
                                    <div class="product-title"><b><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($product->vendor->company_name)]) }}">{{ $product->vendor->company_name }}</a></b></div>
                                    <h3 class="product-title"><a href="{{ route('item_details_page', ['item' => $product->id]) }}">{{ $product->style_no }}</a> /
                                        @if ($product->orig_price != null)
                                            <del>${{ number_format($product->orig_price, 2, '.', '') }}</del>
                                        @endif

                                        ${{ sprintf('%0.2f', $product->price) }}
                                    </h3>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


@section('mobile_filter')
<div class="modal fade" id="modalShopFilters" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filters</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                @if ($smallBannerPath != '')
                    <a href="{{ route('vendor_or_parent_category', ['text' => $vendor->company_name]) }}">
                        <img src="{{ $smallBannerPath }}">
                    </a>
                 @endif

                <!-- Widget Categories-->
                <section class="vendorinfo">
                    <ul>
                        <li><b>COMPANY: </b> {{ $vendor->company_name }}</li>
                        <li><b>ADDRESS: </b> {{ $vendor->billing_address }}</li>
                        <li><b>CITY:</b> {{ $vendor->billing_city }}</li>
                        <li><b>ZipCode:</b> {{ $vendor->billing_zip }}</li>
                        <li><b>Phone:</b> {{ $vendor->billing_phone }}</li>
                    </ul>
                </section>
                <section class="widget widget-categories">
                    <h3 class="widget-title">VENDOR CATEGORY</h3>
                    <ul>
                        @foreach($vendor->parentCategories as $cat)
                            <li><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></li>
                        @endforeach
                    </ul>
                </section>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ cdn('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('js/fotorama.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            var itemInPack = {{ $itemInPack }};
            var perPrice = parseFloat('{{ $item->price }}');
            var totalQty = 0;
            var itemId = '{{ $item->id }}';

            $('.pack').keyup(function () {
                var i = 0;
                var val = $(this).val();

                if (isInt(val)) {
                    i = parseInt(val);

                    if (i < 0)
                        i = 0;
                }

                $(this).closest('tr').find('.qty').html(itemInPack * i);
                $(this).closest('tr').find('.price').html('$' + (itemInPack * i * perPrice).toFixed(2));
                $(this).closest('tr').find('.input-price').val(itemInPack * i * perPrice);

                calculate();
            });

            $('#btnAddToCart').click(function () {
                var colors = [];
                var qty = [];
                var vendor_id = '{{ $item->vendor_meta_id }}';

                if (totalQty == 0) {
                    alert('Please select an item.');
                    return;
                }

                var valid = true;
                $('.pack').each(function () {
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            return valid = false;
                    } else {
                        if (val != '')
                            return valid = false;
                    }

                    if (i != 0) {
                        colors.push($(this).data('color'));
                        qty.push(i);
                    }
                });

                if (!valid) {
                    alert('Invalid Quantity.');
                    return;
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_cart') }}",
                    data: { itemId: itemId, colors: colors, qty: qty, vendor_id: vendor_id }
                }).done(function( data ) {
                    console.log(data);
                    toastr.success('Added to cart.');
                    window.location.replace("{{ route('add_to_cart_success') }}");
                });
            });

            function calculate() {
                totalQty = 0;
                var totalPrice = 0;

                $('.qty').each(function () {
                    totalQty += parseInt($(this).html());
                });

                $('.input-price').each(function () {
                    totalPrice += parseFloat($(this).val());
                });

                $('#totalQty').html(totalQty);
                $('#totalPrice').html('$' + totalPrice.toFixed(2));
            }

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }

            // Wishlist
            $('#btnAddToWishlist').click(function () {
                var id = $(this).data('id');
                $this = $(this);

                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_wishlist') }}",
                    data: { id: id }
                }).done(function( data ) {
                    toastr.success('Added to Wishlist.');
                    $this.remove();
                });
            });

            // On Back
            if (window.history && window.history.pushState) {

                $(window).on('popstate', function() {
                    localStorage['change_page'] = 1;
                    localStorage['change_pos'] = 1;
                    history.back();
                });

                window.history.pushState('forward', null, '');
            }
        });
    </script>
@stop