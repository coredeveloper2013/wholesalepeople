@extends('layouts.app');

@section('content')
<div class="container-fluid padding-bottom-10x padding-top-4x text-center">
	<div class="title">
		<h1>Thank you for Registering!</h1>
		<p>Stylepick will review your documents and you will receive an activation email in 24hrs. Thank you for your patience</p>
	</div>
</div>
@stop