<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Wholesalepeople') }}</title>
    <link rel="icon" href="/images/stylepick_blue.ico" type="image/x-icon">
    <link rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/vendor.min.css') }}">
    <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/styles.css') }}">
</head>

<!-- Body-->
<body>
<!-- Off-Canvas Wrapper-->
<div class="vendor-login offcanvas-wrapper">
    <!-- Page Content-->
    <div class="padding-bottom-3x mb-1 padding-top-1x">
        <div class="vlogin">
            <span>VENDOR ADMIN</span>

            <form class="login-box" method="post" action="{{ route('login_vendor_post') }}">
                @csrf

                <div class="form-group input-group">
                    <input class="form-control" type="text" placeholder="Username" name="username" value="{{ old('username') }}" required>
                </div>
                <div class="form-group input-group">
                    <input class="form-control" type="password" placeholder="Password" name="password" required>
                </div>
                <div class="d-flex flex-wrap justify-content-between">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="remember_me" name="remember_me" checked>
                        <label class="custom-control-label" for="remember_me">Remember me</label>
                    </div>
                </div>

                <div class="has-danger">
                    <div class="form-control-feedback">{{ session('message') }}</div>
                </div>

                <div class="loginbtn">
                    <button class="btn btn-primary margin-bottom-none" type="submit">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>