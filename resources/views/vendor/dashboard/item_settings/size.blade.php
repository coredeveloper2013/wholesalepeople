@extends('vendor.layouts.dashboard')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row {{ ($errors && sizeof($errors) > 0) ? 'd-none' : '' }}" id="addBtnRow">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNewSize">Add New Size</button>
        </div>
    </div>

    <div class="row {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="addEditRow">
        <div class="col-md-12" style="border: 1px solid black">
            <h3><span id="addEditTitle">{{ old('inputAdd') == '0' ? 'Edit Size' : 'Add Size' }}</span></h3>

            <form class="form-horizontal" id="form" method="post" action="{{ (old('inputAdd') == '1') ? route('vendor_size_add_post') : route('vendor_size_edit_post') }}">
                @csrf

                <input type="hidden" name="inputAdd" id="inputAdd" value="{{ old('inputAdd') }}">
                <input type="hidden" name="sizeId" id="sizeId" value="{{ old('sizeId') }}">

                <div class="form-group row">
                    <div class="col-lg-1">
                        <label for="status" class="col-form-label">Status</label>
                    </div>

                    <div class="col-lg-5">
                        <label for="statusActive" class="custom-control custom-radio">
                            <input id="statusActive" name="status" type="radio" class="custom-control-input"
                                   value="1" {{ (old('status') == '1' || empty(old('status'))) ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Active</span>
                        </label>
                        <label for="statusInactive" class="custom-control custom-radio signin_radio4">
                            <input id="statusInactive" name="status" type="radio" class="custom-control-input" value="0" {{ old('status') == '0' ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Inactive</span>
                        </label>
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('size_name') ? ' has-danger' : '' }}">
                    <div class="col-lg-1">
                        <label for="size_name" class="col-form-label">Size Name *</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="size_name" class="form-control{{ $errors->has('size_name') ? ' is-invalid' : '' }}"
                               placeholder="Size Name" name="size_name" value="{{ old('size_name') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-1">
                        <label class="col-form-label">Size *</label>
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s1" name="s1" class="form-control{{ $errors->has('s1') ? ' is-invalid' : '' }}"
                               placeholder="S1" value="{{ old('s1') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s2" name="s2" class="form-control{{ $errors->has('s2') ? ' is-invalid' : '' }}"
                               placeholder="S2" value="{{ old('s2') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s3" name="s3" class="form-control{{ $errors->has('s3') ? ' is-invalid' : '' }}"
                               placeholder="S3" value="{{ old('s3') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s4" name="s4" class="form-control{{ $errors->has('s4') ? ' is-invalid' : '' }}"
                               placeholder="S4" value="{{ old('s4') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s5" name="s5" class="form-control{{ $errors->has('s5') ? ' is-invalid' : '' }}"
                               placeholder="S5" value="{{ old('s5') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s6" name="s6" class="form-control{{ $errors->has('s6') ? ' is-invalid' : '' }}"
                               placeholder="S6" value="{{ old('s6') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s7" name="s7" class="form-control{{ $errors->has('s7') ? ' is-invalid' : '' }}"
                               placeholder="S7" value="{{ old('s7') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s8" name="s8" class="form-control{{ $errors->has('s8') ? ' is-invalid' : '' }}"
                               placeholder="S8" value="{{ old('s8') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s9" name="s9" class="form-control{{ $errors->has('s9') ? ' is-invalid' : '' }}"
                               placeholder="S9" value="{{ old('s9') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s10" name="s10" class="form-control{{ $errors->has('s10') ? ' is-invalid' : '' }}"
                               placeholder="S10" value="{{ old('s10') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-1">
                        <label for="default" class="col-form-label">Default</label>
                    </div>

                    <div class="col-lg-5">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="default" value="1" name="default" {{ old('default') ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-default" id="btnCancel">Cancel</button>
                        <input type="submit" id="btnSubmit" class="btn btn-primary" value="{{ old('inputAdd') == '0' ? 'Update' : 'Add' }}">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Size Name</th>
                        <th>Size Details</th>
                        <th>Created On</th>
                        <th>Active</th>
                        <th>Default</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($sizes as $size)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $size->name }}</td>
                            <td>
                                {{ $size->size1 }}{{ ($size->size2 != '') ? '-'.$size->size2 : '' }}{{ ($size->size3 != '') ? '-'.$size->size3 : '' }}{{ ($size->size4 != '') ? '-'.$size->size4 : '' }}{{ ($size->size5 != '') ? '-'.$size->size5 : '' }}{{ ($size->size6 != '') ? '-'.$size->size6 : '' }}{{ ($size->size7 != '') ? '-'.$size->size7 : '' }}{{ ($size->size8 != '') ? '-'.$size->size8 : '' }}{{ ($size->size9 != '') ? '-'.$size->size9 : '' }}{{ ($size->size10 != '') ? '-'.$size->size10 : '' }}
                            </td>
                            <td>{{ date('d/m/Y', strtotime($size->created_at)) }}</td>
                            <td>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" data-id="{{ $size->id }}" class="custom-control-input status" value="1" {{ $size->status == 1 ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>
                                <label class="custom-control custom-radio">
                                    <input type="radio" name="defaultTable" class="custom-control-input default" data-id="{{ $size->id }}"
                                           value="1" {{ $size->default == 1 ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>
                                <a class="btnEdit" data-id="{{ $size->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                                <a class="btnDelete" data-id="{{ $size->id }}" role="button" style="color: red">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
<script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var sizes = <?php echo json_encode($sizes->toArray()); ?>;
        var selectedId;
        var message = '{{ session('message') }}';

        if (message != '')
            toastr.success(message);


        $('#btnAddNewSize').click(function () {
            $('#addEditRow').removeClass('d-none');
            $('#addBtnRow').addClass('d-none');
            $('#addEditTitle').html('Add Size');
            $('#btnSubmit').val('Add');
            $('#inputAdd').val('1');
            $('#form').attr('action', '{{ route('vendor_size_add_post') }}');
        });

        $('#btnCancel').click(function (e) {
            e.preventDefault();

            $('#addEditRow').addClass('d-none');
            $('#addBtnRow').removeClass('d-none');

            // Clear form
            $('#statusActive').prop('checked', true);
            $('#default').prop('checked', false);
            $('#size_name').val('');
            $('#s1').val('');
            $('#s2').val('');
            $('#s3').val('');
            $('#s4').val('');
            $('#s5').val('');
            $('#s6').val('');
            $('#s7').val('');
            $('#s8').val('');
            $('#s9').val('');
            $('#s10').val('');

            $('input').removeClass('is-invalid');
            $('.form-group').removeClass('has-danger');
        });

        $('.btnEdit').click(function () {
            var id = $(this).data('id');
            var index = $(this).data('index');

            $('#addEditRow').removeClass('d-none');
            $('#addBtnRow').addClass('d-none');
            $('#addEditTitle').html('Edit Size');
            $('#btnSubmit').val('Update');
            $('#inputAdd').val('0');
            $('#form').attr('action', '{{ route('vendor_size_edit_post') }}');
            $('#sizeId').val(id);

            var size = sizes[index];

            if (size.status == 1)
                $('#statusActive').prop('checked', true);
            else
                $('#statusInactive').prop('checked', true);

            if (size.default == 1)
                $('#default').prop('checked', true);
            else
                $('#default').prop('checked', false);

            $('#size_name').val(size.name);
            $('#s1').val(size.size1);
            $('#s2').val(size.size2);
            $('#s3').val(size.size3);
            $('#s4').val(size.size4);
            $('#s5').val(size.size5);
            $('#s6').val(size.size6);
            $('#s7').val(size.size7);
            $('#s8').val(size.size8);
            $('#s9').val(size.size9);
            $('#s10').val(size.size10);
        });

        $('.btnDelete').click(function () {
            $('#deleteModal').modal('show');
            selectedId = $(this).data('id');
        });
        
        $('#modalBtnDelete').click(function () {
            $.ajax({
                method: "POST",
                url: "{{ route('vendor_size_delete') }}",
                data: { id: selectedId }
            }).done(function( msg ) {
                location.reload();
            });
        });
        
        $('.status').change(function () {
            var status = 0;
            var id = $(this).data('id');

            if ($(this).is(':checked'))
                status = 1;

            $.ajax({
                method: "POST",
                url: "{{ route('vendor_size_change_status') }}",
                data: { id: id, status: status }
            }).done(function( msg ) {
                toastr.success('Status Updated!');
            });
        });

        $('.default').change(function () {
            var id = $(this).data('id');

            $.ajax({
                method: "POST",
                url: "{{ route('vendor_size_change_default') }}",
                data: { id: id }
            }).done(function( msg ) {
                toastr.success('Default Updated!');
            });
        });
    })
</script>
@stop