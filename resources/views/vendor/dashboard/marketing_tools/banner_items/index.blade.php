<?php use App\Enumeration\SliderType; ?>

@extends('vendor.layouts.dashboard')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <span class="h4">SP Banner Slider</span> - 
            <span>upto 7 products</span>
        </div>

        <div class="col-md-6 text-right">
            <button class="btn btn-primary" id="btnAddHomeSlider">Add Item</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="banner-item-container">
                <ul id="HomePageSliderItems">
                    @foreach($mainSliderItems as $i)
                        <li class="text-center" data-id="{{ $i->id }}">
                            @if (sizeof($i->item->images) > 0)
                                <img src="{{ asset($i->item->images[0]->image_path) }}" alt="{{ $i->item->style_no }}">
                            @else
                                <img src="{{ asset('images/no-image.png') }}" alt="{{ $i->item->style_no }}">
                            @endif
                            <a class="text-danger btnRemove" data-type="1" data-id="{{ $i->id }}">Remove</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    {{--Category Top Slider--}}
    <div class="row">
        <div class="col-md-6">
            <span class="h4">Category Top Slider</span> - 
            <span>upto 6 products</span>
        </div>

        <div class="col-md-6 text-right">
            <button class="btn btn-primary" id="btnAddCategoryTopSlider">Add Item</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="banner-item-container">
                <ul id="categoryTopSliderItems">
                    @foreach($categoryTopSliderItems as $i)
                        <li class="text-center" data-id="{{ $i->id }}">
                            @if (sizeof($i->item->images) > 0)
                                <img src="{{ asset($i->item->images[0]->image_path) }}" alt="{{ $i->item->style_no }}">
                            @else
                                <img src="{{ asset('images/no-image.png') }}" alt="{{ $i->item->style_no }}">
                            @endif
                            <a class="text-danger btnRemove" data-type="1" data-id="{{ $i->id }}">Remove</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    {{--Category Second Slider--}}

    <div class="row">
        <div class="col-md-6">
            <span class="h4">Category Bottom Slider</span> - 
            <span>upto 6 products</span>
        </div>

        <div class="col-md-6 text-right">
            <button class="btn btn-primary" id="btnAddCategorySecondSlider">Add Item</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="banner-item-container">
                <ul id="categorySecondSliderItems">
                    @foreach($categorySecondSliderItems as $i)
                        <li class="text-center" data-id="{{ $i->id }}">
                            @if (sizeof($i->item->images) > 0)
                                <img src="{{ asset($i->item->images[0]->image_path) }}" alt="{{ $i->item->style_no }}">
                            @else
                                <img src="{{ asset('images/no-image.png') }}" alt="{{ $i->item->style_no }}">
                            @endif
                            <a class="text-danger btnRemove" data-type="1" data-id="{{ $i->id }}">Remove</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    {{--New Arrivals Top Slider--}}
    <div class="row">
        <div class="col-md-6">
            <span class="h4">New Arrival Top Slider</span> - 
            <span>upto 6 products</span>
        </div>

        <div class="col-md-6 text-right">
            <button class="btn btn-primary" id="btnAddNewTopSlider">Add Item</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="banner-item-container">
                <ul id="newTopSliderItems">
                    @foreach($newTopSliderItems as $i)
                        <li class="text-center" data-id="{{ $i->id }}">
                            @if (sizeof($i->item->images) > 0)
                                <img src="{{ asset($i->item->images[0]->image_path) }}" alt="{{ $i->item->style_no }}">
                            @else
                                <img src="{{ asset('images/no-image.png') }}" alt="{{ $i->item->style_no }}">
                            @endif
                            <a class="text-danger btnRemove" data-type="1" data-id="{{ $i->id }}">Remove</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    {{--New Arrivals Second Slider--}}
    <div class="row">
        <div class="col-md-6">
            <span class="h4">New Arrival Bottom Slider</span> - 
            <span>upto 6 products</span>
        </div>

        <div class="col-md-6 text-right">
            <button class="btn btn-primary" id="btnAddNewSecondSlider">Add Item</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="banner-item-container">
                <ul id="newSecondSliderItems">
                    @foreach($newSecondSliderItems as $i)
                        <li class="text-center" data-id="{{ $i->id }}">
                            @if (sizeof($i->item->images) > 0)
                                <img src="{{ asset($i->item->images[0]->image_path) }}" alt="{{ $i->item->style_no }}">
                            @else
                                <img src="{{ asset('images/no-image.png') }}" alt="{{ $i->item->style_no }}">
                            @endif
                            <a class="text-danger btnRemove" data-type="1" data-id="{{ $i->id }}">Remove</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>



    <div class="modal fade" id="item-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelLarge">Select Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Search" id="modal-search">
                        </div>

                        <div class="col-md-4">
                            <select class="form-control" id="modal-category">
                                <option value="">All Category</option>

                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-4">
                            <button class="btn btn-primary" id="modal-btn-search">SEARCH</button>
                        </div>
                    </div>

                    <br>
                    <hr>

                    <ul class="modal-items">
                        @foreach($items as $item)
                            <li class="modal-item" data-id="{{ $item->id }}">
                                @if (sizeof($item->images) > 0)
                                    <img src="{{ asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                @else
                                    <img src="{{ asset('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                @endif

                                <div class="style_no">
                                    {{ $item->style_no }}
                                </div>
                            </li>
                        @endforeach
                    </ul>

                    <div id="modal-pagination">
                        {{ $items->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>

    <template id="template-modal-item">
        <li class="modal-item">
            <img src="">

            <div class="style-no">

            </div>
        </li>
    </template>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sortable/js/Sortable.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = '';
            var id = '';

            $('#btnAddHomeSlider').click(function () {
                type = '{{ SliderType::$MAIN_SLIDER }}';
                $('#item-modal').modal('show');
            });

            $('#btnAddCategoryTopSlider').click(function () {
                type = '{{ SliderType::$CATEGORY_TOP_SLIDER }}';
                $('#item-modal').modal('show');
            });

            $('#btnAddCategorySecondSlider').click(function () {
                type = '{{ SliderType::$CATEGORY_SECOND_SLIDER }}';
                $('#item-modal').modal('show');
            });

            $('#btnAddNewTopSlider').click(function () {
                type = '{{ SliderType::$NEW_ARRIVAL_TOP_SLIDER }}';
                $('#item-modal').modal('show');
            });

            $('#btnAddNewSecondSlider').click(function () {
                type = '{{ SliderType::$NEW_ARRIVAL_SECOND_SLIDER }}';
                $('#item-modal').modal('show');
            });

            
            $(document).on('click', '.modal-item', function () {
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_banner_item_add') }}",
                    data: { id: id, type: type }
                }).done(function( data ) {
                    if (data.success)
                        location.reload();
                    else
                        alert(data.message);
                });
            });

            $('.btnRemove').click(function () {
                $('#deleteModal').modal('show');
                type = $(this).data('type');
                id = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_banner_item_remove') }}",
                    data: { type: type, id: id }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            // ================Sortable==========

            // Homepage Slider
            var el = document.getElementById('HomePageSliderItems');
            Sortable.create(el, {
                animation: 150,
                dataIdAttr: 'data-id',
                onEnd: function () {
                    updateSort(this.toArray());
                },
            });

            // Category Top Slider
            var el2 = document.getElementById('categoryTopSliderItems');
            Sortable.create(el2, {
                animation: 150,
                dataIdAttr: 'data-id',
                onEnd: function () {
                    updateSort(this.toArray());
                },
            });

            // Category Second Slider
            var el3 = document.getElementById('categorySecondSliderItems');
            Sortable.create(el3, {
                animation: 150,
                dataIdAttr: 'data-id',
                onEnd: function () {
                    updateSort(this.toArray());
                },
            });

            // New Arrival Top Slider
            var el4 = document.getElementById('newTopSliderItems');
            Sortable.create(el4, {
                animation: 150,
                dataIdAttr: 'data-id',
                onEnd: function () {
                    updateSort(this.toArray());
                },
            });

            // New Arrival Second Slider
            var el5 = document.getElementById('newSecondSliderItems');
            Sortable.create(el5, {
                animation: 150,
                dataIdAttr: 'data-id',
                onEnd: function () {
                    updateSort(this.toArray());
                },
            });

            function updateSort(ids) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_banner_item_sort') }}",
                    data: { ids: ids }
                }).done(function( msg ) {
                    toastr.success('Items sort updated!');
                });
            }


            // Pagination
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                var page = getURLParameter(url, 'page');

                filterItem(page);
            });

            function getURLParameter(url, name) {
                return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
            }

            function filterItem(page) {
                page = typeof page !== 'undefined' ? page : 1;
                var search = $('#modal-search').val();
                var category = $('#modal-category').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_get_items_for_modal') }}",
                    data: { search: search, category: category, page: page }
                }).done(function( data ) {
                    var products = data.items.data;
                    $('#modal-pagination').html(data.pagination);

                    $('.modal-items').html('');

                    $.each(products, function (index, product) {
                        var html = $('#template-modal-item').html();
                        var row = $(html);

                        row.attr('data-id', product.id);
                        row.find('img').attr('src', product.imagePath);
                        row.find('.style-no').html(product.style_no);

                        $('.modal-items').append(row);
                    });
                });
            }

            $('#modal-btn-search').click(function () {
                filterItem();
            });
        });
    </script>
@stop