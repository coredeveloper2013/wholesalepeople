<form class="form-horizontal" action="{{ route('vendor_company_information_post') }}" method="POST">
    @csrf

    <div class="form-group row">
        <div class="col-lg-2">
            <label for="company_description" class="col-form-label">Company Description</label>
        </div>

        <div class="col-lg-10">
            <textarea class="form-control{{ $errors->has('company_description') ? ' is-invalid' : '' }}"
                      rows="5" name="company_description">{{ empty(old('company_description')) ? ($errors->has('company_description') ? '' : $user->vendor->company_info) : old('company_description') }}</textarea>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-2">
            <label for="year_established" class="col-form-label">Year Established</label>
        </div>

        <div class="col-lg-1 user_icon_change2">
            <input type="text" class="form-control{{ $errors->has('year_established') ? ' is-invalid' : '' }}" name="year_established"
                   value="{{ empty(old('year_established')) ? ($errors->has('year_established') ? '' : $user->vendor->year_established) : old('year_established') }}">
        </div>

        <div class="col-lg-1">
            <label for="username2" class="col-form-label">Type Of Company</label>
        </div>

        <div class="col-lg-1 user_icon_change2">
            <label for="username2" class="col-form-label">Manufacturer</label>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-2">
            <label for="business_category" class="col-form-label">Business Category</label>
        </div>

        <div class="col-lg-3">
            <input type="text" class="form-control{{ $errors->has('business_category') ? ' is-invalid' : '' }}" name="business_category"
                   value="{{ empty(old('business_category')) ? ($errors->has('business_category') ? '' : $user->vendor->business_category) : old('business_category') }}">
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-2">
            <label class="col-form-label">Industry</label>
        </div>

        <div class="col-lg-10">
            <?php
                $in = [];

                foreach($user->industries as $i) {
                    $in[] = $i->id;
                }
            ?>

            @foreach($industries as $industry)
            <div class="checkbox-inline">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="industry[]" value="{{ $industry->id }}" {{ in_array($industry->id, $in) ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">{{ $industry->name }}</span>
                </label>
            </div>
            @endforeach
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-2">
            <label for="business_category" class="col-form-label">Primary Customer Market</label>
        </div>

        <div class="col-lg-3">
            <select class="form-control" id="primaryCustomerMarket" name="primaryCustomerMarket">
                <option value="1" {{ $user->vendor->primary_customer_market == '1' ? 'selected' : '' }}>All</option>
                <option value="2" {{ $user->vendor->primary_customer_market == '2' ? 'selected' : '' }}>African</option>
                <option value="3" {{ $user->vendor->primary_customer_market == '3' ? 'selected' : '' }}>Asian</option>
                <option value="4" {{ $user->vendor->primary_customer_market == '4' ? 'selected' : '' }}>Caucasian</option>
                <option value="5" {{ $user->vendor->primary_customer_market == '5' ? 'selected' : '' }}>Latino/Hispanic</option>
                <option value="6" {{ $user->vendor->primary_customer_market == '6' ? 'selected' : '' }}>Middle Eastern</option>
                <option value="7" {{ $user->vendor->primary_customer_market == '7' ? 'selected' : '' }}>Native American</option>
                <option value="8" {{ $user->vendor->primary_customer_market == '8' ? 'selected' : '' }}>Pacific Islander</option>
                <option value="9" {{ $user->vendor->primary_customer_market == '9' ? 'selected' : '' }}>Other</option>
            </select>
        </div>
    </div>

    <h4>Account Information</h4>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Company Name</label>
                </div>

                <div class="col-md-10">
                    <label class="col-form-label">{{ $user->vendor->company_name }}</label>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Contract Person</label>
                </div>

                <div class="col-md-3">
                    <input type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name"
                           value="{{ empty(old('first_name')) ? ($errors->has('first_name') ? '' : $user->first_name) : old('first_name') }}">
                </div>

                <div class="col-md-3">
                    <input type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name"
                           value="{{ empty(old('last_name')) ? ($errors->has('last_name') ? '' : $user->last_name) : old('last_name') }}">
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Member Since</label>
                </div>

                <div class="col-md-10">
                    <label class="col-form-label">{{ date('F j, Y', strtotime($user->created_at)) }}</label>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Email *</label>
                </div>

                <div class="col-md-6">
                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                           value="{{ empty(old('email')) ? ($errors->has('email') ? '' : $user->email) : old('email') }}">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <h4>Showroom Address</h4>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Address *</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('showroom_address') ? ' is-invalid' : '' }}" name="showroom_address"
                           value="{{ empty(old('showroom_address')) ? ($errors->has('showroom_address') ? '' : $user->vendor->billing_address) : old('showroom_address') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">City *</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('showroom_city') ? ' is-invalid' : '' }}" name="showroom_city"
                           value="{{ empty(old('showroom_city')) ? ($errors->has('showroom_city') ? '' : $user->vendor->billing_city) : old('showroom_city') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">State</label>
                </div>

                <div class="col-md-6">
                    <select class="form-control" name="showroom_state" id="showroom_state">
                        <option value="">Select State</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Zip Code *</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('showroom_zip_code') ? ' is-invalid' : '' }}" name="showroom_zip_code"
                           value="{{ empty(old('showroom_zip_code')) ? ($errors->has('showroom_zip_code') ? '' : $user->vendor->billing_zip) : old('showroom_zip_code') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Country *</label>
                </div>

                <div class="col-md-6">
                    <select class="form-control{{ $errors->has('showroom_country') ? ' is-invalid' : '' }}" name="showroom_country" id="showroom_country">
                        <option value="">Select Country</option>

                        @foreach($countries as $country)
                            <option value="{{ $country->id }}" {{ $user->vendor->billing_country_id == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Tel *</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('showroom_tel') ? ' is-invalid' : '' }}" name="showroom_tel"
                           value="{{ empty(old('showroom_tel')) ? ($errors->has('showroom_tel') ? '' : $user->vendor->billing_phone) : old('showroom_tel') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Alt</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('showroom_alt') ? ' is-invalid' : '' }}" name="showroom_alt"
                           value="{{ empty(old('showroom_alt')) ? ($errors->has('showroom_alt') ? '' : $user->vendor->billing_alternate_phone) : old('showroom_alt') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Fax</label>
                </div>

                <div class="col-md-6 ">
                    <input type="text" class="form-control{{ $errors->has('showroom_fax') ? ' is-invalid' : '' }}" name="showroom_fax"
                           value="{{ empty(old('showroom_fax')) ? ($errors->has('showroom_fax') ? '' : $user->vendor->billing_fax) : old('showroom_fax') }}">
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <h4>Warehouse Address</h4>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Address *</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('warehouse_address') ? ' is-invalid' : '' }}" name="warehouse_address"
                           value="{{ empty(old('warehouse_address')) ? ($errors->has('warehouse_address') ? '' : $user->vendor->factory_address) : old('warehouse_address') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">City *</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('warehouse_city') ? ' is-invalid' : '' }}" name="warehouse_city"
                           value="{{ empty(old('warehouse_city')) ? ($errors->has('warehouse_city') ? '' : $user->vendor->factory_city) : old('warehouse_city') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">State *</label>
                </div>

                <div class="col-md-6">
                    <select class="form-control" name="warehouse_state" id="warehouse_state">
                        <option value="">Select State</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Zip Code *</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('warehouse_zip_code') ? ' is-invalid' : '' }}" name="warehouse_zip_code"
                           value="{{ empty(old('warehouse_zip_code')) ? ($errors->has('warehouse_zip_code') ? '' : $user->vendor->factory_zip) : old('warehouse_zip_code') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Country *</label>
                </div>

                <div class="col-md-6">
                    <select class="form-control{{ $errors->has('warehouse_country') ? ' is-invalid' : '' }}" name="warehouse_country" id="warehouse_country">
                        <option value="">Select Country</option>

                        @foreach($countries as $country)
                            <option value="{{ $country->id }}" {{ $user->vendor->factory_country_id == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Tel *</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('warehouse_tel') ? ' is-invalid' : '' }}" name="warehouse_tel"
                           value="{{ empty(old('warehouse_tel')) ? ($errors->has('warehouse_tel') ? '' : $user->vendor->factory_phone) : old('warehouse_tel') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Alt</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('warehouse_alt') ? ' is-invalid' : '' }}" name="warehouse_alt"
                           value="{{ empty(old('warehouse_alt')) ? ($errors->has('warehouse_alt') ? '' : $user->vendor->factory_alternate_phone) : old('warehouse_alt') }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label class="col-form-label">Fax</label>
                </div>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('warehouse_fax') ? ' is-invalid' : '' }}" name="warehouse_fax"
                           value="{{ empty(old('warehouse_fax')) ? ($errors->has('warehouse_fax') ? '' : $user->vendor->factory_fax) : old('warehouse_fax') }}">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <input class="btn btn-primary" type="submit" value="Save">
        </div>
    </div>
</form>