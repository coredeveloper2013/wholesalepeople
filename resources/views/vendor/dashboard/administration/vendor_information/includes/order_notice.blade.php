<form action="{{ route('vendor_order_notice_post') }}" method="POST">
    @csrf

    <div class="row">
        <div class="col-md-1">
            <label>Description</label>
        </div>

        <div class="col-md-11">
            <textarea name="order_notice_editor" id="order_notice_editor" rows="10" cols="80">{{ $user->vendor->order_notice }}</textarea>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12 text-right">
            <button class="btn btn-primary" id="btnOrderNoticeSubmit">Save</button>
        </div>
    </div>
</form>