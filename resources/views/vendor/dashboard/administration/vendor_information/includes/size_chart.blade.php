<form action="{{ route('vendor_size_chart_post') }}" method="POST">
    @csrf

    <div class="row">
        <div class="col-md-1">
            <label>Description</label>
        </div>

        <div class="col-md-11">
            <textarea name="size_chart_editor" id="size_chart_editor" rows="10" cols="80">{{ $user->vendor->size_chart }}</textarea>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12 text-right">
            <button class="btn btn-primary" id="btnSizeChartSubmit">Save</button>
        </div>
    </div>
</form>