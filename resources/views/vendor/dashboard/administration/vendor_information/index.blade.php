@extends('vendor.layouts.dashboard')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <style>
        .checkbox-inline {
            display: inline-flex;
        }
    </style>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="company-info-tab" data-toggle="tab" href="#company-info" role="tab" aria-controls="company-info" aria-selected="true">Company Info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="size-chart-tab" data-toggle="tab" href="#size-chart" role="tab" aria-controls="size-chart" aria-selected="false">Size Chart</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="order-notice-tab" data-toggle="tab" href="#order-notice" role="tab" aria-controls="order-notice" aria-selected="false">Order Notice</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false">Settings</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="company-info" role="tabpanel" aria-labelledby="company-info-tab">
                    <br>
                    @include('vendor.dashboard.administration.vendor_information.includes.company_info')
                </div>

                <div class="tab-pane fade" id="size-chart" role="tabpanel" aria-labelledby="size-chart-tab">
                    <br>
                    @include('vendor.dashboard.administration.vendor_information.includes.size_chart')
                </div>

                <div class="tab-pane fade" id="order-notice" role="tabpanel" aria-labelledby="order-notice-tab">
                    <br>
                    @include('vendor.dashboard.administration.vendor_information.includes.order_notice')
                </div>

                <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                    <br>
                    @include('vendor.dashboard.administration.vendor_information.includes.settings')
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            var usStates = <?php echo json_encode($usStates); ?>;
            var caStates = <?php echo json_encode($caStates); ?>;
            var showroomStateId = '{{ $user->vendor->billing_state_id }}';
            var warehouseStateId = '{{ $user->vendor->factory_state_id }}';

            $('#showroom_country').change(function () {
                var countryId = $(this).val();
                $('#showroom_state').html('<option value="">Select State</option>');

                if (countryId == 1) {
                    $.each(usStates, function (index, value) {
                        if (value.id == showroomStateId)
                            $('#showroom_state').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#showroom_state').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                } else if (countryId == 2) {
                    $.each(caStates, function (index, value) {
                        if (value.id == showroomStateId)
                            $('#showroom_state').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#showroom_state').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                }
            });

            $('#warehouse_country').change(function () {
                var countryId = $(this).val();
                $('#warehouse_state').html('<option value="">Select State</option>');

                if (countryId == 1) {
                    $.each(usStates, function (index, value) {
                        if (value.id == warehouseStateId)
                            $('#warehouse_state').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#warehouse_state').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                } else if (countryId == 2) {
                    $.each(caStates, function (index, value) {
                        if (value.id == warehouseStateId)
                            $('#warehouse_state').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#warehouse_state').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                }
            });

            $('#showroom_country').trigger('change');
            $('#warehouse_country').trigger('change');

            var sizeEditor = CKEDITOR.replace( 'size_chart_editor' );
            var orderNotice = CKEDITOR.replace( 'order_notice_editor' );

            $('#btnSizeChartSubmit').click(function (e) {
                e.preventDefault();
                var description = sizeEditor.getData();

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_size_chart_post') }}",
                    data: { description: description }
                }).done(function( msg ) {
                    toastr.success("Size Chart Updated!");
                });
            });

            $('#btnOrderNoticeSubmit').click(function (e) {
                e.preventDefault();
                var description = orderNotice.getData();

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_order_notice_post') }}",
                    data: { description: description }
                }).done(function( msg ) {
                    toastr.success("Order Notice Updated!");
                });
            });

            $('#btnSaveSettings').click(function (e) {
                e.preventDefault();

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_save_setting_post') }}",
                    data: $('#form-settings').serialize(),
                }).done(function( data ) {
                    if (data.success) {
                        toastr.success("Settings Saved!");
                    } else {
                        alert(data.message);
                    }
                });
            });
        })
    </script>
@stop